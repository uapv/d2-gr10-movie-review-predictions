import tensorflow as tf
from tensorflow.keras.models import Model
from tensorflow.keras.layers import (
    Dense, 
    Concatenate, 
    Dropout, 
    Embedding, 
    Conv1D, 
    MaxPooling1D, 
    GlobalMaxPooling1D, 
    SpatialDropout1D, 
    BatchNormalization,
)

from CnnModel import CnnModel

class CnnBasicModel(CnnModel):

    def __init__(
        self, vocabulary:dict, 
        embeddings:list, 
        max_tokens:int,
        max_epochs:int,
        batch_size:int,
        learning_rate:float,
        filters:int,
        dropout:float,
        trainable_embeddings:bool,
    ):
        super().__init__(
            vocabulary,
            embeddings,
            max_tokens,
            embedding_dimensions=300,
            # Training Parameters
            max_epochs=max_epochs,
            batch_size=batch_size,
            learning_rate=learning_rate,
            # Model Hyperparameters
            filters=filters,
            dropout=dropout,
            is_trainable_embeddings=trainable_embeddings,
            patience=1,
        )

    def __build_model__(self):
        inputs = tf.keras.layers.Input((self.max_tokens,), name="input", )
        embs = Embedding(
            len(self.vocabulary), self.embedding_dimensions, input_length=self.max_tokens,
            weights=[self.embeddings], 
            trainable=self.is_trainable_embeddings
        )(inputs)

        x1 = Conv1D(self.filters, 1, activation='relu', padding='valid')(embs)
        x1 = SpatialDropout1D(self.dropout)(x1)
        x1 = MaxPooling1D()(x1)
        # x1 = Conv1D(self.filters*2, 1, activation='relu', padding='valid')(x1)
        # x1 = SpatialDropout1D(0.2)(x1)
        # x1 = MaxPooling1D()(x1)
        # x1 = Conv1D(self.filters*3, 1, activation='relu', padding='valid')(x1)
        # x1 = SpatialDropout1D(0.2)(x1)
        # x1 = MaxPooling1D()(x1)
        x1 = GlobalMaxPooling1D()(x1)
        
        x2 = Conv1D(self.filters, 2, activation='relu', padding='valid')(embs)
        x2 = SpatialDropout1D(self.dropout)(x2)
        x2 = MaxPooling1D()(x2)
        # x2 = Conv1D(self.filters*2, 2, activation='relu', padding='valid')(x2)
        # x2 = SpatialDropout1D(0.2)(x2)
        # x2 = MaxPooling1D()(x2)
        # x2 = Conv1D(self.filters*3, 2, activation='relu', padding='valid')(x2)
        # x2 = SpatialDropout1D(0.2)(x2)
        # x2 = MaxPooling1D()(x2)
        x2 = GlobalMaxPooling1D()(x2)
        
        x3 = Conv1D(self.filters, 3, activation='relu', padding='valid')(embs)
        x3 = SpatialDropout1D(self.dropout)(x3)
        x3 = MaxPooling1D()(x3)
        # x3 = Conv1D(self.filters*2, 3, activation='relu', padding='valid')(x3)
        # x3 = SpatialDropout1D(0.2)(x3)
        # x3 = MaxPooling1D()(x3)
        # x3 = Conv1D(self.filters*3, 3, activation='relu', padding='valid')(x3)
        # x3 = SpatialDropout1D(0.2)(x3)
        # x3 = MaxPooling1D()(x3)
        x3 = GlobalMaxPooling1D()(x3)
        
        x4 = Conv1D(self.filters, 4, activation='relu', padding='valid')(embs)
        x4 = SpatialDropout1D(self.dropout)(x4)
        x4 = MaxPooling1D()(x4)
        # x4 = Conv1D(self.filters*2, 4, activation='relu', padding='valid')(x4)
        # x4 = SpatialDropout1D(0.2)(x4)
        # x4 = MaxPooling1D()(x4)
        # x4 = Conv1D(self.filters*3, 4, activation='relu', padding='valid')(x4)
        # x4 = SpatialDropout1D(0.2)(x4)
        # x4 = MaxPooling1D()(x4)
        x4 = GlobalMaxPooling1D()(x4)
        
        x5 = Conv1D(self.filters, 5, activation='relu', padding='valid')(embs)
        x5 = SpatialDropout1D(self.dropout)(x5)
        x5 = MaxPooling1D()(x5)
        # x5 = Conv1D(self.filters*2, 5, activation='relu', padding='valid')(x5)
        # x5 = SpatialDropout1D(0.2)(x5)
        # x5 = MaxPooling1D()(x5)
        # x5 = Conv1D(self.filters*3, 5, activation='relu', padding='valid')(x5)
        # x5 = SpatialDropout1D(0.2)(x5)
        # x5 = MaxPooling1D()(x5)
        x5 = GlobalMaxPooling1D()(x5)
        
        # x6 = Conv1D(self.filters, 6, activation='relu', padding='valid')(embs)
        # x6 = SpatialDropout1D(0.3)(x6)
        # x6 = MaxPooling1D()(x6)
        # # x6 = Conv1D(self.filters*2, 5, activation='relu', padding='valid')(x6)
        # # x6 = SpatialDropout1D(0.2)(x6)
        # # x6 = MaxPooling1D()(x6)
        # # x6 = Conv1D(self.filters*3, 5, activation='relu', padding='valid')(x6)
        # # x6 = SpatialDropout1D(0.2)(x6)
        # # x6 = MaxPooling1D()(x6)
        # x6 = GlobalMaxPooling1D()(x6)

        x = Concatenate(axis=1)([x1,x2,x3,x4,x5])
        # x = GlobalMaxPooling1D()(x)
        # x = Flatten()(x)
        x = Dense(2048, activation='relu')(x)
        x = BatchNormalization()(x)
        x = Dropout(self.dropout)(x)
        output = Dense(self.NB_CLASSES, activation='softmax')(x)  #multi-label (k-hot encoding)
        
        model = Model(inputs=[inputs], outputs=output)
        
        return model

##############################################################################
# [ENTRYPOINTS]
##############################################################################
from tensorflow.keras.layers import Input
from tensorflow.keras.models import Sequential
if __name__ == "__main__":
    
    x_input = Input(shape=(1,))
    x = Dense(32, activation='relu')(x_input, training=False)
    x = Dropout(0.5)(x, training=False)
    x_output = Dense(2, activation='softmax')(x)
    model = Model(x_input, x_output)
    model.compile(loss='mse', optimizer='adam')
    model.summary()

    model1 = Sequential()
    model1.add(Input(shape=(1,)))
    model1.add(Dense(32, activation='relu'))
    model1.add(BatchNormalization())
    model1.add(Dropout(0.5))
    model1.add(Dense(2, activation='softmax'))
    model1.compile(loss='mse', optimizer='adam')
    model1.summary()
    exit