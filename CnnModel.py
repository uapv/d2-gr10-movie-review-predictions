import glob
import os
import uuid
import numpy as np
import matplotlib.pyplot as plt

import tensorflow as tf
from tensorflow.keras.utils import plot_model
from tensorflow.keras import optimizers
from tensorflow.keras.models import Model, load_model
from tensorflow.keras.utils import plot_model
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
from sklearn.metrics import classification_report

class CnnModel(Model):
    
    MAX_SEQUENCE_LEN = 250

    PREDICTOR_CLASSES = ['0.5', '1.0', '1.5', '2.0', '2.5', '3.0', '3.5', '4.0', '4.5', '5.0']
    NB_CLASSES = len(PREDICTOR_CLASSES)
    CAT_TO_NOTE = {
        0:'0,5', 
        1:'1,0', 
        2:'1,5', 
        3:'2,0', 
        4:'2,5', 
        5:'3,0', 
        6:'3,5', 
        7:'4,0', 
        8:'4,5', 
        9:'5,0',
    }

    REGEX_POSITIVE = r":\)+|:\-\)+"
    REGEX_NEGATIVE = r":\(+|:\-\(+"
    # REGEX_EMPHASIS = r"\!{2,}"
    REGEX_EMPHASIS = r"\!+"
    REGEX_DIGITS = r"(?<!\.|,|\/)[0-9]+(?!\.|,|\/)"
    REGEX_DATES = r"\d{2}\/\d{2}\/\d{4}"
    REGEX_URLS = r"(?:http(s)?:\/\/)[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+"
    REGEX_CYRILLIC = r"[А-Яа-яЁё]+"
    REGEX_ASIAN = r"[\p{IsHan}\p{IsBopo}\p{IsHira}\p{IsKatakana}]+"
    REGEX_TRIM_START = r"(?u)\b_+"
    REGEX_TRIM_SPACES = r"\s+"
    REGEX_SPECIAL_CHARS = r"[\"«»…]"
    REGEX_APOSTROPHES = r"[`'’]"
    REGEX_ELONGATED_SEQ = r"(([a-z!#$%&'()*+,-./:;<=>?@[\]^_`{|}~])\2+)"

    TOKEN_POSITIVE = '<tok_pos>'
    TOKEN_NEGATIVE = '<tok_neg>'
    TOKEN_EMPHASIS = '<tok_emp>'

    REGEX_REPLACEMENTS = [
        (REGEX_POSITIVE, TOKEN_POSITIVE),
        (REGEX_NEGATIVE, TOKEN_NEGATIVE),
        (REGEX_EMPHASIS, TOKEN_EMPHASIS),
        (REGEX_URLS, ''),
        (REGEX_DATES, ''),
        (REGEX_CYRILLIC, ''),
        (REGEX_ASIAN, ''),
        (REGEX_TRIM_START, ''),
        (REGEX_APOSTROPHES, ' '), # Already done by regex tokenizer!
        (REGEX_SPECIAL_CHARS, ''), # Already done by regex tokenizer!
    ]

    def __init__(
        self, 
        vocabulary:dict, 
        embeddings:list, 
        max_tokens:int,
        embedding_dimensions,
        # Training Parameters
        max_epochs,
        batch_size,
        learning_rate,
        # Model Hyperparameters
        filters,
        dropout,
        is_trainable_embeddings,
        patience,
    ):
        super().__init__()

        self.id = uuid.uuid4()
        self.model_directory = f'models/CNN/{self.id}'

        if not os.path.exists(self.model_directory):
            os.makedirs(self.model_directory)

        # Vocabulary
        self.vocabulary = vocabulary
        self.max_tokens = max_tokens

        # Embeddings
        self.embeddings = embeddings
        self.embedding_dimensions = embedding_dimensions

        # Training Parameters
        self.max_epochs = max_epochs
        self.batch_size = batch_size
        self.learning_rate = learning_rate

        # Model Hyperparameters
        self.filters = filters
        self.dropout = dropout
        self.is_trainable_embeddings = is_trainable_embeddings
        self.patience = patience

        # Callbacks
        self.callbacks = [
            EarlyStopping(
                patience=self.patience, 
                monitor='val_loss', 
                min_delta=1e-3, 
                verbose=1), 
            ModelCheckpoint(
                '{}/model.b{:0>3}-mt{}-vacc{}.h5'.format(
                self.model_directory,
                self.batch_size, 
                max_tokens, 
                '{val_categorical_accuracy:.4f}'), 
                monitor='val_categorical_accuracy', 
                mode='max', 
                verbose=1, 
                save_best_only=True)
        ]

        self.optimizer = optimizers.Adam(learning_rate=self.learning_rate)

    def train(self, x_train, y_train, x_test, y_test):
        model = self.__build_model__()
        model.compile(loss='categorical_crossentropy', optimizer=self.optimizer, metrics=['categorical_accuracy'])
        model.summary()
        plot_model(model, show_shapes=True, to_file=f'{self.model_directory}/model_architecture.png')

        return model.fit(
            x_train, y_train, 
            validation_data=(x_test,y_test), 
            batch_size=self.batch_size, 
            epochs=self.max_epochs, 
            callbacks=self.callbacks, 
            shuffle=True, 
            verbose=2)

    def get_last_best(self):
        list_of_files = glob.glob(f'{self.model_directory}/*.h5')
        model_filename = max(list_of_files, key=os.path.getctime)
        # model = load_model('models/model.b256-vacc0.4040.h5')
        return load_model(model_filename)

    def evaluate_last_best(self, x_test, y_test):
        model = self.get_last_best()

        y_pred = model.predict(x_test)
        y_pred = np.argmax(y_pred, axis=1)
        y_test = np.argmax(y_test, axis=1)

        print('Test: {}'.format(np.unique(y_test)))
        print('Pred: {}'.format(np.unique(y_pred)))

        print()
        print('> Plotting confusion matrix...')
        cm = confusion_matrix(y_test, y_pred)
        disp = ConfusionMatrixDisplay(confusion_matrix=cm, display_labels=self.PREDICTOR_CLASSES)
        # disp = ConfusionMatrixDisplay(confusion_matrix=cm)
        disp.plot()
        plt.savefig(f'{self.model_directory}/model_confusion-matrix.png')

        print()
        print('> Calculating classification report...')
        cr = classification_report(y_test, y_pred, target_names=self.PREDICTOR_CLASSES)
        # cr = classification_report(y_test, y_pred)
        print(cr)

    def __build_model__(self):
        raise NotImplementedError("Must override '__build_model__'")

##############################################################################
# [ENTRYPOINTS]
##############################################################################
if __name__ == "__main__":
    exit