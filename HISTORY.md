# History

1. Clean XML dataset.
   1. Replace invalid XML syntax. (& => &amp;)
   2. Extract values from XML using XPath.
   3. Save clean dataset in compressed format.
2. Descriptive Statistics
   1. Univariate Analysis
      1. note (describe + histplot + boxplot)
      2. comment_length (describe + histplot + boxplot)
      3. reviews_per_movie_id (describe + histplot + boxplot)
      4. reviews_per_user_id (describe + histplot + boxplot)
      5. reviews_per_name (describe + histplot + boxplot)
   2. Multivariate Analysis
      1. Correlation Matrices (heatmap)
         1. note
         2. comment_length
         3. reviews_per_movie_id
         4. reviews_per_user_id
         5. reviews_per_name
         6. comment_len_avg_per_movie_id
         7. comment_len_med_per_movie_id
         8. comment_len_avg_per_user_id
         9. comment_len_med_per_user_id
         10. note_avg_per_movie_id
         11. note_med_per_movie_id
         12. note_avg_per_user_id
         13. note_med_per_user_id
   3. Textual Analysis
      1. Preprocessing
         1. lowercase
         2. preprocessor
            1. Replace emojis, emoticons with special tokens.
            2. Replace multiple subsequent ! with special token.
            3. Remove digits from tokens, except in note-ish format. (4/5 or 1.5/5.0)
         3. token_pattern
            1. r'(?u)\b\w+\b'
            2. Only keep words and ignore any punctuation.
         4. Save vocabulary + frequencies in compressed and CSV format.
      2. Word Frequencies
         1. Top 30 Most Common Words
         2. Comments containing TOK_POS, TOK_NEG or TOK_EMP
      3. Bag-of-Words
         1. TF
            1. Counter (Removed, too slow!)
            2. CountVectorizer (nltk)
         2. TF-IDF
            1. TfidfVectorizer (nltk)
   4. Extracted Features
      1. comment_length
      2. Special Tokens
         1. TOK_POS
         2. TOK_NEG
         3. TOK_EMP
      3. Word Frequencies
         1. Bag-of-Words with Counter
         2. Bag-of-Words with TF
   5. Observations
      1. Still lot's of insignificant words in the vocabulary.
         1. Implement TF-IDF.
         2. Create problem-specific stop word dictionary.
      2. Missing interesting qualitative and quantitative features in the dataset.
         1. Extract features from AlloCiné or IMDB using web scraper (Scrapy).
      3. Be wary of model generalization on lower notes during evaluation.
         1. Distribution of notes show that reviews tend to concentrate notes >= 2.5.
         2. Might not have enough data to correctly generalize on low note predictions.
      4. Use sentiment dictionary to tag positive/negative/neutral words.
3. Models
    1. SKlearn
    2. CNN
       1. Preprocessing
          1. Aggressive preprocessing seems to negatively affect the classification.
             1. Removing any non alphabetical characters.
             2. Stemming
             3. Lemmatization
             4. Removing short words of a single character.
       2. Sampling
          1. Uniform sampling across all predictor classes.
             1. Undersampling overrepresented classes.
          2. Random undersampling seems to be losing discriminating features.
             1. Overall accuracy is uniform across classes.
             2. Accuracy in some classes is much lower than with no undersampling.
       3. Vocabulary
          1. Save compressed version of vocabulary for faster subsequent runs.
          2. Build full vocabulary using complete dev and train dataset.
       4. Word Embeddings
          1. FastText - No Training
          2. FastText - Pre-trained
          3. Over 100,000 words from vocabulary don't exist in the FastText embeddings.
             1. Inspect words from vocabulary not found in word embeddings.
             2. Many of these are syntactic or grammatical errors introduced by the users.
       5. Inputs (Vectorized Comments)
          1. Ignore tokens from comments that are not found in vocabulary.
          2. Calculate maximum sequence length using 90th quantile of token counts for comments.
       6. Model Architecture
          1. Use multiple Conv1D/MaxPool1D layers with different strides to mimic N-Grams.
4. Experiments
    1. TODO
      1. DONE: Remove useless words like 'a' that appear at the top of the vocabulary.
      2. DONE: Remove tokens with numeric values in the vocabulary.
      3. How do we ensure that special tokens are always in the vocabulary?
         1. Not removed by N-grams filtering (ignoring unigrams) or TF-IDF scores.
         2. <https://stackoverflow.com/questions/19753945/tfidfvectorizer-in-sklearn-how-to-specifically-include-words>
      4. Stemming!
      5. Lemmatization!
      6. Should we create a special token for TOK_SML_EMP (!) and TOK_BIG_EMP (!!+)?
      7. What if we flag comments with occurences of most common words per predictor class?
         1. 0_5_words
         2. 1_0_words
         3. 1_5_words
         4. 2_0_words
         5. 2_5_words
         6. 3_0_words
         7. 3_5_words
         8. 4_0_words
         9. 4_5_words
         10. 5_0_words
      8. What if we flag comments with occurences of most common words per grouped predictor class?
         1. 0_5_to_1_5_words
         2. 2_0_words
         3. 2_5_to_5_0_words
   1. Results and Observations
      1. Special meaning tokens were attached to the start/end of words.
         1. Wrapped them with spaces in the preprocessing step.
      2. Only ~1-2% of comments contain TOK_POS or TOK_NEG.
         1. Include it, but skeptical about usefulness.
      3. Only ~8% of comments contain TOK_EMP.
         1. Include it, but skeptical about usefulness.
      4. Common words appearing frequently across documents seem to negatively impact accuracy. (H1)
         1. Lowering CountVectorizer's 'max_df' from 0.95 to 0.75 increases accuracy.
      5. Rare words appearing infrequently across documents seem to increase accuracy. (H2)
         1. Lowering CountVectorizer's 'min_df' from 0.5 to 0.01 greatly improve accuracy.
      6. Removing too many common words has reduced generalizing accuracy in unseen documents. (H3)
      7. Ratings are sometimes in the comments as is.
         1. Can't remove digits blindly or we might lose on some "rating" tokens in vocabulary.
      8. The training time doesn't seem to be affected by the number of features in the vectorizer.
         1. Setting 'max_features' to a low number such as 2000, does not improve training time.
   2. Hypotheses
         1. Common words in corpus seem to carry less meaning.
         2. Rarer words in corpus seem to carry more meaning.
         3. Even if rarer words carry less meaning, removing many common words prevents from generalizing across unseen documents.
