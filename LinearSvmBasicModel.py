from sklearn.pipeline import Pipeline

from LinearSvmModel import LinearSvmModel
from SklearnPipelineTransformers import TextSelector

class LinearSvmBasicModel(LinearSvmModel):

    def __init__(
        self, 
        vectorizer,
        alpha:float,
        max_iterations:int,
        penalty:str,
        loss:str,
    ):
        super().__init__(
            vectorizer,
            alpha,
            max_iterations,
            penalty,
            loss,
        )
        
        # Training Parameters
        # ...

        # Model Hyperparameters
        # ...
        
        # Pipeline Components
        # ...

    def create_pipeline(self):
        return Pipeline([
            ('text', TextSelector('comment')),
            ('tfidf', self.vectorizer),
            # SOURCE: https://towardsdatascience.com/how-to-train-test-split-kfold-vs-stratifiedkfold-281767b93869
            # SOURCE: https://deepnote.com/@djilax/4-Kfold-cross-validation-My-machine-learning-pipeline-LLi1YztnRp6IBbtNk0HPcg
            # ('skfold', StratifiedKFold(3)),
            # ("classifier", classifier),
            # ('feature_selection', SelectFromModel(LinearSVC())),
            # SOURCE: https://medium.com/@GouthamPeri/pipeline-with-tuning-scikit-learn-b2789dca9dc2
            # ('feature_select', SelectKBest(chi2, k=10)),
            ('classifier', self.classifier)
        ])


##############################################################################
# [MAIN APPLICATION]
##############################################################################
if __name__ == "__main__":
    exit