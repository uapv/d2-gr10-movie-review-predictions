from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.preprocessing import MinMaxScaler, StandardScaler

from LinearSvmModel import LinearSvmModel
from SklearnPipelineTransformers import TextSelector, MovieGenres, MovieNumericValues

class LinearSvmMetadataModel(LinearSvmModel):

    def __init__(
        self, 
        vectorizer,
        alpha:float,
        max_iterations:int,
        penalty:str,
        loss:str,
    ):
        super().__init__(
            vectorizer,
            alpha,
            max_iterations,
            penalty,
            loss,
        )

        # Training Parameters
        # ...

        # Model Hyperparameters
        # ...
        
        # Pipeline Components
        self.tfidf_pipe = Pipeline([
            ('text', TextSelector('comment')),
            ('tfidf', self.vectorizer)
        ])

        self.metadata_pipe = Pipeline([
            ('metadata', MovieGenres()),
            ('scaler', StandardScaler())
        ])

        self.release_month_pipe = Pipeline([
            ('metadata', MovieNumericValues('movie_release_month')),
            ('scaler', StandardScaler())
        ])
        
        self.release_day_pipe = Pipeline([
            ('metadata', MovieNumericValues('movie_release_day')),
            ('scaler', StandardScaler())
        ])

        self.duration_pipe = Pipeline([
            ('metadata', MovieNumericValues('movie_duration_mins')),
            ('scaler', StandardScaler())
        ])

        # print(release_month_pipe.fit_transform(dev.head(2)))

    def create_pipeline(self) -> Pipeline:
        return Pipeline([
            ('features', FeatureUnion([
                ('tfidf', self.tfidf_pipe),
                # ('metadata', metadata_pipe),
                ('metadata', MovieGenres()),
                ('release_month', self.release_month_pipe),
                ('release_day', self.release_day_pipe),
                ('duration', self.duration_pipe),
            ])),
            # SOURCE: https://towardsdatascience.com/how-to-train-test-split-kfold-vs-stratifiedkfold-281767b93869
            # SOURCE: https://deepnote.com/@djilax/4-Kfold-cross-validation-My-machine-learning-pipeline-LLi1YztnRp6IBbtNk0HPcg
            # ('skfold', StratifiedKFold(3)),
            # ("classifier", classifier),
            # ('feature_selection', SelectFromModel(LinearSVC())),
            # SOURCE: https://medium.com/@GouthamPeri/pipeline-with-tuning-scikit-learn-b2789dca9dc2
            # ('feature_select', SelectKBest(chi2, k=10)),
            ('classifier', self.classifier)
        ])

##############################################################################
# [MAIN APPLICATION]
##############################################################################
if __name__ == "__main__":
    exit