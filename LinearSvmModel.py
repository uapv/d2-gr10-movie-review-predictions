import os
import uuid
import pickle
import pandas as pd

from scipy.sparse.csr import csr_matrix
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import SGDClassifier
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
from sklearn.metrics import classification_report

class LinearSvmModel():

    RANDOM_STATE = 42

    PREDICTOR_CLASSES = ['0.5', '1.0', '1.5', '2.0', '2.5', '3.0', '3.5', '4.0', '4.5', '5.0']
    NB_CLASSES = len(PREDICTOR_CLASSES)
    CAT_TO_NOTE = {
        0:'0,5', 
        1:'1,0', 
        2:'1,5', 
        3:'2,0', 
        4:'2,5', 
        5:'3,0', 
        6:'3,5', 
        7:'4,0', 
        8:'4,5', 
        9:'5,0',
    }

    PATH_MODEL = 'models/model.sav'
    PATH_RESULTS = 'results/results.txt'

    REGEX_POSITIVE = r":\)+|:\-\)+"
    REGEX_NEGATIVE = r":\(+|:\-\(+"
    REGEX_EMPHASIS = r"\!+"
    REGEX_DIGITS = r"(?<!\.|,|\/)[0-9]+(?!\.|,|\/)"
    REGEX_DATES = r"\d{2}\/\d{2}\/\d{4}"
    REGEX_URLS = r"(?:http(s)?:\/\/)[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+"
    REGEX_CYRILLIC = r"[А-Яа-яЁё]+"
    REGEX_ASIAN = r"[\p{IsHan}\p{IsBopo}\p{IsHira}\p{IsKatakana}]+"
    REGEX_TRIM_START = r"(?u)\b_+"
    REGEX_TRIM_SPACES = r"\s+"
    REGEX_SPECIAL_CHARS = r"[\"«»…]"
    REGEX_APOSTROPHES = r"[`'’]"
    REGEX_ELONGATED_SEQ = r"(([a-z!#$%&'()*+,-./:;<=>?@[\]^_`{|}~])\2+)"

    TOKEN_POSITIVE = '<tok_pos>'
    TOKEN_NEGATIVE = '<tok_neg>'
    TOKEN_EMPHASIS = '<tok_emp>'

    REGEX_REPLACEMENTS = [
        (REGEX_POSITIVE, TOKEN_POSITIVE),
        (REGEX_NEGATIVE, TOKEN_NEGATIVE),
        (REGEX_EMPHASIS, TOKEN_EMPHASIS),
        (REGEX_URLS, ''),
        (REGEX_DATES, ''),
        (REGEX_CYRILLIC, ''),
        (REGEX_ASIAN, ''),
        (REGEX_TRIM_START, ''),
        (REGEX_APOSTROPHES, ' '), # Already done by regex tokenizer!
        (REGEX_SPECIAL_CHARS, ''), # Already done by regex tokenizer!
    ]

    def __init__(
        self, 
        vectorizer:TfidfVectorizer,
        alpha:float,
        max_iterations:int,
        penalty:str,
        loss:str,
    ):
        self.id = uuid.uuid4()
        self.model_directory = f'models/SVM/{self.id}'

        if not os.path.exists(self.model_directory):
            os.makedirs(self.model_directory)

        # Training Parameters
        # ...

        # Model Hyperparameters
        self.alpha = alpha
        self.max_iterations = max_iterations
        self.penalty = penalty
        self.loss = loss
        
        # Pipeline Components
        self.vectorizer = vectorizer

        # SOURCE: https://michael-fuchs-python.netlify.app/2019/11/11/introduction-to-sgd-classifier/
        self.classifier = SGDClassifier(
            alpha=self.alpha, 
            max_iter=self.max_iterations, 
            penalty=self.penalty, 
            loss=self.loss,
            random_state=self.RANDOM_STATE
        )

    def create_pipeline(self):
        raise NotImplementedError("Must override 'create_pipeline'")

    def train(self, x_train, y_train):
        self.pipeline = self.create_pipeline()

        print('Model Summary :')
        print(self.classifier)

        self.pipeline.fit(x_train, y_train)

        # Save the model for predictions.
        print()
        print('> Saving model to file \'{}\'...'.format(self.model_directory + '/model.sav'))
        with open(self.model_directory + '/model.sav', 'wb') as f:
            pickle.dump(self.pipeline, f)

        features = self.vectorizer.get_feature_names_out()
        print( '      Features : {}'.format(len(features)))

        # Squeeze the csr_matrix down to an array of TF-IDF coefficients.
        coefs = self.classifier.coef_
        if type(coefs) == csr_matrix:
            coefs.toarray().tolist()[0]
        else:
            coefs.tolist()

        # Most predictive overall
        coefs_features = list(zip(features, coefs[0]))
        best_features = sorted(coefs_features, key=lambda x: abs(x[1]), reverse=True)

        df_bow = pd.DataFrame(best_features)
        df_bow.columns = ["token", "frequency"]

        # Save the Bag-of-Words frequencies for analysis.
        df_bow.to_csv(self.model_directory + '/model-bow-tfidf.csv', sep='\t', quotechar='|')
        return

    def evaluate(self, x_test, y_test):
        score = self.pipeline.score(x_test, y_test)
        print(f" Mean Accuracy : {score}")

        # cv_scores = cross_val_score(lsvc, x_train, y_train, cv=10)
        # print("CV average score: %.2f" % cv_scores.mean())

        y_pred = self.pipeline.predict(x_test)

        # # cm = confusion_matrix(y_test, ypred)
        # # print(cm)
        import matplotlib.pyplot as plt
        cm = confusion_matrix(y_test, y_pred)
        disp = ConfusionMatrixDisplay(confusion_matrix=cm, display_labels=self.PREDICTOR_CLASSES)
        disp.plot()
        plt.show()

        cr = classification_report(y_test, y_pred, target_names=self.PREDICTOR_CLASSES)
        print()
        print(cr)
        return

    def predict(self, x_test):
        return self.pipeline.predict(x_test)
    
    def get_last_best(self):
        print('> Loading model from file \'{}\'...'.format(self.model_directory + '/model.sav'))
        with open(self.model_directory + '/model.sav', 'rb') as f:
            self.pipeline = pickle.load(f)


##############################################################################
# [MAIN APPLICATION]
##############################################################################
if __name__ == "__main__":
    exit