from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.preprocessing import MinMaxScaler, StandardScaler

from LinearSvmModel import LinearSvmModel
from SklearnPipelineTransformers import TextSelector, MovieGenres, MovieNumericValues, SentimentFeatures

class LinearSvmSentimentModel(LinearSvmModel):

    def __init__(
        self, 
        vectorizer,
        alpha:float,
        max_iterations:int,
        penalty:str,
        loss:str,
    ):
        super().__init__(
            vectorizer,
            alpha,
            max_iterations,
            penalty,
            loss,
        )

        # Training Parameters
        # ...

        # Model Hyperparameters
        # ...
        
        # Pipeline Components
        self.tfidf_pipe = Pipeline([
            ('text', TextSelector('comment')),
            ('tfidf', self.vectorizer),
        ])

        self.sentiment_pipe = Pipeline([
            ('metadata', SentimentFeatures()),
            # ('scaler', StandardScaler()),
            ('scaler', MinMaxScaler()),
        ])

        # print(release_month_pipe.fit_transform(dev.head(2)))

    def create_pipeline(self) -> Pipeline:
        return Pipeline([
            ('features', FeatureUnion([
                ('tfidf', self.tfidf_pipe),
                ('sentiment', self.sentiment_pipe),
            ])),
            # SOURCE: https://towardsdatascience.com/how-to-train-test-split-kfold-vs-stratifiedkfold-281767b93869
            # SOURCE: https://deepnote.com/@djilax/4-Kfold-cross-validation-My-machine-learning-pipeline-LLi1YztnRp6IBbtNk0HPcg
            # ('skfold', StratifiedKFold(3)),
            # ("classifier", classifier),
            # ('feature_selection', SelectFromModel(LinearSVC())),
            # SOURCE: https://medium.com/@GouthamPeri/pipeline-with-tuning-scikit-learn-b2789dca9dc2
            # ('feature_select', SelectKBest(chi2, k=10)),
            ('classifier', self.classifier)
        ])

##############################################################################
# [MAIN APPLICATION]
##############################################################################
if __name__ == "__main__":
    exit