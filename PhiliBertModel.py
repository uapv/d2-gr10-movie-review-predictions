from flair.embeddings import TransformerDocumentEmbeddings
from flair.models import TextClassifier
from flair.data import Dictionary, Sentence, Corpus
from flair.datasets import ClassificationCorpus, SentenceDataset
import stats_text as st
import auto_upload as au
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import uuid
import torch
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay

from PhiliBertModelTrainer import PhiliBertModelTrainer
from PhiliBertTextClassifier import PhiliBertTextClassifier

class PhiliBertModel():

    PREDICTOR_CLASSES = ['0.5', '1.0', '1.5', '2.0', '2.5', '3.0', '3.5', '4.0', '4.5', '5.0']
    NB_CLASSES = len(PREDICTOR_CLASSES)
    
    PATH_RESULTS = 'results/results.txt'
    
    REGEX_POSITIVE = r":\)+|:\-\)+"
    REGEX_NEGATIVE = r":\(+|:\-\(+"
    REGEX_EMPHASIS = r"\!{2,}"
    # REGEX_EMPHASIS = r"\!+"
    REGEX_DIGITS = r"(?<!\.|,|\/)[0-9]+(?!\.|,|\/)"
    REGEX_DATES = r"\d{2}\/\d{2}\/\d{4}"
    REGEX_URLS = r"(?:http(s)?:\/\/)[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+"
    REGEX_CYRILLIC = r"[А-Яа-яЁё]+"
    REGEX_ASIAN = r"[\p{IsHan}\p{IsBopo}\p{IsHira}\p{IsKatakana}]+"
    REGEX_TRIM_START = r"(?u)\b_+"
    REGEX_TRIM_SPACES = r"\s+"
    REGEX_SPECIAL_CHARS = r"[\"«»…]"
    REGEX_APOSTROPHES = r"[`'’]"
    REGEX_ELONGATED_SEQ = r"(([a-z!#$%&'()*+,-./:;<=>?@[\]^_`{|}~])\2+)"

    TOKEN_POSITIVE = '<tok_pos>'
    TOKEN_NEGATIVE = '<tok_neg>'
    TOKEN_EMPHASIS = '<tok_emp>'

    REGEX_REPLACEMENTS = [
        # (REGEX_POSITIVE, TOKEN_POSITIVE),
        # (REGEX_NEGATIVE, TOKEN_NEGATIVE),
        # (REGEX_EMPHASIS, TOKEN_EMPHASIS),
        (REGEX_URLS, ''),
        (REGEX_DATES, ''),
        (REGEX_CYRILLIC, ''),
        (REGEX_ASIAN, ''),
        (REGEX_TRIM_START, ''),
        (REGEX_APOSTROPHES, ' '), # Already done by regex tokenizer!
        (REGEX_SPECIAL_CHARS, ''), # Already done by regex tokenizer!
    ]

    def __init__(
        self,
        max_epochs:int,
        batch_size:int,
        learning_rate:float,
        epsilon:float,
        max_sequence_len:int,
        use_fp16:bool,
        amp_level:str,
        bert_model:str,
    ):
        self.id = uuid.uuid4()
        self.model_directory = f'models/philiBERT/{self.id}/'

        if not os.path.exists(self.model_directory):
            os.makedirs(self.model_directory)

        self.max_epochs=max_epochs
        self.batch_size=batch_size
        self.learning_rate=learning_rate
        self.epsilon=epsilon
        self.max_sequence_len=max_sequence_len
        self.use_fp16=use_fp16
        self.amp_level=amp_level
        self.bert_model=bert_model

        return

    def train(self, data_path:str):
        # 1. get the corpus
        corpus: Corpus = ClassificationCorpus(
            data_path, label_type='note',
            dev_file='dev.fasttext',
            train_file='train.fasttext',
            test_file='test.fasttext',
            truncate_to_max_tokens=self.max_sequence_len,
        )

        # 2. what label do we want to predict?
        label_type = 'note'

        # 3. create the label dictionary
        # label_dict = corpus.make_label_dictionary(label_type=label_type)
        label_dict = Dictionary(add_unk=False)
        for c in self.PREDICTOR_CLASSES:
            label_dict.add_item(c)
        print(label_dict)

        # 4. initialize transformer document embeddings (many models are available)
        document_embeddings = TransformerDocumentEmbeddings(self.bert_model, fine_tune=True)

        # 5. create the text classifier
        classifier = PhiliBertTextClassifier(document_embeddings, label_dictionary=label_dict, label_type=label_type)

        # 6. initialize trainer
        trainer = PhiliBertModelTrainer(classifier, corpus)

        # 7. run training with fine-tuning
        trainer.fine_tune(
            self.model_directory,
            learning_rate=self.learning_rate,
            mini_batch_size=self.batch_size,
            max_epochs=self.max_epochs,
            # train_with_dev=True, # Prevents saving best model when enabled!!!
            use_amp=self.use_fp16,
            amp_opt_level=self.amp_level,
            # checkpoint=True,
            optimizer=torch.optim.AdamW(trainer.model.parameters(), lr=self.learning_rate, eps=self.epsilon),
            # optimizer=torch.optim.Adam,
            # write_weights=True,
            monitor_train=True,
            # monitor_test=True, # No longer needed without 'train_with_dev'!
            use_final_model_for_eval=False,
            save_final_model=False,
            patience=1,
            # anneal_against_dev_loss=True, # Loss is a better indicator of generalization.
        )

        # visualize
        from flair.visual.training_curves import Plotter

        plotter = Plotter()
        plotter.plot_training_curves(self.model_directory + 'loss.tsv')
        # plotter.plot_weights(self.model_directory + 'weights.txt')
        return

    def evaluate(self, x_test, y_test):
        classifier:PhiliBertTextClassifier = PhiliBertTextClassifier.load('models/philiBERT/cf9074b4-a99c-4a3e-9f75-2a4127c666d5/best-model.pt')

        y_pred = classifier.predict(x_test)
        y_pred = np.argmax(y_pred)
        y_test = np.argmax(y_test)

        print('Test: {}'.format(np.unique(y_test)))
        print('Pred: {}'.format(np.unique(y_pred)))

        print()
        print('> Plotting confusion matrix...')
        cm = confusion_matrix(y_test, y_pred)
        disp = ConfusionMatrixDisplay(confusion_matrix=cm, display_labels=self.PREDICTOR_CLASSES)
        # disp = ConfusionMatrixDisplay(confusion_matrix=cm)
        disp.plot()
        plt.savefig(f'{self.model_directory}/model_confusion-matrix.png')
        
    def predict(self, review_ids:list, comments:list): 
        # classifier:PhiliBertTextClassifier = PhiliBertTextClassifier.load(self.model_directory + 'final-model.pt')
        # classifier:PhiliBertTextClassifier = PhiliBertTextClassifier.load(self.model_directory + 'best-model.pt')
        classifier:PhiliBertTextClassifier = PhiliBertTextClassifier.load('models/philiBERT/cf9074b4-a99c-4a3e-9f75-2a4127c666d5/best-model.pt')
        # classifier:PhiliBertTextClassifier = PhiliBertTextClassifier.load('models/philiBERT/9889b77c-61ec-44d7-9315-156866b95eaa/checkpoint.pt')

        sentence = Sentence("Non vraiment, je trouve que ce film d'animation n'est pas à la hauteur des prétentions affichées et surtout des possibilités du réalisateur. S'il arrive à reproduire l'âme d'Hergé à travers la musique et les couleurs...Les personnages sont trop caricaturées et l'intrigue mal menée..On s'ennuie presque. Entre film pour petits enfants et fusillades pour plus grands, ce film à rater sa cible et manque cruellement de grandeur! A voir tout de même mais à prendre pour ce qu'il est, un film commun!")
        classifier.predict(sentence)
        print()
        print('> Hard sentence prediction...')
        print(sentence.labels)
        print()
        print('> Making movie rating predictions...')
        classifier.predict(comments)

        # Prepare the predictions for output.
        print(comments[:5])
        print()
        print('> Matching \'review_id\' with predictions...')
        output = [(review_ids[ind], max(c.labels, key=lambda item: item.value).value.replace('.', ',')) for ind, c in enumerate(comments)]
        output.sort(reverse=False)

        # Save the results to disk.
        print()
        print('> Saving results to disk...')
        with open(self.PATH_RESULTS, 'wt') as f:
            for r, n in output:
                f.write('{} {}\n'.format(r, n))

        print()
        print('> Submitting results to evaluation platform...')
        au.submit_results_list(output)
        return
