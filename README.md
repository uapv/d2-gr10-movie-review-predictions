# Défi 2 - Sentiment Analysis

## Installation Steps

### pip

1. virtualenv venv
2. venv/Scripts/activate
3. python -m pip install -U pip
4. pip install -r requirements.txt

```bash
virtualenv venv
venv/Scripts/activate
python -m pip install -U pip
pip install -r requirements.txt
```

### Conda

1. conda create --name d2-bert;conda activate d2-bert
2. conda install -c huggingface tokenizers=0.10.1 datasets
3. conda install -c pytorch pytorch cudatoolkit=11.3.1
4. conda config --append channels conda-forge bioconda
4. conda install -c conda-forge fasttext
5. conda install pydot graphviz
6. pip install git+https://github.com/huggingface/transformers
7. Always check that GPU is enabled in PyTorch or TensorFlow on SLURM.
    - python -c 'import torch; print(torch.cuda.is_available())'
8. sbatch slurm.sh
9. install nvidia apex
   1.  /home/nas-wks01/users/uapv2101376/.conda/envs/d2-main/bin/pip install -v --disable-pip-version-check --no-cache-dir --global-option="--cpp_ext" --global-option="--cuda_ext" ./

### Download FastText Embeddings

```python
cd embeddings
python -c "import fasttext;fasttext.util.download_model('fr', if_exists='ignore')"
gzip -dk cc.fr.300.bin.gz
```

## Running the project

### Dataset Analysis

```bash
python stats.py
```

### Profiling Executing Time

#### Linux/UNIX

```bash
time python yourprogram.py
```

#### Windows (PowerShell)

```PowerShell
# WARNING: Measure-Command will not respect the UTF-8 encoding for the output...
(Measure-Command { python stats.py | Out-Default }).ToString()
(Measure-Command { python main.py --action train -tv bow-tfidf --type svm --corpus train --fast --corpus-subset 5000 | Out-Default }).ToString()
```

## References

- <https://fasttext.cc/docs/en/crawl-vectors.html>
- <https://arxiv.org/pdf/2105.01990.pdf>
- <https://chriskhanhtran.github.io/_posts/2020-02-01-cnn-sentence-classification/>
- <https://www.kaggle.com/vsmolyakov/keras-cnn-with-fasttext-embeddings/notebook>
- [OSError: Unable to open file (unable to lock file, errno = 37, error message = 'No locks available')](https://github.com/h5py/h5py/issues/1101)
- <https://stackabuse.com/python-for-nlp-working-with-facebook-fasttext-library/>
- <https://www.mastersindatascience.org/learning/statistics-data-science/undersampling/>
- <https://keras.io/guides/functional_api/>
- <https://fasttext.cc/docs/en/python-module.html#usage-overview>
- 
- **<https://towardsdatascience.com/text-classification-with-nlp-tf-idf-vs-word2vec-vs-bert-41ff868d1794>**
- <https://arxiv.org/pdf/1911.03894.pdf>
- <https://stackoverflow.com/questions/56166089/wor2vec-fine-tuning>
- <https://code.google.com/archive/p/word2vec/>
- Interesting Articles
  - **<https://github.com/TheophileBlard/french-sentiment-analysis-with-bert>**
  - <https://arxiv.org/pdf/1904.07531.pdf>
  - <https://hal.archives-ouvertes.fr/hal-02784740v3/document>
  - <https://arxiv.org/pdf/2105.01990.pdf>
  - <https://hal.inria.fr/LIA/hal-03426326v1>
- <https://www.kaggle.com/houssemayed/camembert-for-french-tweets-classification>
- <https://stackoverflow.com/questions/63633534/is-it-necessary-to-do-stopwords-removal-stemming-lemmatization-for-text-classif>
- **<https://colab.research.google.com/github/abhimishra91/transformers-tutorials/blob/master/transformers_multiclass_classification.ipynb#scrollTo=_TL-5nLzjmbt>**
- <https://medium.com/the-artificial-impostor/use-nvidia-apex-for-easy-mixed-precision-training-in-pytorch-46841c6eed8c>
- <https://pythonrepo.com/repo/NVIDIA-apex-python-gpu-utilities>
  - Removed `--global-option="--cpp_ext" --global-option="--cuda_ext"`
- <https://github.com/NVIDIA/apex>
- **<https://stackabuse.com/text-classification-with-bert-tokenizer-and-tf-2-0-in-python/>**
- <https://medium.com/@vitalshchutski/french-nlp-entamez-le-camembert-avec-les-librairies-fast-bert-et-transformers-14e65f84c148>