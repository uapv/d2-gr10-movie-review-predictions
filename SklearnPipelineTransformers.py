import pandas as pd
import numpy as np
from sklearn.base import BaseEstimator, TransformerMixin

import _01_dataset as ds

# SOURCE: https://stackoverflow.com/questions/41879130/adding-extra-features-to-bag-of-words-in-scikit-learn-pipeline-with-featureunion
# SOURCE: https://towardsdatascience.com/featureunion-columntransformer-pipeline-for-preprocessing-text-data-9dcb233dbcb6
class MovieGenres(BaseEstimator, TransformerMixin):

    def __init__(self):
        pass

    def fit(self, X, y=None):
        return self

    def transform(self, X:pd.DataFrame):
        genres = [ds.onehot_encode_genres(row['movie_genres'].split(',')) for index, row in X.iterrows()]
        return genres

class SentimentFeatures(BaseEstimator, TransformerMixin):

    def __init__(self):
        self.word_sentiment = ds.load_sentimental_lexicon(None, 'data/polarity_fr.txt', 'data/polarity.h5')
        pass

    def fit(self, X, y=None):
        return self

    def transform(self, X:pd.DataFrame):
        genres = [ds.get_sentiment_features(row['comment'], self.word_sentiment) for index, row in X.iterrows()]
        return genres

class MovieNumericValues(BaseEstimator, TransformerMixin):

    def __init__(self, key):
        self.key = key

    def fit(self, X, y=None):
        return self

    def transform(self, X:pd.DataFrame):
        return X[self.key].to_numpy().reshape(-1, 1)

class TextSelector(BaseEstimator, TransformerMixin):

    def __init__(self, key):
        self.key = key

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        return X[self.key]