import io, regex, os.path
import pandas
from bs4.element import Tag
import numpy as np
from xml.etree import ElementTree as ET
from pandas import read_hdf
from pandas.core.frame import DataFrame

import requests
import dateparser, math
from bs4 import BeautifulSoup

##############################################################################
# [SET RANDOM SEEDS]
##############################################################################
# SOURCE: https://pytorch.org/docs/stable/notes/randomness.html
# SOURCE: https://www.tensorflow.org/api_docs/python/tf/random/set_seed
RANDOM_STATE = 42

import random
random.seed(RANDOM_STATE)

import numpy as np
np.random.seed(RANDOM_STATE)

import torch
torch.manual_seed(RANDOM_STATE)

import tensorflow
tensorflow.random.set_seed(RANDOM_STATE)
##############################################################################

# Script Arguments and Choices
ARGS_CORPUS_DEV = 'dev'
ARGS_CORPUS_TRAIN = 'train'
ARGS_CORPUS_TEST = 'test'

# Data Paths
PATH_DEV = 'data/dev.xml'
PATH_DEV_CLEAN = 'data/dev-clean.h5'
PATH_DEV_CLEAN_AUGMENTED = 'data/dev-clean-aug.h5'
PATH_DEV_VOCAB = 'data/dev-vocab.h5'
PATH_TRAIN = 'data/train.xml'
PATH_TRAIN_CLEAN = 'data/train-clean.h5'
PATH_TRAIN_CLEAN_AUGMENTED = 'data/train-clean-aug.h5'
PATH_TRAIN_VOCAB = 'data/train-vocab.h5'
PATH_TEST = 'data/test.xml'
PATH_TEST_CLEAN = 'data/test-clean.h5'
PATH_TEST_CLEAN_AUGMENTED = 'data/test-clean-aug.h5'
PATH_TEST_VOCAB = 'data/test-vocab.h5'

DATA_FILES = {
    ARGS_CORPUS_DEV: (PATH_DEV, PATH_DEV_CLEAN, PATH_DEV_CLEAN_AUGMENTED, PATH_DEV_VOCAB),
    ARGS_CORPUS_TRAIN: (PATH_TRAIN, PATH_TRAIN_CLEAN, PATH_TRAIN_CLEAN_AUGMENTED, PATH_TRAIN_VOCAB),
    ARGS_CORPUS_TEST: (PATH_TEST, PATH_TEST_CLEAN, PATH_TEST_CLEAN_AUGMENTED, PATH_TEST_VOCAB)
}

GENRES = [
    'action',
    'animation',
    'arts martiaux',
    'aventure',
    'biopic',
    'bollywood',
    'classique',
    'comédie',
    'comédie dramatique',
    'comédie musicale',
    'concert',
    'dessin animé',
    'divers',
    'drama',
    'drame',
    'épouvante-horreur',
    'érotique',
    'espionnage',
    'événement sportif',
    'expérimental',
    'famille',
    'fantastique',
    'guerre',
    'historique',
    'judiciaire',
    'musical',
    'médical',
    'opéra',
    'policier',
    'péplum',
    'romance',
    'science fiction',
    'spectacle',
    'thriller',
    'western',
    # Exceptions
    'epouvante-horreur',
    'erotique',
    'sport event',
    'documentaire',
]

SENTIMENT_POS_IDX = 0
SENTIMENT_NEU_IDX = 1
SENTIMENT_NEG_IDX = 2

def clean_xml(in_path):
    with io.open(in_path, 'r', encoding='utf-8') as fr:
        contents = fr.read()

        # Escape XML special characters. (i.e. &)
        # Online RegEx Visualizer: https://regex101.com/r/EpsJM1/1/
        contents, count = regex.subn(r'&(?![a-z]+\;)', '&amp;', contents)

        return contents, count

def load_data(in_file, out_file):
    if (os.path.isfile(out_file)):
        df = read_hdf(out_file, 'data', mode='r')
        return df, None

    # Fix XML issues with unescaped characters (e.g. &).
    xml_data, syntax_errors = clean_xml(in_file)

    # Load the XML data into a XPath-able structure.
    root = ET.fromstring(xml_data)

    # Collect data into dictionary.
    data = {
        'review_id': [str(x.text) for x in root.iter('review_id')],
        'movie_id': [str(x.text) for x in root.iter('movie')],
        'user_id': [str(x.text) for x in root.iter('user_id')],
        'name': [str(x.text) for x in root.iter('name')],
        'note': [(float)(x.text.replace(',', '.')) for x in root.iter('note')],
        # Convert empty comments (None) to an empty string instead of 'None'.
        # 'comment': [str(x.text or '') for x in root.iter('commentaire')],
        'comment': [x.text for x in root.iter('commentaire')],
        'comment_length': [len(('' if x.text == None else x.text)) for x in root.iter('commentaire')]
    }

    # Special case for basic analysis of the 'test.xml' file.
    if (len(data['note']) == 0):
        data['note'] = np.zeros(len(data['movie_id']))

    # Build a Pandas DataFrame for the dataset.
    df = DataFrame(data)

    # Save the DataFrame in HDF5 for faster reload.
    df.to_hdf(out_file, key='data', complevel=9)

    # Remove outliers from dataset.
    # filtered = df[(df.comment_length < 10000)]
    # df = df[(df.comment_length < 5000) & (df.comment_length > 0)]

    return df, syntax_errors

def fetch_movie_metadata(movie_id:str):
    r = requests.get(f'https://www.allocine.fr/film/fichefilm_gen_cfilm={movie_id}.html')
    soup = BeautifulSoup(r.text, 'html.parser')
    movie = soup.find(class_='meta-body-item meta-body-info')

    if (movie is None):
        print(f'> Movie (id: {movie_id} doesn\'t exist in AlloCine...')
        print('> Checking in Internet Archive...')
        r = requests.get(f'https://web.archive.org/web/https://www.allocine.fr/film/fichefilm_gen_cfilm={movie_id}.html')
        soup = BeautifulSoup(r.text, 'html.parser')
        movie = soup.find(class_='meta-body-item meta-body-info')

        if (movie is None):
            print(f'> Movie (id: {movie_id} doesn\'t exist in Internet Archive...')
            return {
                'release_date': None,
                'duration_mins': None,
                'genres': ''
            }

    date = movie.find(class_='date')
    if not date is None:
        date = dateparser.parse(date.get_text().strip()).date()

    split = movie.find(class_='spacer').next_sibling.strip().split()
    if not split is None and len(split) == 2:
        duration = int(split[0].replace('h', '')) * 60 + int(split[1].replace('min', ''))
    else:
        duration = None

    genres = movie.find_all(class_='spacer')[-1].find_next_siblings('span')
    genres = ','.join([g.get_text().strip().lower() for g in genres])

    return {
        'release_date': date,
        'duration_mins': duration,
        'genres': genres
    }

def load_movie_metadata_file(movie_ids:np.array, out_file:str):
    if (os.path.isfile(out_file)):
        df = read_hdf(out_file, 'data', mode='r')
        return df

    print('Unique Movies: {}'.format(len(movie_ids)))
    
    movies = {}
    for movie_id in movie_ids:
        metadata = fetch_movie_metadata(movie_id)
        movies[movie_id] = metadata
        # time.sleep(0.025)

    df = pandas.DataFrame(movie_ids, columns=['movie_id'])
    df['movie_release_date'] = df.apply(lambda x: movies[x['movie_id']]['release_date'], axis=1)
    df['movie_release_year'] = df.apply(lambda x: movies[x['movie_id']]['release_date'].year if not x['movie_release_date'] is None else None, axis=1)
    df['movie_release_month'] = df.apply(lambda x: movies[x['movie_id']]['release_date'].month if not x['movie_release_date'] is None else None, axis=1)
    df['movie_release_day'] = df.apply(lambda x: movies[x['movie_id']]['release_date'].day if not x['movie_release_date'] is None else None, axis=1)
    df['movie_duration_mins'] = df.apply(lambda x: movies[x['movie_id']]['duration_mins'], axis=1)
    df['movie_genres'] = df.apply(lambda x: movies[x['movie_id']]['genres'], axis=1)

    df.to_hdf(out_file, key='data', complevel=9)

    return df

def filter_nan(value):
    return value if not math.isnan(value) else 0

def load_augmented_data(data:DataFrame, metadata:DataFrame, out_file:str):
    if (os.path.isfile(out_file)):
        df = read_hdf(out_file, 'data', mode='r')
        return df

    df = data.copy(deep=True)

    movie_genres = []
    movie_release_year = []
    movie_release_month = []
    movie_release_day = []
    movie_duration_mins = []
    for index, row in df.iterrows():
        movie_id = row['movie_id']
        movie = metadata.loc[metadata['movie_id'] == movie_id].iloc[0]

        movie_genres.append(
            movie['movie_genres']
        )
        movie_release_year.append(
            int(filter_nan(movie['movie_release_year']))
        )
        movie_release_month.append(
            int(filter_nan(movie['movie_release_month']))
        )
        movie_release_day.append(
            int(filter_nan(movie['movie_release_day']))
        )
        movie_duration_mins.append(
            int(filter_nan(movie['movie_duration_mins']))
        )

    df['movie_genres'] = movie_genres
    df['movie_release_year'] = movie_release_year
    df['movie_release_month'] = movie_release_year
    df['movie_release_day'] = movie_release_month
    df['movie_duration_mins'] = movie_duration_mins

    df.to_hdf(out_file, key='data', complevel=9)

    return df

def onehot_encode_genres(values:list):
    encoded = np.zeros((len(GENRES),))
    for v in values:
        if (v in GENRES):
            encoded[GENRES.index(v)] = 1
        else:
            print('Missing Value : {}'.format(v))
    return encoded


def get_sentiment_features(comment:str, word_sentiment:dict):
    features = [0,0,0]
    
    for token in comment.split(' '):
        #TODO: Fix N-gram issue where token is unigram, but word_sentiment is N-gram!
        if (token in word_sentiment):
            features[word_sentiment[token]] += 1

    return features

def load_sentimental_lexicon(data:DataFrame, in_file:str, out_file:str):
    if (os.path.isfile(out_file)):
        df = read_hdf(out_file, 'data', mode='r')
        return df

    word_sentiment = {}

    import csv
    with open(in_file, 'r', newline='\n') as csvfile:
        reader = csv.reader(csvfile, delimiter=';', quotechar='\"')
        for row in reader:
            # print(row)            
            if (len(row) == 5 and not row[0].startswith('//')):
                ngram = row[1]

                # Transform strings to int
                row[2:] = [int(x) for x in row[2:]]

                max_value = max(row[2:])
                if (max_value == row[2]):
                    word_sentiment[ngram] = SENTIMENT_POS_IDX
                elif (max_value == row[3]):
                    word_sentiment[ngram] = SENTIMENT_NEU_IDX
                elif (max_value == row[4]):
                    word_sentiment[ngram] = SENTIMENT_NEG_IDX

                # print('{} : {}'.format(ngram, word_sentiment[ngram]))

    return word_sentiment

def get_class_sample_tweaked(class_label:str, class_size:int, sample_size:int):
    class_label = str(class_label)

    computed = sample_size

    if (class_label in ['4.5', '4.0', '2.5', '2.0', '1.5']):
        computed = computed * 1.2
    elif (class_label in ['5.0', '3.0']):
        computed = computed * 1.1

    if (computed > class_size):
        computed = class_size

    print('[{}] Comp:{}\t(Class Size: {})'.format(class_label, computed, class_size))
    return int(computed)

def get_class_sample_uniform(class_label:str, class_size:int, sample_size:int):
    class_label = str(class_label)

    computed = sample_size

    if (computed > class_size):
        computed = class_size

    print('[{}] Comp:{}\t(Class Size: {})'.format(class_label, computed, class_size))
    return int(computed)

def get_class_sample_size(class_label:str, class_size:int, corpus_size:int, total_sample_size:int, tot_classes:int=10):
    class_label = str(class_label)

    # Use a class distribution ratio.
    selected = computed = int((class_size/corpus_size)*total_sample_size)

    # Set a fixed minimum value sampling size.
    min = int(total_sample_size / tot_classes)

    if (computed > class_size or min > class_size):
        selected = class_size
    elif(computed < min):
        selected = min

    print('[{}] Comp:{}\tMin:{}\tSel:{}\t(Class Size: {})'.format(class_label, computed, min, selected, class_size))
    return int(selected)

def get_dataset(
    data:DataFrame, 
    sampling_mode:str, 
    subset_size:int=2000, 
    tot_classes:int=10, 
    padding:bool=False,
    shuffle:bool=False,
):
    # Replace NULL comments with empty strings.
    if (padding):
        replace = '<pad>'
    else:
        replace = ''
    data['comment'] = data['comment'].apply(lambda x: x if not x in (None, '') else replace)

    if (sampling_mode == 'all'):
        shuffle = True
    elif (sampling_mode == 'uniform'):
        data = data.groupby(['note']).apply(lambda x: x.sample(
            get_class_sample_uniform(x.name, int(x['review_id'].count()), subset_size/tot_classes),
            random_state=RANDOM_STATE,
        ))
    elif (sampling_mode == 'tweaked'):
        data = data.groupby(['note']).apply(lambda x: x.sample(
            get_class_sample_tweaked(x.name, int(x['review_id'].count()), subset_size/tot_classes),
            random_state=RANDOM_STATE,
        ))
    elif (sampling_mode == 'ratio'):
        data = data.groupby(['note']).apply(lambda x: x.sample(
            get_class_sample_size(x.name, int(x['review_id'].count()), len(data), subset_size, tot_classes),
            random_state=RANDOM_STATE,
        ))
    
    if (shuffle):
        data = data.sample(frac=1, random_state=RANDOM_STATE)
    
    data = data.reset_index(drop=True)

    return data

##############################################################################
# [ENTRYPOINTS]
##############################################################################
if __name__ == "__main__":
    # Prepare and/or load datasets
    # df_dev, _ = load_data('data/dev.xml', 'data/dev-clean.h5')
    # df_train, _ = load_data('data/train.xml', 'data/train-clean.h5')
    # df_test, _ = load_data('data/test.xml', 'data/test-clean.h5')
    # df_all:DataFrame = pandas.concat([df_dev, df_train, df_test])

    # # Prepare and/or load movie metadata
    # df_movies = load_movie_metadata_file(
    #     df_all['movie_id'].unique(), 
    #     'data/movie-metadata.h5'
    # )

    # df_train_aug = load_augmented_data(df_train, df_movies, 'data/train-clean-aug.h5')
    # df_dev_aug = load_augmented_data(df_dev, df_movies, 'data/dev-clean-aug.h5')
    # df_test_aug = load_augmented_data(df_test, df_movies, 'data/test-clean-aug.h5')

    # # Print dataset and metadata columns
    # print(df_all.info())
    # print()
    # print(df_movies.info())
    # print()
    # print(df_dev_aug.info())

    df_sentiment = load_sentimental_lexicon(None, 'data/polarity_fr.txt', 'data/polarity.h5')

    # sample = df_all['movie_genres'].sample(1, random_state=42).to_list()[0]
    # print(sample)
    # print(onehot_encode_genres(sample.split(',')))

    # metadata = fetch_movie_metadata('262929')
    # metadata = fetch_movie_metadata('257053')
    # metadata = fetch_movie_metadata('254560')
    # print(metadata)