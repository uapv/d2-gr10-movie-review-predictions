import argparse
import regex
import re
import pandas as pd
from sklearn import preprocessing
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import TfidfVectorizer, strip_accents_unicode

import _01_dataset as ds
import auto_upload as au
from LinearSvmModel import LinearSvmModel
from LinearSvmBasicModel import LinearSvmBasicModel
from LinearSvmMetadataModel import LinearSvmMetadataModel
from LinearSvmSentimentModel import LinearSvmSentimentModel

##############################################################################
# [SET RANDOM SEEDS]
##############################################################################
# SOURCE: https://pytorch.org/docs/stable/notes/randomness.html
# SOURCE: https://www.tensorflow.org/api_docs/python/tf/random/set_seed
RANDOM_STATE = 42

import random
random.seed(RANDOM_STATE)

import numpy as np
np.random.seed(RANDOM_STATE)

import torch
torch.manual_seed(RANDOM_STATE)

import tensorflow
tensorflow.random.set_seed(RANDOM_STATE)
##############################################################################

# Script Arguments and Choices
ARGS_FEATURE_SET_1 = 'feature_set'
ARGS_FEATURE_SET_2 = 'fs'
ARGS_FEATURE_SET_BASIC = 'basic'
ARGS_FEATURE_SET_METADATA = 'metadata'
ARGS_FEATURE_SET_SENTIMENT = 'sentiment'
ARGS_CORPUS_SUBSET = 'corpus_subset'
ARGS_CORPUS_SUBSET_DEFAULT = 20000
ARGS_TEST_RATIO = 'test_ratio'
ARGS_TEST_RATIO_DEFAULT = 0.2
ARGS_MODEL_ALPHA = 'alpha'
ARGS_MODEL_ALPHA_DEFAULT = 1e-4
ARGS_MODEL_MAX_ITERATIONS_1 = 'max_iterations'
ARGS_MODEL_MAX_ITERATIONS_2 = 'mi'
ARGS_MODEL_MAX_ITERATIONS_DEFAULT=1000
ARGS_MODEL_PENALTY = 'penalty'
ARGS_MODEL_PENALTY_L1 = 'l1'
ARGS_MODEL_PENALTY_L2 = 'l2'
ARGS_MODEL_PENALTY_ELASTICNET = 'elasticnet'
ARGS_MODEL_LOSS = 'loss'
ARGS_MODEL_LOSS_HINGE = 'hinge'
ARGS_MODEL_LOSS_SQUARED_HINGE = 'squared-hinge'
ARGS_NGRAM_MIN = 'ngram_min'
ARGS_NGRAM_MAX = 'ngram_max'
ARGS_DF_MAX = 'df_max'
ARGS_DF_MIN = 'df_min'
ARGS_MAX_FEATURES = 'max_features'

def parse_cmdline_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description='Predict movies ratings (0.5 to 5.0 in 0.5 steps) from movie reviews.'
    )
    parser.add_argument(
        f'--{ARGS_FEATURE_SET_1}', f'-{ARGS_FEATURE_SET_2}',
        help='Determine which feature set to use for the model.',
        choices=[ARGS_FEATURE_SET_BASIC, ARGS_FEATURE_SET_METADATA, ARGS_FEATURE_SET_SENTIMENT], 
        # required=True
    )
    parser.add_argument(
        f'--{ARGS_CORPUS_SUBSET}', metavar='SIZE',
        type=int,
        nargs='?',
        help=f"Size of the subset used for faster training."
    )
    parser.add_argument(
        f'--{ARGS_TEST_RATIO}', metavar='RATIO',
        type=float,
        nargs='?',
        help=f"Ratio for the test split."
    )
    parser.add_argument(
        f'--{ARGS_MODEL_ALPHA}',
        type=float,
        nargs='?',
        help=f"Specify the model's alpha regularization term value.",
    )
    parser.add_argument(
        f'--{ARGS_MODEL_MAX_ITERATIONS_1}', f'-{ARGS_MODEL_MAX_ITERATIONS_2}',
        type=int,
        nargs='?',
        help=f"Specify the model's maximum iteration count.",
    )
    parser.add_argument(
        f'--{ARGS_MODEL_PENALTY}',
        choices=[ARGS_MODEL_PENALTY_L1, ARGS_MODEL_PENALTY_L2, ARGS_MODEL_PENALTY_ELASTICNET], 
        help=f"Specify the model's penalty (i.e. regularization method).",
    )
    parser.add_argument(
        f'--{ARGS_MODEL_LOSS}',
        choices=[ARGS_MODEL_LOSS_HINGE, ARGS_MODEL_LOSS_SQUARED_HINGE], 
        help=f"Specify the model's loss function.",
    )
    
    parser.add_argument(
        f'--{ARGS_NGRAM_MIN}',
        type=int,
        nargs='?',
        help=f"Minimum size of N-grams for TF-IDF."
    )
    parser.add_argument(
        f'--{ARGS_NGRAM_MAX}',
        type=int,
        nargs='?',
        help=f"Maximum size of N-grams for TF-IDF."
    )
    parser.add_argument(
        f'--{ARGS_DF_MIN}',
        type=float,
        nargs='?',
        help=f"Minimum size of document frequencies."
    )
    parser.add_argument(
        f'--{ARGS_DF_MAX}',
        type=float,
        nargs='?',
        help=f"Maximum size of document frequencies."
    )
    parser.add_argument(
        f'--{ARGS_MAX_FEATURES}',
        type=int,
        nargs='?',
        help=f"Maximum number of features kept in the TF-IDF vector."
    )

    args = parser.parse_args()

    return args

def get_stopwords():
    stop_words = set()
    stop_words.update(stopwords.words('french'))
    stop_words.update(stopwords.words('english'))
    # stop_words.update(stopwords.words('russian'))
    return [preprocess_item(s) for s in stop_words]

def preprocess_item(document:str):
    # document = strip_accents_unicode(document)
    document = strip_accents_unicode(document.lower()) # BEST!
    # document = document.lower()

    for expr, sub in LinearSvmModel.REGEX_REPLACEMENTS:
        # Wrap the tokens with spaces to ensure that they are not attached to
        # words.
        document = regex.sub(expr, f' {sub} ', document)

    # Trim whitespace down to a single space
    document = regex.sub(LinearSvmModel.REGEX_TRIM_SPACES, ' ', document)

    # Trim duplicate character sequences down to 2 characters
    document = re.sub(
        LinearSvmModel.REGEX_ELONGATED_SEQ,
        lambda x : x.group(0)[0]*2,
        document)

    return document

# IDEA: https://buhrmann.github.io/tfidf-analysis.html
def get_tfidf(
    ngram_min:int,
    ngram_max:int,
    df_min,
    df_max:float,
    max_features:int,
    vocabulary:dict=None,
):
    stop_words = get_stopwords()

    if (vocabulary is None):
        vectorizer = TfidfVectorizer(
            # The 'lowercase' and 'strip_accents' parameters are ignored when
            # 'preprocessor' is passed.
            # lowercase=True,
            # strip_accents='unicode', # {‘ascii’, ‘unicode’}
            ngram_range=(ngram_min, ngram_max),
            min_df=1, # Ignore too infrequent terms. 1
            max_df=df_max, # Ignore too frequent terms. 1.0
            max_features=max_features,
            token_pattern=r'(?u)\b\w+\b', # (?u)\b\w\w+\b
            stop_words=stop_words, 
            preprocessor=preprocess_item
        )
    else:
        vectorizer = TfidfVectorizer(
            lowercase=False,
            vocabulary=vocabulary
        )

    return vectorizer
    
# SOURCE: https://scikit-learn.org/stable/auto_examples/text/plot_document_classification_20newsgroups.html
def run_linear_svm(
    train:pd.DataFrame, 
    dev:pd.DataFrame, 
    test:pd.DataFrame, 
    feature_set:str, 
    corpus_subset_size:int,
    test_set_ratio:int,
    alpha:float,
    max_iterations:int,
    penalty:str,
    loss:str,
    ngram_min:int,
    ngram_max:int,
    df_min,
    df_max:float,
    max_features:int,
):
    vectorizer = get_tfidf(
        ngram_min,
        ngram_max,
        df_min,
        df_max,
        max_features,
    )

    if (feature_set == ARGS_FEATURE_SET_BASIC):
        model = LinearSvmBasicModel(
            vectorizer=vectorizer,
            alpha=alpha,
            max_iterations=max_iterations,
            penalty=penalty,
            loss=loss,
        )
    elif (feature_set == ARGS_FEATURE_SET_METADATA):
        model = LinearSvmMetadataModel(
            vectorizer=vectorizer,
            alpha=alpha,
            max_iterations=max_iterations,
            penalty=penalty,
            loss=loss,
        )
    elif (feature_set == ARGS_FEATURE_SET_SENTIMENT):
        model = LinearSvmSentimentModel(
            vectorizer=vectorizer,
            alpha=alpha,
            max_iterations=max_iterations,
            penalty=penalty,
            loss=loss,
        )
    
    print()
    print('-------------------------------------------------------------------------------')
    print('[DATA SAMPLING]')
    print('-------------------------------------------------------------------------------')
    if (corpus_subset_size < len(train)):
        sampling_mode = 'ratio'
        train_subset_size = corpus_subset_size
        dev_subset_size = corpus_subset_size * test_set_ratio
    else:
        sampling_mode = 'all'
        train_subset_size = len(train)
        dev_subset_size = len(dev) * test_set_ratio

    print('> Sample train split...')
    train = ds.get_dataset(train, sampling_mode=sampling_mode, subset_size=train_subset_size)
    print()
    print('> Sample dev split...')
    dev = ds.get_dataset(dev, sampling_mode=sampling_mode, subset_size=dev_subset_size)

    notes = train['note'].apply(lambda x: str(x))
    print()
    print('Note Distribution:')
    print(train['note'].describe())
    print('-------------------------------------------------------------------------------')

    
    print()
    print('-------------------------------------------------------------------------------')
    print('[INPUTS]')
    print('-------------------------------------------------------------------------------')
    # SOURCE: https://stackoverflow.com/questions/41925157/logisticregression-unknown-label-type-continuous-using-sklearn-in-python
    lab_enc = preprocessing.LabelEncoder()
    y_train = lab_enc.fit_transform(notes)
    y_dev = lab_enc.fit_transform(dev['note'])
    print('-------------------------------------------------------------------------------')


    print()
    print('-------------------------------------------------------------------------------')
    print('[TRAINING]')
    print('-------------------------------------------------------------------------------')
    model.train(train, y_train)
    print('-------------------------------------------------------------------------------')
    

    print()
    print('-------------------------------------------------------------------------------')
    print('[EVALUATION]')
    print('-------------------------------------------------------------------------------')
    model.evaluate(dev, y_dev)
    print('-------------------------------------------------------------------------------')


    print()
    print('-------------------------------------------------------------------------------')
    print(' [PREDICTIONS]')
    print('-------------------------------------------------------------------------------')
    print('> Sample test split...')
    test = ds.get_dataset(test, sampling_mode='all', subset_size=len(test))

    print()
    print('> Making movie rating predictions...')
    model.get_last_best()
    y_pred = model.predict(test)

    print()
    print('> Matching \'review_id\' with predictions...')
    review_ids = test['review_id'].tolist()
    output = [(review_ids[ind], LinearSvmModel.CAT_TO_NOTE[p]) for ind, p in enumerate(y_pred)]
    output.sort(reverse=False)

    print()
    print('> Saving results to disk...')
    with open(LinearSvmModel.PATH_RESULTS, 'wt') as f:
        for r, n in output:
            f.write('{} {}\n'.format(r, n))

    print()
    print('> Submitting results to evaluation platform...')
    au.submit_results_list(output)
    print('-------------------------------------------------------------------------------')
    return

##############################################################################
# [ENTRYPOINTS]
##############################################################################
if __name__ == "__main__":
    args = parse_cmdline_args()
    
    feature_set = args.feature_set
    corpus_subset_size = args.corpus_subset
    test_ratio = args.test_ratio
    alpha = args.alpha
    max_iterations = args.max_iterations
    penalty = args.penalty
    loss = args.loss
    ngram_min = args.ngram_min
    ngram_max = args.ngram_max
    df_min = args.df_min
    df_max = args.df_max
    max_features = args.max_features

    print()
    print('###############################################################################')
    print(' [PREDICT_NOTES]')
    print('###############################################################################')
    print('-------------------------------------------------------------------------------')
    print(' [ARGUMENTS]')
    print('-------------------------------------------------------------------------------')
    print(f'            Feature Set : {feature_set}')
    print(f'     Corpus Subset Size : {corpus_subset_size}')
    print(f'             Test Ratio : {test_ratio}')
    print(f'                  Alpha : {alpha}')
    print(f'     Maximum Iterations : {max_iterations}')
    print(f'                Penalty : {penalty}')
    print(f'          Loss Function : {loss}')
    print(f'        N-gram Min Size : {ngram_min}')
    print(f'        N-gram Max Size : {ngram_max}')
    print(f' Min Document Frequency : {df_min}')
    print(f' Max Document Frequency : {df_max}')
    print(f'           Max Features : {max_features}')
    print('-------------------------------------------------------------------------------')
    print()
    print('-------------------------------------------------------------------------------')
    print(' [DATASETS]')
    print('-------------------------------------------------------------------------------')
    print('> Loading train dataset...')
    data_raw_path, data_clean_path, data_clean_aug_path, data_vocab_path = ds.DATA_FILES[ds.ARGS_CORPUS_TRAIN]
    df_train, *_ = ds.load_data(data_raw_path, data_clean_aug_path)
    print()
    print('> Loading dev dataset...')
    data_raw_path, data_clean_path, data_clean_aug_path, _ = ds.DATA_FILES[ds.ARGS_CORPUS_DEV]
    df_dev, *_ = ds.load_data(data_raw_path, data_clean_aug_path)
    print()
    print('> Loading test dataset...')
    data_raw_path, data_clean_path, data_clean_aug_path, _ = ds.DATA_FILES[ds.ARGS_CORPUS_TEST]
    df_test, *_  = ds.load_data(data_raw_path, data_clean_aug_path)
    print('-------------------------------------------------------------------------------')

    run_linear_svm(
        df_train, 
        df_dev, 
        df_test,
        feature_set,
        corpus_subset_size,
        test_ratio,
        alpha=alpha,
        max_iterations=max_iterations,
        penalty=penalty,
        loss=loss,
        ngram_min=ngram_min,
        ngram_max=ngram_max,
        df_min=1,
        df_max=df_max,
        max_features=max_features,
    )
    print('###############################################################################')
    print()

    exit()