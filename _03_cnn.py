import torch
import regex
import argparse
import os
import re
import fasttext
import pandas as pd
import numpy as np
from collections import Counter
from nltk.corpus import stopwords
from tensorflow.keras.utils import to_categorical
from sklearn.feature_extraction.text import strip_accents_unicode

import _01_dataset as ds
import auto_upload as au
from CnnModel import CnnModel
from CnnBasicModel import CnnBasicModel

##############################################################################
# [SET RANDOM SEEDS]
##############################################################################
# SOURCE: https://pytorch.org/docs/stable/notes/randomness.html
# SOURCE: https://www.tensorflow.org/api_docs/python/tf/random/set_seed
RANDOM_STATE = 42

import random
random.seed(RANDOM_STATE)

import numpy as np
np.random.seed(RANDOM_STATE)

import torch
torch.manual_seed(RANDOM_STATE)

import tensorflow
tensorflow.random.set_seed(RANDOM_STATE)
##############################################################################

# Script Arguments and Choices
ARGS_FEATURE_SET_1 = 'feature_set'
ARGS_FEATURE_SET_2 = 'fs'
ARGS_FEATURE_SET_BASIC = 'basic'
ARGS_FEATURE_SET_METADATA = 'metadata'
ARGS_FEATURE_SET_SENTIMENT = 'sentiment'
ARGS_CORPUS_SUBSET = 'corpus_subset'
ARGS_CORPUS_SUBSET_DEFAULT = 20000
ARGS_TEST_RATIO = 'test_ratio'
ARGS_TEST_RATIO_DEFAULT = 0.2
ARGS_MAX_EPOCHS = 'max_epochs'
ARGS_BATCH_SIZE = 'batch_size'
ARGS_LEARNING_RATE_1 = 'learning_rate'
ARGS_LEARNING_RATE_2 = 'lr'
ARGS_CONVOLUTIONAL_FILTERS = 'filters'
ARGS_DROPOUT = 'dropout'
ARGS_TRAINABLE_EMBEDDINGS = 'trainable_embeddings'

PATH_RESULTS = 'results/results.txt'
PATH_EMBEDDINGS_TXT = 'embeddings/cc.fr.300.vec'
PATH_EMBEDDINGS_BIN = 'embeddings/cc.fr.300.bin'

INCLUSION_LIST = [
    '<pad>', 
    '<unk>', 
    '<tok_pos>', 
    '<tok_neg>', 
    '<tok_emp>',
]

def parse_cmdline_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description='Predict movies ratings (0.5 to 5.0 in 0.5 steps) from movie reviews.'
    )
    parser.add_argument(
        f'--{ARGS_FEATURE_SET_1}', f'-{ARGS_FEATURE_SET_2}',
        help='Determine which feature set to use for the model.',
        choices=[ARGS_FEATURE_SET_BASIC, ARGS_FEATURE_SET_METADATA, ARGS_FEATURE_SET_SENTIMENT], 
        default=ARGS_FEATURE_SET_BASIC
        # required=True
    )
    parser.add_argument(
        f'--{ARGS_CORPUS_SUBSET}', metavar='SIZE',
        type=int,
        nargs='?',
        default=ARGS_CORPUS_SUBSET_DEFAULT,
        help=f"Size of the subset used for faster training."
    )
    parser.add_argument(
        f'--{ARGS_TEST_RATIO}', metavar='RATIO',
        type=float,
        nargs='?',
        default=ARGS_TEST_RATIO_DEFAULT,
        help=f"Ratio for the test split."
    )
    
    parser.add_argument(
        f'--{ARGS_MAX_EPOCHS}', metavar='EPOCHS',
        type=int,
        nargs='?',
        help=f"Maximum number of epochs."
    )
    parser.add_argument(
        f'--{ARGS_BATCH_SIZE}',
        type=int,
        nargs='?',
        help=f"Batch size for the training."
    )
    parser.add_argument(
        f'--{ARGS_LEARNING_RATE_1}', f'-{ARGS_LEARNING_RATE_2}',
        type=float,
        nargs='?',
        help=f"Learning rate for the optimizer."
    )
    parser.add_argument(
        f'--{ARGS_CONVOLUTIONAL_FILTERS}',
        type=int,
        nargs='?',
        help=f"Number of filters in the convolutional layers."
    )
    parser.add_argument(
        f'--{ARGS_DROPOUT}',
        type=float,
        nargs='?',
        help=f"Ratio for the dropout layers."
    )
    parser.add_argument(
        f'--{ARGS_TRAINABLE_EMBEDDINGS}', action='store_true',
        help=f"Determines if the embeddings are kept trainable."
    )

    args = parser.parse_args()

    return args

def get_stopwords():
    with open('dictionaries/stopwords-min-fr.txt', 'r', encoding='utf-8') as f:
        stop_words = set([l.replace('\n', '') for l in f.readlines() if not l.startswith('#')])

    # stop_words = set()
    stop_words.update(stopwords.words('french'))
    stop_words.update(stopwords.words('english'))
    # stop_words.update(stopwords.words('russian'))
    return [preprocess_item(s) for s in stop_words]

def preprocess_item(document:str):
    # document = strip_accents_unicode(document)
    document = strip_accents_unicode(document.lower()) # BEST!
    # document = document.lower()

    for expr, sub in CnnModel.REGEX_REPLACEMENTS:
        # Wrap the tokens with spaces to ensure that they are not attached to
        # words.
        document = regex.sub(expr, f' {sub} ', document)

    # Trim whitespace down to a single space
    document = regex.sub(CnnModel.REGEX_TRIM_SPACES, ' ', document)

    # Trim duplicate character sequences down to 2 characters
    document = re.sub(
        CnnModel.REGEX_ELONGATED_SEQ,
        lambda x : x.group(0)[0]*2,
        document)

    return document

def tokenize(document:str, stop_words, vocabulary:dict=None):
    document = preprocess_item(document)

    # Tokenize
    # tokens = word_tokenize(comment, 'french')
    token_pattern = re.compile(r'(?u)<*\b\w+\b>*')
    tokens = re.findall(token_pattern, document)

    # Remove stop words
    # tokens = [t for t in tokens if (t not in stop_words) and (len(t) > 1) or (t in INCLUSION_LIST)]
    # tokens = [t for t in tokens if (t.isalpha() or t in INCLUSION_LIST)]
    tokens = [t for t in tokens if t not in stop_words]
    # tokens = [t for t in tokens if len(t) > 1]

    #TODO: Should we keep this?
    # Remove out of vocabulary words.
    if (vocabulary is not None):
        # # TODO: Display words that are out-of-vocabulary.
        # oov_tokens = [token for token in tokens if not token in vocabulary]

        # if (len(oov_tokens) > 0):
        #     print('Out-of-Vocabulary Tokens in Sequence: {}'.format(len(oov_tokens)))
        #     print('5 First Out-of-Vocabulary tokens:')
        #     print(oov_tokens[:5])

        tokens = [token for token in tokens if token in vocabulary]
        
    return tokens

def build_vocab(data:pd.DataFrame, vocabulary_path:str, stop_words:set):
    if (os.path.isfile(vocabulary_path)):
        print('> Existing vocabulary loaded from \'{}\'...'.format(vocabulary_path))
        df = pd.read_hdf(vocabulary_path, 'data', mode='r')
        final_vocab = dict(zip(df.key, df.value))
        max_tokens = df.max_tokens.max()
    else:
        print('> Building new vocabulary...')
        print()
        print('> Preprocessing and tokenizing comments...')
        tokenized_comments = [tokenize(comment, stop_words) for comment in data['comment'].to_list()]
        print()
        print('> Calculating maximum number of tokens per sequence...')
        token_counts = pd.DataFrame([len(tokens) for tokens in tokenized_comments], columns=['token_count'])
        # max_tokens = CnnModel.MAX_SEQUENCE_LEN
        # max_tokens = int(df.median()) # Much better accuracy!
        max_tokens = int(token_counts.quantile(q=0.999))
        print()
        print('Token Length Distribution:')
        print(token_counts.describe())

        vocab = set()
        for tokens in tokenized_comments:
            # When tokens from the inclusion list aren't removed, they corrupt
            # the indices for the final vocabulary making them fall out of
            # range.
            added = [t for t in tokens if t not in INCLUSION_LIST]
            vocab.update(added)

        # Transform to a dictionary.
        final_vocab = {k:v for v,k in enumerate(INCLUSION_LIST)}
        final_vocab.update({k: v for v, k in enumerate(vocab, len(final_vocab))})

        df = pd.DataFrame.from_dict(final_vocab, orient='index', columns=['value']).reset_index()
        df['max_tokens'] = max_tokens
        df.columns = ['key', 'value', 'max_tokens']
        df.to_hdf(vocabulary_path, key='data', complevel=9)

    # Save human-readable version of the vocabulary
    df.to_csv(vocabulary_path.replace('.h5', '.csv'), index=False)  
    
    print()
    print('Vocabulary Samples:')
    print(df)

    return final_vocab, max_tokens

def load_embeddings(vocabulary:dict, embeddings_path:str):
    # fasttext.util.download_model('fr', if_exists='ignore')
    ft = fasttext.load_model(embeddings_path)

    # Initilize random embeddings
    embeddings = np.random.uniform(-0.25, 0.25, (len(vocabulary), ft.get_dimension()))

    # Initialize the padding token to an origin (zero) vector
    embeddings[vocabulary['<pad>']] = np.zeros((ft.get_dimension(),))

    # Load pretrained vectors
    missing_words = Counter()
    for word, idx in vocabulary.items():
        # Don't generate FastText embeddings for some special tokens
        if (not word in ['<pad>']):
            if (ft.get_word_id(word) != -1):
                embeddings[idx] = ft.get_word_vector(word)
            else:
                missing_words.update([word])

    print()
    print('Embedding Size: {}'.format(ft.get_dimension()))
    print('Total Missing Words: {}'.format(len(missing_words)))
    print()
    print('Missing Words (word, count):')
    print(missing_words.most_common())

    return embeddings

def encode_tokens(tokenized_comments:list, vocabulary:dict, max_tokens:int):
    inputs = []
    for tokens in tokenized_comments:
        # Pad sentences/documents to MAX_SEQUENCE_LEN
        tokens += ['<pad>'] * (max_tokens - len(tokens))

        tokens = tokens[:max_tokens]

        # Encode tokens to their vocabulary indexes.
        input = [vocabulary.get(token) for token in tokens] # Out-of-vocabulary tokens already removed!
        # input = [vocabulary.get(token) if token in vocabulary else vocabulary.get('<unk>') for token in tokens]

        inputs.append(input)
        
    return inputs

def build_inputs(comments, vocabulary:dict, max_tokens:int, stop_words:set):
    print('> Preprocessing and tokenizing comments...')
    tokenized_comments = [tokenize(comment, stop_words, vocabulary) for comment in comments]

    # Encode tokens to their vocabulary indexes (and pad sequences)
    inputs = encode_tokens(tokenized_comments, vocabulary, max_tokens)

    return np.array(inputs), max_tokens

def run_cnn(
    train:pd.DataFrame, 
    dev:pd.DataFrame, 
    test:pd.DataFrame,
    stop_words:set, 
    vocabulary:pd.DataFrame,
    embeddings:np.ndarray,
    max_tokens:int,
    feature_set:str, 
    corpus_subset_size:int,
    test_set_ratio:int,
    max_epochs:int,
    batch_size:int,
    learning_rate:float,
    filters:int,
    dropout:float,
    trainable_embeddings:bool,
):
    if (feature_set == ARGS_FEATURE_SET_BASIC):
        model = CnnBasicModel(
            vocabulary, 
            embeddings, 
            max_tokens,
            max_epochs,
            batch_size,
            learning_rate,
            filters,
            dropout,
            trainable_embeddings,
        )
    elif (feature_set == ARGS_FEATURE_SET_METADATA):
        model = CnnBasicModel(
            vocabulary, 
            embeddings, 
            max_tokens,
            max_epochs,
            batch_size,
            learning_rate,
            filters,
            dropout,
            trainable_embeddings,
        )

    print()
    print('-------------------------------------------------------------------------------')
    print('[DATA SAMPLING]')
    print('-------------------------------------------------------------------------------')
    if (corpus_subset_size < len(train)):
        sampling_mode = 'uniform'
        train_subset_size = corpus_subset_size
        dev_subset_size = corpus_subset_size * test_set_ratio
    else:
        sampling_mode = 'all'
        train_subset_size = len(train)
        dev_subset_size = len(dev) * test_set_ratio

    print('> Sample train split...')
    train = ds.get_dataset(train, sampling_mode=sampling_mode, subset_size=train_subset_size)
    print()
    print('> Sample dev split...')
    dev = ds.get_dataset(dev, sampling_mode=sampling_mode, subset_size=dev_subset_size)

    notes_train = train['note'].apply(lambda x: str(x))
    notes_dev = dev['note'].apply(lambda x: str(x))
    print()
    print('Note Distribution:')
    print(train['note'].describe())
    print('-------------------------------------------------------------------------------')


    print()
    print('-------------------------------------------------------------------------------')
    print('[INPUTS]')
    print('-------------------------------------------------------------------------------')
    x_train, max_tokens = build_inputs(train['comment'], vocabulary, max_tokens, stop_words)
    y_train = to_categorical(notes_train.apply(lambda x: CnnBasicModel.PREDICTOR_CLASSES.index(x)), CnnBasicModel.NB_CLASSES)
    print()
    x_dev, _ = build_inputs(dev['comment'], vocabulary, max_tokens, stop_words)
    y_dev = to_categorical(notes_dev.apply(lambda x: CnnBasicModel.PREDICTOR_CLASSES.index(x)), CnnBasicModel.NB_CLASSES)
    print()
    print('Input Shape (train): {}'.format(x_train.shape))
    print('  Input Shape (dev): {}'.format(x_dev.shape))
    print()
    print('First 2 Inputs:')
    print(x_train[:2])
    print('-------------------------------------------------------------------------------')


    print()
    print('-------------------------------------------------------------------------------')
    print('[TRAINING]')
    print('-------------------------------------------------------------------------------')
    model.train(x_train, y_train, x_dev, y_dev)
    print('-------------------------------------------------------------------------------')
    

    print()
    print('-------------------------------------------------------------------------------')
    print('[EVALUATION]')
    print('-------------------------------------------------------------------------------')
    model.evaluate_last_best(x_dev, y_dev)
    print('-------------------------------------------------------------------------------')


    print()
    print('-------------------------------------------------------------------------------')
    print('[PREDICTIONS]')
    print('-------------------------------------------------------------------------------')
    print('> Sample test split...')
    test = ds.get_dataset(test, sampling_mode='all', subset_size=len(test))
    
    print()
    print('[INPUTS]')
    x_data, _ = build_inputs(test['comment'], vocabulary, max_tokens, stop_words)

    print()
    print('> Making movie rating predictions...')
    model = model.get_last_best()
    y_pred = model.predict(x_data)
    y_pred = np.argmax(y_pred, axis=1)

    print()
    print('> Matching \'review_id\' with predictions...')
    review_ids = test['review_id'].tolist()
    output = [(review_ids[ind], CnnBasicModel.CAT_TO_NOTE[p]) for ind, p in enumerate(y_pred)]
    output.sort(reverse=False)

    print()
    print('> Saving results to disk...')
    with open(PATH_RESULTS, 'wt') as f:
        for r, n in output:
            f.write('{} {}\n'.format(r, n))
    
    print()
    print('> Submitting results to evaluation platform...')
    au.submit_results_list(output)
    print('-------------------------------------------------------------------------------')

    return

##############################################################################
# [ENTRYPOINTS]
##############################################################################
if __name__ == "__main__":
    args = parse_cmdline_args()

    feature_set = args.feature_set
    corpus_subset_size = args.corpus_subset
    test_ratio = args.test_ratio
    max_epochs = args.max_epochs
    batch_size = args.batch_size
    learning_rate = args.learning_rate
    filters = args.filters
    dropout = args.dropout
    trainable_embeddings = args.trainable_embeddings

    print()
    print('###############################################################################')
    print(' [PREDICT_NOTES]')
    print('###############################################################################')
    print('-------------------------------------------------------------------------------')
    print(' [ARGUMENTS]')
    print('-------------------------------------------------------------------------------')
    print(f'             Feature Set : {feature_set}')
    print(f'      Corpus Subset Size : {corpus_subset_size}')
    print(f'              Test Ratio : {test_ratio}')
    print(f'              Max Epochs : {max_epochs}')
    print(f'              Batch Size : {batch_size}')
    print(f'           Learning Rate : {learning_rate}')
    print(f'                 Filters : {filters}')
    print(f'                 Dropout : {dropout}')
    print(f' Is Trainable Embeddings : {trainable_embeddings}')
    print('-------------------------------------------------------------------------------')
    print()
    print('-------------------------------------------------------------------------------')
    print(' [DATASETS]')
    print('-------------------------------------------------------------------------------')
    print('> Loading train dataset...')
    data_raw_path, data_clean_path, data_clean_aug_path, data_vocab_path = ds.DATA_FILES[ds.ARGS_CORPUS_TRAIN]
    df_train, *_ = ds.load_data(data_raw_path, data_clean_aug_path)
    print()
    print('> Loading dev dataset...')
    data_raw_path, data_clean_path, data_clean_aug_path, _ = ds.DATA_FILES[ds.ARGS_CORPUS_DEV]
    df_dev, *_ = ds.load_data(data_raw_path, data_clean_aug_path)
    print()
    print('> Loading test dataset...')
    data_raw_path, data_clean_path, data_clean_aug_path, _ = ds.DATA_FILES[ds.ARGS_CORPUS_TEST]
    df_test, *_  = ds.load_data(data_raw_path, data_clean_aug_path)
    print('-------------------------------------------------------------------------------')
    print()
    print('-------------------------------------------------------------------------------')
    print('[VOCABULARY]')
    print('-------------------------------------------------------------------------------')
    stop_words = get_stopwords()

    # df_vocab = df_train.copy(deep=True)
    df_vocab = pd.concat([df_train.copy(deep=True), df_dev.copy(deep=True)])
    df_vocab = ds.get_dataset(df_vocab, 'all', len(df_vocab))

    vocabulary, max_tokens = build_vocab(df_vocab, data_vocab_path, stop_words)
    print()
    print('    Vocabulary Size: {}'.format(len(vocabulary)))
    print('Max Sequence Length: {}'.format(max_tokens))
    print('-------------------------------------------------------------------------------')
    print()
    print('-------------------------------------------------------------------------------')
    print('[EMBEDDINGS]')
    print('-------------------------------------------------------------------------------')
    embeddings = load_embeddings(vocabulary, PATH_EMBEDDINGS_BIN)
    print('-------------------------------------------------------------------------------')
    run_cnn(
        df_train, 
        df_dev, 
        df_test,
        stop_words,
        vocabulary,
        embeddings,
        max_tokens,
        feature_set,
        corpus_subset_size,
        test_ratio,
        max_epochs,
        batch_size,
        learning_rate,
        filters,
        dropout,
        trainable_embeddings,
    )
    print('###############################################################################')
    print()

    exit()