import torch
import argparse
from flair.data import Sentence
import numpy as np
import pandas as pd
import io
import os
import string
import regex
import re
from sklearn.model_selection import train_test_split

import _01_dataset as ds
from PhiliBertModel import PhiliBertModel

##############################################################################
# [SET RANDOM SEEDS]
##############################################################################
# SOURCE: https://pytorch.org/docs/stable/notes/randomness.html
# SOURCE: https://www.tensorflow.org/api_docs/python/tf/random/set_seed
RANDOM_STATE = 42

import random
random.seed(RANDOM_STATE)

import numpy as np
np.random.seed(RANDOM_STATE)

import torch
torch.manual_seed(RANDOM_STATE)

import tensorflow
tensorflow.random.set_seed(RANDOM_STATE)
##############################################################################

# Script Arguments and Choices
ARGS_FEATURE_SET_1 = 'feature_set'
ARGS_FEATURE_SET_2 = 'fs'
ARGS_FEATURE_SET_BASIC = 'basic'
ARGS_FEATURE_SET_METADATA = 'metadata'
ARGS_FEATURE_SET_SENTIMENT = 'sentiment'
ARGS_CORPUS_SUBSET = 'corpus_subset'
ARGS_CORPUS_SUBSET_DEFAULT = 20000
ARGS_TEST_RATIO = 'test_ratio'
ARGS_TEST_RATIO_DEFAULT = 0.2
ARGS_MAX_EPOCHS = 'max_epochs'
ARGS_BATCH_SIZE = 'batch_size'
ARGS_LEARNING_RATE_1 = 'learning_rate'
ARGS_LEARNING_RATE_2 = 'lr'
ARGS_EPSILON = 'epsilon'
ARGS_MAX_SEQUENCE_LEN = 'max_sequence_len'
ARGS_USE_FP16 = 'use_fp16'
ARGS_AMP_LEVEL = 'amp_level'
ARGS_AMP_LEVEL_O1 = 'O1'
ARGS_AMP_LEVEL_O2 = 'O2'
ARGS_AMP_LEVEL_O3 = 'O3'
ARGS_BERT_MODEL = 'bert_model'

# Data Paths
PATH_DEV = 'data/dev.xml'
PATH_DEV_CLEAN = 'data/dev-clean.h5'
PATH_DEV_VOCAB = 'data/dev-vocab.h5'
PATH_TRAIN = 'data/train.xml'
PATH_TRAIN_CLEAN = 'data/train-clean.h5'
PATH_TRAIN_VOCAB = 'data/train-vocab.h5'
PATH_TEST = 'data/test.xml'
PATH_TEST_CLEAN = 'data/test-clean.h5'
PATH_TEST_VOCAB = 'data/test-vocab.h5'

ARGS_CORPUS_DEV = 'dev'
ARGS_CORPUS_TRAIN = 'train'
ARGS_CORPUS_TEST = 'test'

DATA_FILES = {
    ARGS_CORPUS_DEV: (PATH_DEV, PATH_DEV_CLEAN, PATH_DEV_VOCAB),
    ARGS_CORPUS_TRAIN: (PATH_TRAIN, PATH_TRAIN_CLEAN, PATH_TRAIN_VOCAB),
    ARGS_CORPUS_TEST: (PATH_TEST, PATH_TEST_CLEAN, PATH_TEST_VOCAB)
}

PATH_RESULTS = 'results/results.txt'

def parse_cmdline_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description='Predict movies ratings (0.5 to 5.0 in 0.5 steps) from movie reviews.'
    )
    parser.add_argument(
        f'--{ARGS_FEATURE_SET_1}', f'-{ARGS_FEATURE_SET_2}',
        help='Determine which feature set to use for the model.',
        choices=[ARGS_FEATURE_SET_BASIC, ARGS_FEATURE_SET_METADATA, ARGS_FEATURE_SET_SENTIMENT], 
        default=ARGS_FEATURE_SET_BASIC
        # required=True
    )
    parser.add_argument(
        f'--{ARGS_CORPUS_SUBSET}', metavar='SIZE',
        type=int,
        nargs='?',
        default=ARGS_CORPUS_SUBSET_DEFAULT,
        help=f"Size of the subset used for faster training."
    )
    parser.add_argument(
        f'--{ARGS_TEST_RATIO}', metavar='RATIO',
        type=float,
        nargs='?',
        default=ARGS_TEST_RATIO_DEFAULT,
        help=f"Ratio for the test split."
    )
    
    parser.add_argument(
        f'--{ARGS_MAX_EPOCHS}', metavar='EPOCHS',
        type=int,
        nargs='?',
        help=f"Maximum number of epochs."
    )
    parser.add_argument(
        f'--{ARGS_BATCH_SIZE}',
        type=int,
        nargs='?',
        help=f"Batch size for the training."
    )
    parser.add_argument(
        f'--{ARGS_LEARNING_RATE_1}', f'-{ARGS_LEARNING_RATE_2}',
        type=float,
        nargs='?',
        help=f"Learning rate for the optimizer."
    )
    parser.add_argument(
        f'--{ARGS_EPSILON}',
        type=float,
        nargs='?',
        help=f"Epsilon value for the optimizer."
    )
    parser.add_argument(
        f'--{ARGS_MAX_SEQUENCE_LEN}',
        type=int,
        nargs='?',
        help=f"Maximum length for sequences in the BERT model."
    )
    parser.add_argument(
        f'--{ARGS_USE_FP16}', action='store_true',
        help=f"Determines if the FP16 is used."
    )
    parser.add_argument(
        f'--{ARGS_AMP_LEVEL}',
        choices=[ARGS_AMP_LEVEL_O1, ARGS_AMP_LEVEL_O2, ARGS_AMP_LEVEL_O3], 
        help=f"Level of optimization for the FP16.",
    )
    parser.add_argument(
        f'--{ARGS_BERT_MODEL}',
        help=f"BERT model used for the contextual embeddings.",
    )

    args = parser.parse_args()

    return args

def preprocess_item(document:str):
    # CamemBERT is uncased.
    # document = strip_accents_unicode(document)
    # document = strip_accents_unicode(document.lower())
    # document = document.lower()

    for expr, sub in PhiliBertModel.REGEX_REPLACEMENTS:
        # Wrap the tokens with spaces to ensure that they are not attached to
        # words.
        document = regex.sub(expr, f' {sub} ', document)

    # Trim whitespace down to a single space
    document = regex.sub(PhiliBertModel.REGEX_TRIM_SPACES, ' ', document)

    # Trim duplicate character sequences down to 2 characters
    document = re.sub(
        PhiliBertModel.REGEX_ELONGATED_SEQ,
        lambda x : x.group(0)[0]*2,
        document)

    # Wrap punctuation characters in whitespace
    document = re.sub(
        r'[' + string.punctuation + ']',
        lambda x : ' {} '.format(x.group(0)),
        document)

    return document

def tokenize(document:str, stop_words, max_sequence_len:int):
    document = preprocess_item(document)

    # Tokenize
    from nltk.tokenize import word_tokenize
    tokens = word_tokenize(document, 'french')

    # Remove stop words
    tokens = [t for t in tokens if t not in stop_words]

    if (len(tokens) > max_sequence_len):
        return tokens[:max_sequence_len]
    else:
        return tokens if len(tokens) > 0 else ['<pad>']

def create_corpus(
    train:pd.DataFrame, 
    dev:pd.DataFrame, 
    stop_words:list, 
    max_sequence_len:int, 
    force:bool=False
):    
    train_path = 'data/train.fasttext'
    dev_path = 'data/dev.fasttext'
    test_path = 'data/test.fasttext'

    if (not force and os.path.isfile(train_path)):
        print('> Existing corpus from \'{}\'...'.format(train_path))
        return

    print('> Building new corpus...')

    train['train'] = train.apply(lambda x: 
        '__label__{} {}'.format(
            x['note'], 
            ' '.join(tokenize(x['comment'], stop_words, max_sequence_len))), 
        axis=1)
    
    dev['train'] = dev.apply(lambda x: 
        '__label__{} {}'.format(
            x['note'], 
            ' '.join(tokenize(x['comment'], stop_words, max_sequence_len))), 
        axis=1)

    # Flair will automatically split our train dataset to create a dev split
    # so we will use our dev as the test split
    test = dev

    y_train = train['note'].to_list()
    x_train, x_dev, _, _ = train_test_split(
        train['train'].to_list(), 
        y_train, 
        test_size=test_ratio, 
        shuffle=True,
        stratify=y_train,
        random_state=RANDOM_STATE,
    )

    with io.open(train_path, 'w', encoding='utf-8') as f:
        for i in x_train:
            f.write(i + '\n')
    with io.open(dev_path, 'w', encoding='utf-8') as f:
        for i in x_dev:
            f.write(i + '\n')
    with io.open(test_path, 'w', encoding='utf-8') as f:
        for i in test['train']:
            f.write(i + '\n')

def run_bert(
    train:pd.DataFrame, 
    dev:pd.DataFrame, 
    test:pd.DataFrame,
    stop_words:set, 
    feature_set:str, 
    corpus_subset_size:int,
    test_set_ratio:int,
    max_epochs:int,
    batch_size:int,
    learning_rate:float,
    epsilon:float,
    max_sequence_len:int,
    use_fp16:bool,
    amp_level:str,
    bert_model:str,
):
    if (feature_set == 'basic'):
        model = PhiliBertModel(
            max_epochs=max_epochs,
            batch_size=batch_size,
            learning_rate=learning_rate,
            epsilon=epsilon,
            max_sequence_len=max_sequence_len,
            use_fp16=use_fp16,
            amp_level=amp_level,
            bert_model=bert_model,
        )
    elif (feature_set == 'basic'):
        model = PhiliBertModel(
            max_epochs=max_epochs,
            batch_size=batch_size,
            learning_rate=learning_rate,
            epsilon=epsilon,
            max_sequence_len=max_sequence_len,
            use_fp16=use_fp16,
            amp_level=amp_level,
            bert_model=bert_model,
        )

    
    print()
    print('-------------------------------------------------------------------------------')
    print('[DATA SAMPLING]')
    print('-------------------------------------------------------------------------------')
    if (corpus_subset_size < len(train)):
        sampling_mode = 'tweaked'
        train_subset_size = corpus_subset_size
        dev_subset_size = corpus_subset_size * test_set_ratio
    else:
        sampling_mode = 'all'
        train_subset_size = len(train)
        dev_subset_size = len(dev) * test_set_ratio

    print('> Sample train split...')
    train = ds.get_dataset(train, sampling_mode=sampling_mode, subset_size=train_subset_size, shuffle=True)
    print()
    print('> Sample dev split...')
    dev = ds.get_dataset(dev, sampling_mode=sampling_mode, subset_size=dev_subset_size, shuffle=True)
    print()
    print('Note Distribution:')
    print(train['note'].describe())
    print('-------------------------------------------------------------------------------')


    # print()
    # print('-------------------------------------------------------------------------------')
    # print('[INPUTS]')
    # print('-------------------------------------------------------------------------------')
    # create_corpus(train, dev, stop_words, max_sequence_len, force=True)
    # # create_corpus(train, dev, stop_words, max_sequence_len, force=False)
    # print('-------------------------------------------------------------------------------')


    # print()
    # print('-------------------------------------------------------------------------------')
    # print('[TRAINING]')
    # print('-------------------------------------------------------------------------------')
    # model.train('data')

    dev_path = 'data/dev.fasttext'
    
    x_test = y_test = []
    with io.open(dev_path, 'r', encoding='utf-8') as f:
        for line in f.readlines():
            tokens = line.replace('\n', '').split()
            label = tokens[0].replace('__label__', '')
            sentence = Sentence(tokens[1:])
            x_test.append(sentence)
            y_test.append(label)

    model.evaluate(x_test, y_test)
    print('-------------------------------------------------------------------------------')


    # print()
    # print('-------------------------------------------------------------------------------')
    # print('[PREDICTIONS]')
    # print('-------------------------------------------------------------------------------')
    # print('> Sample test split...')
    # test = ds.get_dataset(test, sampling_mode='all', subset_size=len(test), padding=True)

    # comments = [Sentence(
    #     ' '.join(
    #         tokenize(comment, stop_words, max_sequence_len)
    #     )) 
    # for comment in test['comment']]

    # model.predict(test['review_id'].to_list(), comments)
    # print('-------------------------------------------------------------------------------')

##############################################################################
# [ENTRYPOINTS]
##############################################################################
if __name__ == "__main__":
    args = parse_cmdline_args()

    feature_set = args.feature_set
    corpus_subset_size = args.corpus_subset
    test_ratio = args.test_ratio
    max_epochs = args.max_epochs
    batch_size = args.batch_size
    learning_rate = args.learning_rate
    epsilon = args.epsilon
    max_sequence_len = args.max_sequence_len
    use_fp16 = args.use_fp16
    amp_level = args.amp_level
    bert_model = args.bert_model

    print()
    print('###############################################################################')
    print(' [PREDICT_NOTES]')
    print('###############################################################################')
    print('-------------------------------------------------------------------------------')
    print(' [ARGUMENTS]')
    print('-------------------------------------------------------------------------------')
    print(f'             Feature Set : {feature_set}')
    print(f'      Corpus Subset Size : {corpus_subset_size}')
    print(f'              Test Ratio : {test_ratio}')
    print(f'              Max Epochs : {max_epochs}')
    print(f'              Batch Size : {batch_size}')
    print(f'           Learning Rate : {learning_rate}')
    print(f'                 Epsilon : {epsilon}')
    print(f' Maximum Sequence Length : {max_sequence_len}')
    print(f'                Use FP16 : {use_fp16}')
    print(f'               AMP Level : {amp_level}')
    print(f'              BERT Model : {bert_model}')
    print('-------------------------------------------------------------------------------')
    print()
    print('-------------------------------------------------------------------------------')
    print(' [DATASETS]')
    print('-------------------------------------------------------------------------------')
    print('> Loading train dataset...')
    data_raw_path, data_clean_path, data_clean_aug_path, data_vocab_path = ds.DATA_FILES[ds.ARGS_CORPUS_TRAIN]
    df_train, *_ = ds.load_data(data_raw_path, data_clean_aug_path)
    print()
    print('> Loading dev dataset...')
    data_raw_path, data_clean_path, data_clean_aug_path, _ = ds.DATA_FILES[ds.ARGS_CORPUS_DEV]
    df_dev, *_ = ds.load_data(data_raw_path, data_clean_aug_path)
    print()
    print('> Loading test dataset...')
    data_raw_path, data_clean_path, data_clean_aug_path, _ = ds.DATA_FILES[ds.ARGS_CORPUS_TEST]
    df_test, *_  = ds.load_data(data_raw_path, data_clean_aug_path)
    print('-------------------------------------------------------------------------------')

    run_bert(
        df_train, 
        df_dev, 
        df_test,
        stop_words=set(),
        feature_set=feature_set,
        corpus_subset_size=corpus_subset_size,
        test_set_ratio=test_ratio,
        max_epochs=max_epochs,
        batch_size=batch_size,
        learning_rate=learning_rate,
        epsilon=epsilon,
        max_sequence_len=max_sequence_len,
        use_fp16=use_fp16,
        amp_level=amp_level,
        bert_model=bert_model,
    )

    print('###############################################################################')
    print()

    exit()

    # vocab = [
    #     'qwertyqwerty',
    #     'asdkasdk',
    #     'oiasfkj',
    #     '123123asa7i',
    #     'asd!+$%?_',
    #     'policiermondieu',
    #     'bonjour',
    #     'monde',
    #     'mauvais',
    #     'bon',
    # ]
    # vocabulary = {k:v for v,k in enumerate(INCLUSION_LIST)}
    # vocabulary.update({k: v for v, k in enumerate(vocab, len(vocabulary))})

    # stop_words = [st.preprocess_item(sw) for sw in st.get_stopwords()]
    # print(tokenize("Hey!!!! John Riichaaaaaard Booooond .......... explique le rôle de l'astronomie,,,,,,.", stop_words, 250))

    # exit()