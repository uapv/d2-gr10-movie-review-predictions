import regex
from pandas.core.series import Series
from pandas.core.frame import DataFrame

import _01_dataset as ds

REGEX_POSITIVE = r":\)+|:\-\)+"
REGEX_NEGATIVE = r":\(+|:\-\(+"
# REGEX_EMPHASIS = r"\!{2,}"
REGEX_EMPHASIS = r"\!+"

TOKEN_POSITIVE = '<tok_pos>'
TOKEN_NEGATIVE = '<tok_neg>'
TOKEN_EMPHASIS = '<tok_emp>'

PATH_DEV = 'data/dev.xml'
PATH_TRAIN = 'data/train.xml'
PATH_TEST = 'data/test.xml'
PATH_DEV_CLEAN = 'data/dev-clean.h5'
PATH_DEV_VOCAB = 'data/dev-vocab.h5'
PATH_TRAIN_CLEAN = 'data/train-clean.h5'
PATH_TRAIN_VOCAB = 'data/train-vocab.h5'
PATH_TEST_CLEAN = 'data/test-clean.h5'
PATH_TEST_VOCAB = 'data/test-vocab.h5'

DATA_FILES = {
    'dev': (PATH_DEV, PATH_DEV_CLEAN, PATH_DEV_VOCAB),
    'train': (PATH_TRAIN, PATH_TRAIN_CLEAN, PATH_TRAIN_VOCAB),
    'test': (PATH_TEST, PATH_TEST_CLEAN, PATH_TEST_VOCAB)
}

import stats_data as sd

def descriptive_analysis(df:DataFrame, in_file:str, syntax_errors:int, incl_correlation:bool=False, incl_distribution:bool=False):
    total_reviews = df.review_id.nunique()
    total_movies = df.movie_id.nunique()
    total_users = df.user_id.nunique()
    total_empty_comments = df.comment.isnull().sum()

    if (incl_correlation):
        # dfc = df.copy(deep=True)
        # # dfc['review_id'] = dfc['review_id'].count()
        # # dfc['movie_id'] = dfc.groupby(['movie_id'])['review_id'].transform('count')
        # # dfc['user_id'] = dfc.groupby(['user_id'])['review_id'].transform('count')
        # # dfc['name'] = dfc.groupby(['name'])['review_id'].transform('count')
        # # dfc['note'] = dfc.groupby(['note'])['review_id'].transform('count')
        # print(dfc.info())
        # df_corr = dfc.corr()['note'].drop('note')
        # golden_features_list = df_corr[abs(df_corr) > 0.0].sort_values(ascending=False)

        dfc, df_corr = sd.get_correlation_matrix(df, include_computed=False)
        df_corr = dfc.corr()['note'].drop('note')
        golden_features_list = df_corr[abs(df_corr) > 0.0].sort_values(ascending=False)

    if (incl_distribution):
        note_dist = next(iter(sd.get_distribution(df.note, False)))
        comment_len_dist = next(iter(sd.get_distribution(df.comment_length, False)))

    # Print statistical analyses for the dataset.
    print('--------------------------------------------------------------------------------')
    print('[DATASET ANALYSIS]')
    print('--------------------------------------------------------------------------------')
    print(df)
    print()
    print(f'Total Reviews     : {total_reviews}')
    print(f'XML Syntax Errors : {syntax_errors}')
    print()
    print('--------------------------------------------------------------------------------')
    print('[COLUMNS ANALYSIS]')
    print('--------------------------------------------------------------------------------')
    print(df.info())
    print()
    print('--------------------------------------------------------------------------------')
    print('[DATA ANALYSIS]')
    print('--------------------------------------------------------------------------------')
    print('Describe categorical columns :')
    print(df.describe(include=['O']).transpose())
    print()
    print('Describe numerical columns :')
    print(df.describe().transpose())
    if (incl_correlation):
        print()
        print('Describe numerical columns (+ distributions) :')
        print(dfc.describe().transpose())
    print()
    print('--------------------------------------------------------------------------------')
    print('[REVIEWS (review_id)]')
    print('--------------------------------------------------------------------------------')
    print()
    print('--------------------------------------------------------------------------------')
    print('[NOTES (note)]')
    print('--------------------------------------------------------------------------------')
    if (incl_distribution):
        print(f'Note Distribution : {note_dist}, Skewed Left (distplot/histogram)')
    print( 'Outliers          : Many outliers outide mustache. (boxplot)')
    print()
    print('--------------------------------------------------------------------------------')
    print('[FILMS (movie)]')
    print('--------------------------------------------------------------------------------')
    print(f'Unique Movies     : {total_movies}')
    print()
    print('--------------------------------------------------------------------------------')
    print('[USERS (user_id + name)]')
    print('--------------------------------------------------------------------------------')
    print(f'Unique Users      : {total_users}')
    print()
    print("Names by 'user_id' :")
    print(DataFrame(df.groupby(['movie_id'])['movie_id'].count().describe()).transpose())
    # unique_user_ids = df.groupby(['user_id', 'name'])['name'].unique()
    # unique_names = df['name'].unique()
    # print(unique_user_ids[:25])
    # print(unique_names[:25])

    # df2 = df.copy(deep=True)
    # df['count'] = df.groupby(['name'])['user_id'].transform('size')
    # print(df)
    # print(df[df['name'].str.contains('Kurosawa')])
    # print(df.groupby(['name'])['user_id'].transform('size').sort_values(ascending=False).head(10))
    print()
    print("Notes by 'user_id' and by 'name' :")
    print(df[['user_id', 'name']].groupby(df['note']).describe())
    print()
    print('Top 10 Mean Notes by users :')
    print(df[['user_id', 'name', 'note']].groupby(['note','name']).count().sort_values(by='user_id', ascending=False).head(10))
    print()
    print('--------------------------------------------------------------------------------')
    print('[COMMENTS (commentaire)]')
    print('--------------------------------------------------------------------------------')
    if (incl_distribution):
        print(f'Comment Lenth Distribution : {comment_len_dist}, Skewed Right (distplot/histogram)')
    print( 'Outliers                   : Many outliers outide mustache. (boxplot)')
    print(f'            Empty Comments : {total_empty_comments}')
    if (incl_correlation):
        print()
        print('--------------------------------------------------------------------------------')
        print('[RELATIONSHIPS and CORRELATION]')
        print('--------------------------------------------------------------------------------')
        print("There is {} correlated values with 'note':\n{}".format(len(golden_features_list), golden_features_list))
    return

def textual_analysis(df:DataFrame, dfv:DataFrame):
    sample_size = 15

    total_comments = len(df['comment'][~df['comment'].isin([None, ''])])
    comments_with_tok_pos = df['comment'][df['comment'].str.contains(REGEX_POSITIVE, na=False)].count()
    comments_with_tok_neg = df['comment'][df['comment'].str.contains(REGEX_NEGATIVE, na=False)].count()
    comments_with_tok_emp = df['comment'][df['comment'].str.contains(REGEX_EMPHASIS, na=False)].count()

    print()
    print('--------------------------------------------------------------------------------')
    print('[TEXTUAL FREQUENCY ANALYSIS]')
    print('--------------------------------------------------------------------------------')
    print('Special Meaning Tokens :')
    print(dfv[(dfv['token'].isin([TOKEN_POSITIVE, TOKEN_NEGATIVE, TOKEN_EMPHASIS]))])
    print()
    print('Comments with Special Meaning Tokens :')
    print('> TOK_POS : {}/{} ({:.2f})'.format(comments_with_tok_pos, total_comments, comments_with_tok_pos/total_comments))
    print('> TOK_NEG : {}/{} ({:.2f})'.format(comments_with_tok_neg, total_comments, comments_with_tok_neg/total_comments))
    print('> TOK_EMP : {}/{} ({:.2f})'.format(comments_with_tok_emp, total_comments, comments_with_tok_emp/total_comments))
    print()
    print('--------------------------------------------------------------------------------')
    print(f'[TOP {sample_size} LISTS]')
    print('--------------------------------------------------------------------------------')
    print(f'Most Frequent Words :')
    print(dfv.head(sample_size))
    print()
    print(f'Cyrillic Tokens :')
    print(dfv[(dfv['token'].str.match(r'[\u0400-\u04FF]'))].head(sample_size))
    return

def perform_descriptive_analysis(corpus:str):
    if (corpus == ds.ARGS_CORPUS_DEV):
        corpus_path = ds.PATH_DEV_CLEAN
    elif (corpus == ds.ARGS_CORPUS_TRAIN):
        corpus_path = ds.PATH_TRAIN_CLEAN

    print()
    print('###############################################################################')
    print(' [DESCRIPTIVE_ANALYSIS]')
    print('###############################################################################')
    print('-------------------------------------------------------------------------------')
    print(' [ARGUMENTS]')
    print('-------------------------------------------------------------------------------')
    print(f' Corpus : {corpus} ({corpus_path})')
    print('-------------------------------------------------------------------------------')
    print()

    #TODO: FIX BREAKING CHANGES INTRODUCED BY MAJOR REFACTOR!
    df = ds.load_augmented_data(None, None, corpus_path)
    descriptive_analysis(df, '', 0)
    textual_analysis()

    print('###############################################################################')
    print()
    return

##############################################################################
# [MAIN APPLICATION]
##############################################################################
if __name__ == "__main__": 
    exit