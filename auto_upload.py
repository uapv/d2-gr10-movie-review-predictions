import requests;
from bs4 import BeautifulSoup
from io import BytesIO, BufferedIOBase

def parse_score(response:str):
    soup = BeautifulSoup(response, 'html.parser')
    scores = soup.findAll('blockquote')
    if (len(scores) > 0):
        scores = scores[0]
        for br in scores.findAll('br'):
            br.replace_with('\n')
        return scores.text
    else:
        return 'ERROR: No scores found...'
        
def submit_results_path(results_path:str):
    submit_results_internal(open(results_path,'rb'))

def submit_results_list(results:list):
    # Create a buffer object that can be read by the MultipartEncoder class
    # This works just like an open file object
    with BytesIO() as file:
        # The file content will be simple for my test.
        # But you could just as easily have a multi-megabyte mpg file
        # Write the contents to the file
        for r, n in results:
            file.write(bytes('{} {}\n'.format(r, n), encoding='utf-8'))

        # Then seek to the beginning of the file so that the
        # MultipartEncoder can read it from the beginning
        file.seek(0)

        submit_results_internal(file)
    
def submit_results_internal(results_file:BufferedIOBase):
    r = requests.post(
        'http://mickael-rouvier.fr/eval_2021/index.php', 
        data={
            'utilisateur': 'G10_Maazouz-Turcotte',
            'submit': 'Upload Image'
        }, 
        files={
            'fileToUpload': (
                'fileToUpload', 
                results_file, 
                'application/octet-stream',
            )
        },
    )

    if r.status_code != 200:
        print('sendErr: '+r.url)
    else :
        print(parse_score(r.content))

##############################################################################
# [ENTRYPOINTS]
##############################################################################
if __name__ == "__main__":
    submit_results_list([('test', '0,5')])
    exit()