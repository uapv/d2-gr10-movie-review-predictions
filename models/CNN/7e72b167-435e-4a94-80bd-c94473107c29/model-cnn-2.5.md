# Model CNN 2.5

## Results

### Leaderboard

```powershell
G10_Maazouz-Turcotte	83.17%	40.45%	51.3%	21.19%	49.5%	33.19%	42.58%	25.96%	39.14%	19.25%	27.99%	55.26%
```

### Execution

```powershell
time python main.py --action train -tv bow-tfidf --type cnn --corpus train

My first sbatch script!
True

###############################################################################
 [TRAIN_MODEL]
###############################################################################
-------------------------------------------------------------------------------
 [ARGUMENTS]
-------------------------------------------------------------------------------
             Corpus : train (data/train.xml)
    Text Vectorizer : bow-tfidf
         Model Type : cnn
        Is fast run : False
 Corpus Subset Size : 1000
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 [TRAINING]
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
[VOCABULARY]
-------------------------------------------------------------------------------
> Existing vocabulary loaded from 'data/train-vocab.h5'...

Vocabulary Samples:
                  key   value
0               <pad>       0
1               <unk>       1
2         __tok_pos__       2
3         __tok_neg__       3
4         __tok_emp__       4
...               ...     ...
251613  inconstatable  251613
251614       selvajes  251614
251615    délivrerait  251615
251616      sarcastic  251616
251617  labyrinthique  251617

[251618 rows x 2 columns]

Vocabulary Size: 251618
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
[EMBEDDINGS]
-------------------------------------------------------------------------------

Embedding Size: 300
Total Missing Words: 626

Missing Words (word, count):
[('e', 104886), ('a', 81323), ('i', 74967), ('r', 68056), ('n', 67300), ('s', 62161), ('t', 57544), ('o', 56814), ('l', 45808), ('u', 38373), ('c', 32342), ('m', 29285), ('d', 24862), ('h', 22577), ('p', 21616), ('é', 19132), ('g', 18249), ('b', 17804), ('f', 13282), ('v', 9950), ('k', 9537), ('y', 8950), ('è', 6911), ('q', 5457), ('w', 5130), ('z', 5115), ('j', 4049), ('x', 3656), ('0', 1693), ('2', 1535), ('_', 1436), ('1', 1401), ('3', 938), ('9', 769), ('â', 536), ('7', 527), ('ï', 523), ('ç', 507), ('ê', 474), ('8', 471), ('4', 414), ('ô', 385), ('5', 381), ('î', 341), ('6', 274), ('à', 270), ('û', 147), ('ö', 145), ('ė', 134), ('ü', 127), ('ë', 97), ('á', 94), ('ó', 84), ('장', 82), ('ä', 81), ('ù', 63), ('í', 62), ('출', 56), ('흥', 39), ('ñ', 37), ('的', 37), ('œ', 32), ('²', 30), ('ø', 30), ('ō', 25), ('ú', 23), ('ョ', 18), ('ã', 17), ('是', 16), ('ĺ', 16), ('성', 16), ('ì', 16), ('𝙀', 15), ('ò', 15), ('一', 14), ('ŕ', 14), ('보', 14), ('고', 13), ('마', 13), ('ا', 13), ('了', 13), ('å', 13), ('샵', 13), ('ı', 13), ('화', 11), ('순', 11), ('ń', 11), ('ʼ', 10), ('部', 10), ('æ', 10), ('가', 10), ('ć', 10), ('𝙎', 9), ('콜', 9), ('걸', 9), ('𝙊', 9), ('ر', 9), ('片', 9), ('ś', 9), ('남', 9), ('지', 9), ('만', 8), ('ð', 8), ('𝙍', 8), ('不', 8), ('사', 8), ('안', 7), ('这', 7), ('õ', 7), ('ł', 7), ('ǝ', 7), ('看', 7), ('𝙇', 6), ('𝙐', 6), ('人', 6), ('电', 6), ('影', 6), ('격', 6), ('모', 6), ('텔', 6), ('š', 6), ('ū', 6), ('카', 6), ('톡', 6), ('就', 6), ('和', 5), ('³', 5), ('ş', 5), ('시', 5), ('时', 5), ('有', 5), ('ɐ', 5), ('𝙄', 5), ('最', 5), ('ˋ', 5), ('ی', 5), ('情', 5), ('𝙏', 5), ('还', 4), ('조', 4), ('건', 4), ('ý', 4), ('ğ', 4), ('个', 4), ('小', 4), ('ď', 4), ('于', 4), ('国', 4), ('给', 4), ('结', 4), ('无', 4), ('ل', 4), ('م', 4), ('س', 4), ('同', 4), ('아', 4), ('씨', 4), ('过', 4), ('ت', 4), ('渐', 4), ('O', 4), ('𝙋', 4), ('些', 3), ('𝘼', 3), ('前', 3), ('对', 3), ('업', 3), ('č', 3), ('ă', 3), ('𝙁', 3), ('𝘿', 3), ('分', 3), ('特', 3), ('意', 3), ('演', 3), ('ē', 3), ('都', 3), ('星', 3), ('ɹ', 3), ('爱', 3), ('た', 3), ('子', 3), ('い', 3), ('狗', 3), ('þ', 3), ('次', 3), ('面', 3), ('几', 3), ('美', 3), ('大', 3), ('候', 3), ('自', 3), ('己', 3), ('在', 3), ('T', 3), ('K', 3), ('怪', 3), ('里', 3), ('𝘽', 3), ('ک', 3), ('ÿ', 3), ('ɯ', 3), ('血', 2), ('𝙑', 2), ('𝙉', 2), ('の', 2), ('半', 2), ('此', 2), ('法', 2), ('缺', 2), ('也', 2), ('高', 2), ('靠', 2), ('多', 2), ('소', 2), ('西', 2), ('风', 2), ('死', 2), ('ż', 2), ('很', 2), ('遗', 2), ('打', 2), ('享', 2), ('得', 2), ('绘', 2), ('声', 2), ('色', 2), ('后', 2), ('出', 2), ('野', 2), ('做', 2), ('什', 2), ('么', 2), ('所', 2), ('能', 2), ('π', 2), ('ρ', 2), ('ه', 2), ('½', 2), ('ī', 2), ('应', 2), ('该', 2), ('主', 2), ('ό', 2), ('昏', 2), ('句', 2), ('话', 2), ('说', 2), ('正', 2), ('ع', 2), ('و', 2), ('暴', 2), ('力', 2), ('外', 2), ('맛', 2), ('狄', 2), ('あ', 2), ('り', 2), ('𝘾', 2), ('ま', 2), ('其', 2), ('剧', 2), ('杰', 2), ('之', 2), ('神', 2), ('王', 2), ('ā', 2), ('复', 2), ('活', 2), ('场', 2), ('起', 2), ('پ', 2), ('ž', 2), ('ب', 2), ('ň', 2), ('把', 2), ('哥', 2), ('制', 2), ('麻', 2), ('们', 2), ('ǁ', 2), ('会', 2), ('断', 2), ('续', 2), ('µ', 2), ('ล', 2), ('ᵉ', 2), ('산', 2), ('克', 2), ('曼', 2), ('身', 2), ('为', 2), ('观', 2), ('众', 2), ('戏', 2), ('E', 2), ('P', 2), ('难', 2), ('没', 2), ('ن', 2), ('画', 2), ('拍', 2), ('精', 2), ('良', 2), ('ε', 2), ('ύ', 2), ('ς', 2), ('如', 2), ('短', 2), ('束', 2), ('ح', 2), ('っ', 2), ('đ', 2), ('社', 2), ('区', 2), ('单', 2), ('好', 2), ('已', 2), ('年', 2), ('带', 1), ('着', 1), ('腥', 1), ('懵', 1), ('懂', 1), ('ص', 1), ('غ', 1), ('世', 1), ('界', 1), ('频', 1), ('繁', 1), ('交', 1), ('错', 1), ('ķ', 1), ('勾', 1), ('回', 1), ('忆', 1), ('比', 1), ('虽', 1), ('然', 1), ('荒', 1), ('诞', 1), ('雅', 1), ('谱', 1), ('采', 1), ('慢', 1), ('오', 1), ('피', 1), ('ⅳ', 1), ('종', 1), ('구', 1), ('¹', 1), ('憾', 1), ('底', 1), ('别', 1), ('那', 1), ('段', 1), ('念', 1), ('潮', 1), ('尾', 1), ('乎', 1), ('料', 1), ('魔', 1), ('女', 1), ('可', 1), ('少', 1), ('性', 1), ('发', 1), ('送', 1), ('反', 1), ('馈', 1), ('谓', 1), ('以', 1), ('去', 1), ('α', 1), ('τ', 1), ('ὴ', 1), ('李', 1), ('宝', 1), ('田', 1), ('ف', 1), ('د', 1), ('团', 1), ('圆', 1), ('局', 1), ('ง', 1), ('บ', 1), ('欲', 1), ('睡', 1), ('确', 1), ('ة', 1), ('곡', 1), ('효', 1), ('진', 1), ('加', 1), ('恐', 1), ('怖', 1), ('무', 1), ('명', 1), ('欧', 1), ('律', 1), ('刻', 1), ('私', 1), ('は', 1), ('な', 1), ('よ', 1), ('も', 1), ('𝙂', 1), ('ʌ', 1), ('が', 1), ('と', 1), ('う', 1), ('ご', 1), ('ざ', 1), ('し', 1), ('余', 1), ('聊', 1), ('仁', 1), ('龙', 1), ('关', 1), ('ß', 1), ('撑', 1), ('ộ', 1), ('ج', 1), ('ﻧ', 1), ('ز', 1), ('近', 1), ('粗', 1), ('俗', 1), ('蛮', 1), ('露', 1), ('ס', 1), ('ר', 1), ('ט', 1), ('冷', 1), ('丁', 1), ('枪', 1), ('习', 1), ('惯', 1), ('题', 1), ('令', 1), ('讨', 1), ('厌', 1), ('老', 1), ('造', 1), ('烦', 1), ('朋', 1), ('友', 1), ('想', 1), ('往', 1), ('事', 1), ('계', 1), ('제', 1), ('위', 1), ('ร', 1), ('ะ', 1), ('或', 1), ('许', 1), ('它', 1), ('魅', 1), ('일', 1), ('광', 1), ('굿', 1), ('ⅱ', 1), ('외', 1), ('인', 1), ('한', 1), ('尼', 1), ('基', 1), ('德', 1), ('技', 1), ('本', 1), ('中', 1), ('M', 1), ('院', 1), ('毁', 1), ('创', 1), ('下', 1), ('丰', 1), ('功', 1), ('伟', 1), ('绩', 1), ('黄', 1), ('海', 1), ('节', 1), ('盘', 1), ('屎', 1), ('S', 1), ('ⁿ', 1), ('另', 1), ('真', 1), ('ي', 1), ('算', 1), ('亮', 1), ('点', 1), ('吗', 1), ('ầ', 1), ('浪', 1), ('费', 1), ('两', 1), ('烂', 1), ('گ', 1), ('除', 1), ('摄', 1), ('作', 1), ('ę', 1), ('门', 1), ('ο', 1), ('μ', 1), ('η', 1), ('θ', 1), ('俳', 1), ('谋', 1), ('杀', 1), ('木', 1), ('现', 1), ('扩', 1), ('充', 1), ('称', 1), ('动', 1), ('雷', 1), ('奥', 1), ('兽', 1), ('般', 1), ('地', 1), ('ř', 1), ('达', 1), ('蒙', 1), ('再', 1), ('奇', 1), ('迹', 1), ('只', 1), ('신', 1), ('מ', 1), ('ע', 1), ('ו', 1), ('ל', 1), ('ה', 1), ('メ', 1), ('ア', 1), ('リ', 1), ('ƃ', 1), ('ɥ', 1), ('彩', 1), ('犯', 1), ('罪', 1), ('델', 1), ('루', 1), ('와', 1), ('ŝ', 1), ('𝙈', 1), ('𝙃', 1), ('서', 1), ('비', 1), ('스', 1), ('ě', 1), ('박', 1), ('춘', 1), ('배', 1), ('く', 1), ('じ', 1), ('ビ', 1), ('ジ', 1), ('ン', 1), ('を', 1), ('持', 1), ('て', 1), ('る', 1), ('ζ', 1), ('ช', 1), ('า', 1), ('ต', 1), ('历', 1), ('史', 1), ('记', 1), ('录', 1), ('弹', 1), ('穿', 1), ('脑', 1), ('袋', 1), ('渲', 1), ('染', 1), ('ك', 1), ('来', 1), ('幸', 1), ('我', 1), ('觉', 1), ('经', 1), ('ế', 1), ('¾', 1), ('ĩ', 1), ('保', 1), ('存', 1), ('今', 1), ('早', 1), ('巴', 1), ('黎', 1), ('展', 1), ('览', 1), ('二', 1), ('十', 1), ('他', 1), ('ɟ', 1), ('格', 1), ('物', 1), ('设', 1), ('计', 1), ('灰', 1), ('暗', 1), ('天', 1), ('気', 1), ('胧', 1), ('月', 1), ('N', 1), ('G', 1), ('家', 1), ('衛', 1), ('ʇ', 1), ('实', 1), ('ȏ', 1), ('坨', 1), ('翔', 1), ('ⅰ', 1), ('像', 1), ('成', 1), ('长', 1), ('𝙕', 1)]
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
[DATA SAMPLING]
-------------------------------------------------------------------------------

Note Distribution:
count    766362.000000
mean          3.215005
std           1.263355
min           0.500000
25%           2.500000
50%           3.500000
75%           4.000000
max           5.000000
Name: note, dtype: float64
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
[INPUTS]
2021-12-11 22:29:14.471410: I tensorflow/core/platform/cpu_feature_guard.cc:151] This TensorFlow binary is optimized with oneAPI Deep Neural Network Library (oneDNN) to use the following CPU instructions in performance-critical operations:  AVX2 AVX512F FMA
To enable them in other operations, rebuild TensorFlow with the appropriate compiler flags.
2021-12-11 22:29:15.170419: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1525] Created device /job:localhost/replica:0/task:0/device:GPU:0 with 15397 MB memory:  -> device: 0, name: Tesla P100-PCIE-16GB, pci bus id: 0000:3b:00.0, compute capability: 6.0
2021-12-11 22:29:16.489415: W tensorflow/core/framework/cpu_allocator_impl.cc:82] Allocation of 1953301200 exceeds 10% of free system memory.
2021-12-11 22:29:18.528486: W tensorflow/core/framework/cpu_allocator_impl.cc:82] Allocation of 1953301200 exceeds 10% of free system memory.
2021-12-11 22:29:21.476636: I tensorflow/stream_executor/cuda/cuda_dnn.cc:366] Loaded cuDNN version 8200
-------------------------------------------------------------------------------
Token Length Distribution:
         token_count
count  766362.000000
mean       55.603869
std        72.390854
min         0.000000
25%        17.000000
50%        32.000000
75%        64.000000
max      2697.000000

Max Sequence Length: 354
Input Shape: (766362, 354)

First 2 Inputs:
[[233428  99083 109016 116888 194548  85639  84730  82101 204176 251148
   82256    208 234644  19929 116888  77979 214991 157993 111045 122588
   15650  41942 208121 210605  22173 143629 205478 212539 171249 210479
   93686  16962 221730  18474  97896  43835 109016 178535  10416  66167
   19776  66106  71870 190967  72983 108595    208 233748 119719 233094
  164722 134547 218459 161937 193710 193288 183454 187679 116888  23806
   88899 185106  97715  26737  74605  37219  75585 104239  19068 116888
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0]
 [ 68934 128970 149881  93686 247811  50216 243704 131682 241497  97896
   34873  67774  28508      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0]]

> Splitting data and labels into train/test sets.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
[TRAINING]
-------------------------------------------------------------------------------
Epoch 1/10

Epoch 00001: val_categorical_accuracy improved from -inf to 0.40838, saving model to models/7e72b167-435e-4a94-80bd-c94473107c29/model.b256-mt354-vacc0.4084.h5
2695/2695 - 1272s - loss: 1.6019 - categorical_accuracy: 0.3806 - val_loss: 1.4747 - val_categorical_accuracy: 0.4084 - 1272s/epoch - 472ms/step
Epoch 2/10

Epoch 00002: val_categorical_accuracy improved from 0.40838 to 0.41160, saving model to models/7e72b167-435e-4a94-80bd-c94473107c29/model.b256-mt354-vacc0.4116.h5
2695/2695 - 1261s - loss: 1.3845 - categorical_accuracy: 0.4447 - val_loss: 1.4746 - val_categorical_accuracy: 0.4116 - 1261s/epoch - 468ms/step
Epoch 3/10

Epoch 00003: val_categorical_accuracy did not improve from 0.41160
2695/2695 - 1258s - loss: 1.2276 - categorical_accuracy: 0.5103 - val_loss: 1.5696 - val_categorical_accuracy: 0.4018 - 1258s/epoch - 467ms/step
Warning : `load_model` does not return WordVectorModel or SupervisedModel any more, but a `FastText` object which is very similar.
Epoch 00003: early stopping
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
[EVALUATION]
-------------------------------------------------------------------------------
Test: [0 1 2 3 4 5 6 7 8 9]
Pred: [0 1 2 3 4 5 6 7 8 9]

> Plotting confusion matrix...

> Calculating classification report...
              precision    recall  f1-score   support

         0.5       0.44      0.77      0.56      4332
         1.0       0.30      0.14      0.19      3572
         1.5       0.30      0.06      0.10      3154
         2.0       0.35      0.43      0.39      6382
         2.5       0.40      0.22      0.29      6884
         3.0       0.40      0.42      0.41     10770
         3.5       0.37      0.39      0.38     11078
         4.0       0.42      0.52      0.46     14100
         4.5       0.36      0.20      0.26      7432
         5.0       0.53      0.61      0.57      8933

    accuracy                           0.41     76637
   macro avg       0.39      0.38      0.36     76637
weighted avg       0.40      0.41      0.39     76637

-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
[PREDICTIONS]
-------------------------------------------------------------------------------
[INPUTS]
-------------------------------------------------------------------------------
###############################################################################


real	73m55.624s
user	44m10.753s
sys	2m29.386s
```

## Changes

1. Using complete dev and train dataset to build the vocabulary.
2. Using complete train dataset for training with a 90/10 percent train/validation split.
3. 

```python
class CnnModel():

    PREDICTOR_CLASSES = ['0.5', '1.0', '1.5', '2.0', '2.5', '3.0', '3.5', '4.0', '4.5', '5.0']
    NB_CLASSES = len(PREDICTOR_CLASSES)

    def __init__(self, vocabulary:dict, embeddings:list, max_tokens:int):
        super(CnnModel, self).__init__()

        self.id = uuid.uuid4()
        self.model_directory = f'models/{self.id}'

        if not os.path.exists(self.model_directory):
            os.makedirs(self.model_directory)

        # Vocabulary
        self.vocabulary = vocabulary
        self.max_tokens = max_tokens

        # Embeddings
        self.embedding_dimensions = 300

        # Training Parameters
        self.batch_size = 256
        self.max_epochs = 10
        self.learning_rate = 0.0005
        self.weight_decay = 1e-4

        # Model Hyperparameters
        self.is_trainable_embeddings = True
        self.patience = 2
        self.filters = self.embedding_dimensions * 3

        # Callbacks
        self.callbacks = [
            EarlyStopping(
                patience=self.patience, 
                monitor='val_loss', 
                min_delta=0.01, 
                verbose=1), 
            ModelCheckpoint(
                '{}/model.b{:0>3}-mt{}-vacc{}.h5'.format(
                self.model_directory,
                self.batch_size, 
                max_tokens, 
                '{val_categorical_accuracy:.4f}'), 
                monitor='val_categorical_accuracy', 
                mode='max', 
                verbose=1, 
                save_best_only=True)
        ]

        self.model = self.__build_model__(vocabulary, embeddings, max_tokens)

    def train(self, x_train, y_train, x_test, y_test):
        return self.model.fit(
            x_train, y_train, 
            validation_data=(x_test,y_test), 
            batch_size=self.batch_size, 
            epochs=self.max_epochs, 
            callbacks=self.callbacks, 
            shuffle=True, 
            verbose=2)

    def get_last_best(self):
        list_of_files = glob.glob(f'{self.model_directory}/*.h5')
        model_filename = max(list_of_files, key=os.path.getctime)
        # model = load_model('models/model.b256-vacc0.4040.h5')
        return load_model(model_filename)

    def evaluate_last_best(self, x_test, y_test):
        model = self.get_last_best()

        y_pred = model.predict(x_test)
        y_pred = np.argmax(y_pred, axis=1)
        y_test = np.argmax(y_test, axis=1)

        print('Test: {}'.format(np.unique(y_test)))
        print('Pred: {}'.format(np.unique(y_pred)))

        print()
        print('> Plotting confusion matrix...')
        cm = confusion_matrix(y_test, y_pred)
        disp = ConfusionMatrixDisplay(confusion_matrix=cm, display_labels=self.PREDICTOR_CLASSES)
        # disp = ConfusionMatrixDisplay(confusion_matrix=cm)
        disp.plot()
        plt.savefig(f'{self.model_directory}/model_confusion-matrix.png')

        print()
        print('> Calculating classification report...')
        cr = classification_report(y_test, y_pred, target_names=self.PREDICTOR_CLASSES)
        # cr = classification_report(y_test, y_pred)
        print(cr)

    def __build_model__(self, vocabulary:dict, embeddings:list, max_tokens:int):
        inputs = tf.keras.layers.Input((max_tokens,), name="input", )
        embs = Embedding(
            len(vocabulary), self.embedding_dimensions, input_length=max_tokens,
            weights=[embeddings], 
            trainable=self.is_trainable_embeddings
        )(inputs)

        x1 = Conv1D(self.filters, 1, activation='relu', padding='valid')(embs)
        x1 = SpatialDropout1D(0.3)(x1)
        x1 = MaxPooling1D()(x1)
        # x1 = Conv1D(embed_dim*2, 1, activation='relu', padding='valid')(x1)
        # x1 = SpatialDropout1D(0.3)(x1)
        # x1 = MaxPooling1D()(x1)
        # x1 = Conv1D(embed_dim, 1, activation='relu', padding='valid')(x1)
        # x1 = SpatialDropout1D(0.3)(x1)
        # x1 = MaxPooling1D()(x1)
        x1 = GlobalMaxPooling1D()(x1)
        
        x2 = Conv1D(self.filters, 2, activation='relu', padding='valid')(embs)
        x2 = SpatialDropout1D(0.3)(x2)
        x2 = MaxPooling1D()(x2)
        # x2 = Conv1D(embed_dim*2, 2, activation='relu', padding='valid')(x2)
        # x2 = SpatialDropout1D(0.3)(x2)
        # x2 = MaxPooling1D()(x2)
        # x2 = Conv1D(embed_dim, 2, activation='relu', padding='valid')(x2)
        # x2 = SpatialDropout1D(0.3)(x2)
        # x2 = MaxPooling1D()(x2)
        x2 = GlobalMaxPooling1D()(x2)
        
        x3 = Conv1D(self.filters, 3, activation='relu', padding='valid')(embs)
        x3 = SpatialDropout1D(0.3)(x3)
        x3 = MaxPooling1D()(x3)
        # x3 = Conv1D(embed_dim*2, 3, activation='relu', padding='valid')(x3)
        # x3 = SpatialDropout1D(0.3)(x3)
        # x3 = MaxPooling1D()(x3)
        # x3 = Conv1D(embed_dim, 3, activation='relu', padding='valid')(x3)
        # x3 = SpatialDropout1D(0.3)(x3)
        # x3 = MaxPooling1D()(x3)
        x3 = GlobalMaxPooling1D()(x3)
        
        x4 = Conv1D(self.filters, 4, activation='relu', padding='valid')(embs)
        x4 = SpatialDropout1D(0.3)(x4)
        x4 = MaxPooling1D()(x4)
        # x4 = Conv1D(embed_dim*2, 4, activation='relu', padding='valid')(x4)
        # x4 = SpatialDropout1D(0.3)(x4)
        # x4 = MaxPooling1D()(x4)
        # x4 = Conv1D(embed_dim, 4, activation='relu', padding='valid')(x4)
        # x4 = SpatialDropout1D(0.3)(x4)
        # x4 = MaxPooling1D()(x4)
        x4 = GlobalMaxPooling1D()(x4)
        
        x5 = Conv1D(self.filters, 5, activation='relu', padding='valid')(embs)
        x5 = SpatialDropout1D(0.3)(x5)
        x5 = MaxPooling1D()(x5)
        # x5 = Conv1D(embed_dim*2, 5, activation='relu', padding='valid')(x5)
        # x5 = SpatialDropout1D(0.3)(x5)
        # x5 = MaxPooling1D()(x5)
        # x5 = Conv1D(embed_dim, 5, activation='relu', padding='valid')(x5)
        # x5 = SpatialDropout1D(0.3)(x5)
        # x5 = MaxPooling1D()(x5)
        x5 = GlobalMaxPooling1D()(x5)

        x = Concatenate(axis=1)([x1,x2,x3,x4,x5])
        # x = GlobalMaxPooling1D()(x)
        # x = Flatten()(x)
        # x = Dropout(0.25)(x)
        x = Dense(4096, activation='relu', kernel_regularizer=regularizers.l2(self.weight_decay))(x)
        output = Dense(self.NB_CLASSES, activation='softmax')(x)  #multi-label (k-hot encoding)
        model = Model(inputs=[inputs], outputs=output)
        
        optim = optimizers.Adam(learning_rate=self.learning_rate)
        # optim = optimizers.Adadelta(learning_rate=LEARNING_RATE, rho=0.95, epsilon=1e-06)
        model.compile(loss='categorical_crossentropy', optimizer=optim, metrics=['categorical_accuracy'])

        plot_model(model, show_shapes=True, to_file=f'{self.model_directory}/model_architecture.png')

        return model

    def __str__(self):
        return self.model.summary()
```
