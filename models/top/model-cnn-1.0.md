# Model SVM 2.6

## Results

### Leaderboard

```powershell
G10_Maazouz-Turcotte	81.23%	37.68%	50.94%	12.81%	45.11%	30.57%	39.06%	27.39%	33.26%	8.39%	15.84%	54.45%
```

### Execution

```powershell
(venv) PS D:\Docs\M2\M2S3-APP-INNOV\D2-APP\d2-notes-films> (Measure-Command { python main.py --action train -tv bow-tfidf --type svm --corpus train --fast --corpus-subset 200000 | Out-Default }).ToString()

###############################################################################
 [TRAIN_MODEL]
###############################################################################
-------------------------------------------------------------------------------
 [ARGUMENTS]
-------------------------------------------------------------------------------
             Corpus : train (data/train.xml)
    Text Vectorizer : bow-tfidf
         Model Type : svm
        Is fast run : True
 Corpus Subset Size : 200000
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 [TRAINING]
-------------------------------------------------------------------------------

Model Summary :
SGDClassifier(max_iter=50)

      Features : 32195478
 Mean Accuracy : 0.3812

              precision    recall  f1-score   support

         0.5       0.45      0.71      0.55      1757
         1.0       0.28      0.09      0.14      1362
         1.5       0.31      0.05      0.08      1217
         2.0       0.34      0.33      0.33      2462
         2.5       0.35      0.22      0.27      2657
         3.0       0.36      0.41      0.39      4215
         3.5       0.34      0.28      0.31      4431
         4.0       0.39      0.52      0.44      5531
         4.5       0.30      0.10      0.15      2859
         5.0       0.44      0.69      0.53      3509

    accuracy                           0.38     30000
   macro avg       0.36      0.34      0.32     30000
weighted avg       0.36      0.38      0.35     30000

-------------------------------------------------------------------------------
###############################################################################


###############################################################################
 [PREDICT_NOTES]
###############################################################################
-------------------------------------------------------------------------------
 [ARGUMENTS]
-------------------------------------------------------------------------------
    Text Vectorizer : bow-tfidf
         Model Type : svm
        Is fast run : False
 Corpus Subset Size : 1000
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 [PREDICTIONS]
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
###############################################################################

00:12:12.1784306
```

## Changes

1. Completely removed the 'max_features' from TF-IDF vectorizer.
   1. Generated 32,195,478 features, 1.37GB vocabulary and 3.93GB pipeline (vectorizer+classifier)!
   2. Need to start tracking feature, vocabulary and model/pipeline size as a metric to avoid just scaling resources infinitely.

```python
stop_words = stopwords.words('french')

vectorizer = TfidfVectorizer(
    # The 'lowercase' and 'strip_accents' parameters are ignored when
    # 'preprocessor' is passed.
    # lowercase=True,
    # strip_accents='unicode', # {‘ascii’, ‘unicode’}
    ngram_range=(1,5), # (1,1), # (1,3)
    # min_df=0.00025, # Ignore too infrequent terms. 1
    max_df=0.50, # Ignore too frequent terms. 1.0
    max_features=500000,
    token_pattern=r'(?u)\b\w+\b', # (?u)\b\w\w+\b
    stop_words=stop_words, 
    preprocessor=preprocess_item
)

vectorizer = st.get_tfidf()    
classifier = SGDClassifier(alpha=0.0001, max_iter=50, penalty='l2')
```
