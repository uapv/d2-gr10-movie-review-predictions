# Model CNN 2.0

## Results

### Leaderboard

```powershell
G10_Maazouz-Turcotte	81.46%	37.63%	53.15%	2.92%	46.84%	28.94%	40.27%	29.64%	27.91%	17.06%	27.22%	54.01%
```

### Execution

```powershell
time python main.py --action train -tv bow-tfidf --type cnn --corpus train --fast --corpus-subset 300000

My first sbatch script!
True
2021-12-09 13:59:52.852155: I tensorflow/core/platform/cpu_feature_guard.cc:151] This TensorFlow binary is optimized with oneAPI Deep Neural Network Library (oneDNN) to use the following CPU instructions in performance-critical operations:  AVX2 AVX512F FMA
To enable them in other operations, rebuild TensorFlow with the appropriate compiler flags.
2021-12-09 13:59:53.532126: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1525] Created device /job:localhost/replica:0/task:0/device:GPU:0 with 15397 MB memory:  -> device: 0, name: Tesla P100-PCIE-16GB, pci bus id: 0000:3b:00.0, compute capability: 6.0
2021-12-09 13:59:56.107275: I tensorflow/stream_executor/cuda/cuda_dnn.cc:366] Loaded cuDNN version 8200

###############################################################################
 [TRAIN_MODEL]
###############################################################################
-------------------------------------------------------------------------------
 [ARGUMENTS]
-------------------------------------------------------------------------------
             Corpus : train (data/train.xml)
    Text Vectorizer : bow-tfidf
         Model Type : cnn
        Is fast run : True
 Corpus Subset Size : 300000
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 [TRAINING]
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
[DATA SAMPLING]
-------------------------------------------------------------------------------
> Sampling using the minimum value...
[0.5] Comp:17285	Min:30000	Sel:30000	(Class Size: 38372)
> Sampling using the minimum value...
[1.0] Comp:13620	Min:30000	Sel:30000	(Class Size: 30236)
> Sampling whole class size...
[1.5] Comp:12139	Min:30000	Sel:26949	(Class Size: 26949)
> Sampling using the minimum value...
[2.0] Comp:24904	Min:30000	Sel:30000	(Class Size: 55285)
> Sampling using the minimum value...
[2.5] Comp:26451	Min:30000	Sel:30000	(Class Size: 58719)
> Sampling calculated value...
[3.0] Comp:42391	Min:30000	Sel:42391	(Class Size: 94103)
> Sampling calculated value...
[3.5] Comp:43895	Min:30000	Sel:43895	(Class Size: 97443)
> Sampling calculated value...
[4.0] Comp:56154	Min:30000	Sel:56154	(Class Size: 124656)
> Sampling using the minimum value...
[4.5] Comp:28963	Min:30000	Sel:30000	(Class Size: 64296)
> Sampling calculated value...
[5.0] Comp:34192	Min:30000	Sel:34192	(Class Size: 75903)

count    353581.000000
mean          2.918157
std           1.381336
min           0.500000
25%           2.000000
50%           3.000000
75%           4.000000
max           5.000000
Name: note, dtype: float64

Y Shape : 353581
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
[VOCABULARY]
-------------------------------------------------------------------------------
count    353581.000000
mean         53.909393
std          70.314838
min           0.000000
25%          17.000000
50%          31.000000
75%          62.000000
max        2551.000000
Name: token_length, dtype: float64
Vocabulary Size: 164951
Max Sequence Length: 121
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
[INPUTS]
-------------------------------------------------------------------------------
(353581, 121)
> Splitting data into train/test sets.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
[EMBEDDINGS]
-------------------------------------------------------------------------------
Loading pretrained vectors...
Embedding Size: 164951
There are 110895 / 164951 pretrained vectors found.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
[TRAINING]
-------------------------------------------------------------------------------
Model: "model"
__________________________________________________________________________________________________
 Layer (type)                   Output Shape         Param #     Connected to                     
==================================================================================================
 input (InputLayer)             [(None, 121)]        0           []                               
                                                                                                  
 embedding (Embedding)          (None, 121, 300)     49485300    ['input[0][0]']                  
                                                                                                  
 conv1d (Conv1D)                (None, 121, 600)     180600      ['embedding[0][0]']              
                                                                                                  
 conv1d_1 (Conv1D)              (None, 120, 600)     360600      ['embedding[0][0]']              
                                                                                                  
 conv1d_2 (Conv1D)              (None, 119, 600)     540600      ['embedding[0][0]']              
                                                                                                  
 conv1d_3 (Conv1D)              (None, 118, 600)     720600      ['embedding[0][0]']              
                                                                                                  
 conv1d_4 (Conv1D)              (None, 117, 600)     900600      ['embedding[0][0]']              
                                                                                                  
 max_pooling1d (MaxPooling1D)   (None, 60, 600)      0           ['conv1d[0][0]']                 
                                                                                                  
 max_pooling1d_1 (MaxPooling1D)  (None, 60, 600)     0           ['conv1d_1[0][0]']               
                                                                                                  
 max_pooling1d_2 (MaxPooling1D)  (None, 59, 600)     0           ['conv1d_2[0][0]']               
                                                                                                  
 max_pooling1d_3 (MaxPooling1D)  (None, 59, 600)     0           ['conv1d_3[0][0]']               
                                                                                                  
 max_pooling1d_4 (MaxPooling1D)  (None, 58, 600)     0           ['conv1d_4[0][0]']               
                                                                                                  
 concatenate (Concatenate)      (None, 296, 600)     0           ['max_pooling1d[0][0]',          
                                                                  'max_pooling1d_1[0][0]',        
                                                                  'max_pooling1d_2[0][0]',        
                                                                  'max_pooling1d_3[0][0]',        
                                                                  'max_pooling1d_4[0][0]']        
                                                                                                  
 global_max_pooling1d (GlobalMa  (None, 600)         0           ['concatenate[0][0]']            
 xPooling1D)                                                                                      
                                                                                                  
 dense (Dense)                  (None, 128)          76928       ['global_max_pooling1d[0][0]']   
                                                                                                  
 dropout (Dropout)              (None, 128)          0           ['dense[0][0]']                  
                                                                                                  
 dense_1 (Dense)                (None, 10)           1290        ['dropout[0][0]']                
                                                                                                  
==================================================================================================
Total params: 52,266,518
Trainable params: 52,266,518
Non-trainable params: 0
__________________________________________________________________________________________________
Epoch 1/10

Epoch 00001: val_categorical_accuracy improved from -inf to 0.36589, saving model to models/model.b256-vacc0.3659.h5
1174/1174 - 152s - loss: 1.7111 - categorical_accuracy: 0.3213 - val_loss: 1.5584 - val_categorical_accuracy: 0.3659 - 152s/epoch - 129ms/step
Epoch 2/10

Epoch 00002: val_categorical_accuracy improved from 0.36589 to 0.37435, saving model to models/model.b256-vacc0.3744.h5
1174/1174 - 148s - loss: 1.4766 - categorical_accuracy: 0.3991 - val_loss: 1.5384 - val_categorical_accuracy: 0.3744 - 148s/epoch - 126ms/step
Epoch 3/10

Epoch 00003: val_categorical_accuracy did not improve from 0.37435
1174/1174 - 147s - loss: 1.2761 - categorical_accuracy: 0.4798 - val_loss: 1.6060 - val_categorical_accuracy: 0.3647 - 147s/epoch - 125ms/step
Epoch 4/10

Epoch 00004: val_categorical_accuracy did not improve from 0.37435
1174/1174 - 147s - loss: 1.0048 - categorical_accuracy: 0.5988 - val_loss: 1.8684 - val_categorical_accuracy: 0.3532 - 147s/epoch - 125ms/step
Epoch 5/10

Epoch 00005: val_categorical_accuracy did not improve from 0.37435
1174/1174 - 147s - loss: 0.7302 - categorical_accuracy: 0.7183 - val_loss: 2.2952 - val_categorical_accuracy: 0.3400 - 147s/epoch - 125ms/step
Epoch 00005: early stopping
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
[EVALUATION]
-------------------------------------------------------------------------------
Test: [0 1 2 3 4 5 6 7 8 9]
Pred: [0 1 2 3 4 5 6 7 8 9]

> Plotting confusion matrix...

> Calculating classification report...
              precision    recall  f1-score   support

         0.5       0.48      0.65      0.55      4512
         1.0       0.29      0.33      0.31      4440
         1.5       0.27      0.15      0.19      4007
         2.0       0.30      0.23      0.26      4495
         2.5       0.32      0.28      0.30      4472
         3.0       0.33      0.46      0.39      6279
         3.5       0.36      0.25      0.29      6617
         4.0       0.39      0.57      0.46      8550
         4.5       0.44      0.02      0.03      4519
         5.0       0.49      0.61      0.54      5147

    accuracy                           0.37     53038
   macro avg       0.37      0.35      0.33     53038
weighted avg       0.37      0.37      0.35     53038

-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
[PREDICTIONS]
-------------------------------------------------------------------------------
[INPUTS]
-------------------------------------------------------------------------------
###############################################################################


real	21m45.100s
user	17m31.262s
sys	0m28.215s
```

## Changes

1. Used FastText pre-trained word embeddings instead of word2vec.
2. Set the max sentence/token length to the 90th quantile instead of 200, mean or median.
3. Removed the limit on vocabulary size.
	- Could we define the vocabulary based on TF-IDF values of DEV+TRAIN corpus?
4. Kept the word embeddings trainable.

```python
PREDICTOR_CLASSES = ['0.5', '1.0', '1.5', '2.0', '2.5', '3.0', '3.5', '4.0', '4.5', '5.0']
NB_CLASSES = len(PREDICTOR_CLASSES)

MAX_NB_WORDS = 1000000
MAX_SEQUENCE_LEN = 200
IS_TRAINABLE_EMBEDDINGS = True

#training params
BATCH_SIZE = 256
LEARNING_RATE = 0.0005
num_epochs = 10

#model parameters
PATIENCE=3

num_filters = embed_dim*2

inputs = tf.keras.layers.Input((max_len,), name="input", )
embs = Embedding(
	len(vocab), embed_dim, input_length=max_len,
	weights=[embeddings], 
	trainable=IS_TRAINABLE_EMBEDDINGS
)(inputs)

x1 = Conv1D(num_filters, 1, activation='relu', padding='valid', strides=1)(embs)
x1 = MaxPooling1D()(x1)

x2 = Conv1D(num_filters, 2, activation='relu', padding='valid', strides=1)(embs)
x2 = MaxPooling1D()(x2)

x3 = Conv1D(num_filters, 3, activation='relu', padding='valid', strides=1)(embs)
x3 = MaxPooling1D()(x3)

x4 = Conv1D(num_filters, 4, activation='relu', padding='valid', strides=1)(embs)
x4 = MaxPooling1D()(x4)

x5 = Conv1D(num_filters, 5, activation='relu', padding='valid', strides=1)(embs)
x5 = MaxPooling1D()(x5)

x = Concatenate(axis=1)([x1,x2,x3,x4,x5])
x = GlobalMaxPooling1D()(x)
x = Dense(128, activation='relu')(x)
x = Dropout(0.25)(x)
output = Dense(NB_CLASSES, activation='softmax')(x)  #multi-label (k-hot encoding)
model = Model(inputs=[inputs], outputs=output)

optim = optimizers.Adam(learning_rate=LEARNING_RATE)
model.compile(loss='categorical_crossentropy', optimizer=optim, metrics=['categorical_accuracy'])
model.summary()
```
