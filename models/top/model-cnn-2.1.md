# Model CNN 2.1

## Results

### Leaderboard

```powershell
G10_Maazouz-Turcotte	82.33%	38.69%	51.9%	21.99%	46.66%	32.12%	38.7%	28.77%	35.98%	4.59%	23.39%	55.76%
```

### Execution

```powershell
time python main.py --action train -tv bow-tfidf --type cnn --corpus train --fast --corpus-subset 500000

My first sbatch script!
True
2021-12-09 14:38:33.894200: I tensorflow/core/platform/cpu_feature_guard.cc:151] This TensorFlow binary is optimized with oneAPI Deep Neural Network Library (oneDNN) to use the following CPU instructions in performance-critical operations:  AVX2 AVX512F FMA
To enable them in other operations, rebuild TensorFlow with the appropriate compiler flags.
2021-12-09 14:38:34.521710: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1525] Created device /job:localhost/replica:0/task:0/device:GPU:0 with 15397 MB memory:  -> device: 0, name: Tesla P100-PCIE-16GB, pci bus id: 0000:3b:00.0, compute capability: 6.0
2021-12-09 14:38:37.417267: I tensorflow/stream_executor/cuda/cuda_dnn.cc:366] Loaded cuDNN version 8200

###############################################################################
 [TRAIN_MODEL]
###############################################################################
-------------------------------------------------------------------------------
 [ARGUMENTS]
-------------------------------------------------------------------------------
             Corpus : train (data/train.xml)
    Text Vectorizer : bow-tfidf
         Model Type : cnn
        Is fast run : True
 Corpus Subset Size : 500000
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 [TRAINING]
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
[DATA SAMPLING]
-------------------------------------------------------------------------------
> Sampling whole class size...
[0.5] Comp:28809	Min:50000	Sel:38372	(Class Size: 38372)
> Sampling whole class size...
[1.0] Comp:22700	Min:50000	Sel:30236	(Class Size: 30236)
> Sampling whole class size...
[1.5] Comp:20233	Min:50000	Sel:26949	(Class Size: 26949)
> Sampling using the minimum value...
[2.0] Comp:41507	Min:50000	Sel:50000	(Class Size: 55285)
> Sampling using the minimum value...
[2.5] Comp:44085	Min:50000	Sel:50000	(Class Size: 58719)
> Sampling calculated value...
[3.0] Comp:70651	Min:50000	Sel:70651	(Class Size: 94103)
> Sampling calculated value...
[3.5] Comp:73159	Min:50000	Sel:73159	(Class Size: 97443)
> Sampling calculated value...
[4.0] Comp:93590	Min:50000	Sel:93590	(Class Size: 124656)
> Sampling using the minimum value...
[4.5] Comp:48273	Min:50000	Sel:50000	(Class Size: 64296)
> Sampling calculated value...
[5.0] Comp:56987	Min:50000	Sel:56987	(Class Size: 75903)

count    539944.000000
mean          3.087635
std           1.313874
min           0.500000
25%           2.000000
50%           3.500000
75%           4.000000
max           5.000000
Name: note, dtype: float64

Y Shape : 539944
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
[VOCABULARY]
-------------------------------------------------------------------------------
count    539944.000000
mean         54.063899
std          70.274719
min           0.000000
25%          17.000000
50%          31.000000
75%          63.000000
max        2551.000000
Name: token_length, dtype: float64
Vocabulary Size: 196992
Max Sequence Length: 121
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
[INPUTS]
-------------------------------------------------------------------------------
(539944, 121)
> Splitting data into train/test sets.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
[EMBEDDINGS]
-------------------------------------------------------------------------------
Loading pretrained vectors...
Embedding Size: 196992
There are 123620 / 196992 pretrained vectors found.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
[TRAINING]
-------------------------------------------------------------------------------
Model: "model"
__________________________________________________________________________________________________
 Layer (type)                   Output Shape         Param #     Connected to                     
==================================================================================================
 input (InputLayer)             [(None, 121)]        0           []                               
                                                                                                  
 embedding (Embedding)          (None, 121, 300)     59097600    ['input[0][0]']                  
                                                                                                  
 conv1d (Conv1D)                (None, 121, 600)     180600      ['embedding[0][0]']              
                                                                                                  
 conv1d_1 (Conv1D)              (None, 120, 600)     360600      ['embedding[0][0]']              
                                                                                                  
 conv1d_2 (Conv1D)              (None, 119, 600)     540600      ['embedding[0][0]']              
                                                                                                  
 conv1d_3 (Conv1D)              (None, 118, 600)     720600      ['embedding[0][0]']              
                                                                                                  
 conv1d_4 (Conv1D)              (None, 117, 600)     900600      ['embedding[0][0]']              
                                                                                                  
 max_pooling1d (MaxPooling1D)   (None, 60, 600)      0           ['conv1d[0][0]']                 
                                                                                                  
 max_pooling1d_1 (MaxPooling1D)  (None, 60, 600)     0           ['conv1d_1[0][0]']               
                                                                                                  
 max_pooling1d_2 (MaxPooling1D)  (None, 59, 600)     0           ['conv1d_2[0][0]']               
                                                                                                  
 max_pooling1d_3 (MaxPooling1D)  (None, 59, 600)     0           ['conv1d_3[0][0]']               
                                                                                                  
 max_pooling1d_4 (MaxPooling1D)  (None, 58, 600)     0           ['conv1d_4[0][0]']               
                                                                                                  
 concatenate (Concatenate)      (None, 296, 600)     0           ['max_pooling1d[0][0]',          
                                                                  'max_pooling1d_1[0][0]',        
                                                                  'max_pooling1d_2[0][0]',        
                                                                  'max_pooling1d_3[0][0]',        
                                                                  'max_pooling1d_4[0][0]']        
                                                                                                  
 global_max_pooling1d (GlobalMa  (None, 600)         0           ['concatenate[0][0]']            
 xPooling1D)                                                                                      
                                                                                                  
 dense (Dense)                  (None, 128)          76928       ['global_max_pooling1d[0][0]']   
                                                                                                  
 dropout (Dropout)              (None, 128)          0           ['dense[0][0]']                  
                                                                                                  
 dense_1 (Dense)                (None, 10)           1290        ['dropout[0][0]']                
                                                                                                  
==================================================================================================
Total params: 61,878,818
Trainable params: 61,878,818
Non-trainable params: 0
__________________________________________________________________________________________________
Epoch 1/10

Epoch 00001: val_categorical_accuracy improved from -inf to 0.38606, saving model to models/model.b256-vacc0.3861.h5
1793/1793 - 233s - loss: 1.6576 - categorical_accuracy: 0.3427 - val_loss: 1.5242 - val_categorical_accuracy: 0.3861 - 233s/epoch - 130ms/step
Epoch 2/10

Epoch 00002: val_categorical_accuracy improved from 0.38606 to 0.38982, saving model to models/model.b256-vacc0.3898.h5
1793/1793 - 230s - loss: 1.4440 - categorical_accuracy: 0.4130 - val_loss: 1.5174 - val_categorical_accuracy: 0.3898 - 230s/epoch - 128ms/step
Epoch 3/10

Epoch 00003: val_categorical_accuracy did not improve from 0.38982
1793/1793 - 229s - loss: 1.2515 - categorical_accuracy: 0.4862 - val_loss: 1.6014 - val_categorical_accuracy: 0.3799 - 229s/epoch - 128ms/step
Epoch 4/10

Epoch 00004: val_categorical_accuracy did not improve from 0.38982
1793/1793 - 228s - loss: 0.9981 - categorical_accuracy: 0.5964 - val_loss: 1.8960 - val_categorical_accuracy: 0.3637 - 228s/epoch - 127ms/step
Epoch 00004: early stopping
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
[EVALUATION]
-------------------------------------------------------------------------------
Test: [0 1 2 3 4 5 6 7 8 9]
Pred: [0 1 2 3 4 5 6 7 8 9]

> Plotting confusion matrix...

> Calculating classification report...
              precision    recall  f1-score   support

         0.5       0.51      0.68      0.58      5743
         1.0       0.31      0.19      0.24      4634
         1.5       0.27      0.03      0.05      4069
         2.0       0.33      0.39      0.36      7579
         2.5       0.35      0.24      0.29      7582
         3.0       0.34      0.41      0.38     10614
         3.5       0.35      0.30      0.32     10860
         4.0       0.39      0.57      0.47     14133
         4.5       0.34      0.18      0.23      7428
         5.0       0.51      0.57      0.54      8350

    accuracy                           0.39     80992
   macro avg       0.37      0.36      0.35     80992
weighted avg       0.38      0.39      0.37     80992

-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
[PREDICTIONS]
-------------------------------------------------------------------------------
[INPUTS]
-------------------------------------------------------------------------------
###############################################################################


real	28m44.037s
user	23m15.954s
sys	0m32.598s
```

## Changes

1. Increased sample size from 300,000 to 500,000.

```python
PREDICTOR_CLASSES = ['0.5', '1.0', '1.5', '2.0', '2.5', '3.0', '3.5', '4.0', '4.5', '5.0']
NB_CLASSES = len(PREDICTOR_CLASSES)

MAX_NB_WORDS = 1000000
MAX_SEQUENCE_LEN = 200
IS_TRAINABLE_EMBEDDINGS = True

#training params
BATCH_SIZE = 256
LEARNING_RATE = 0.0005
num_epochs = 10

#model parameters
PATIENCE=3

num_filters = embed_dim*2

inputs = tf.keras.layers.Input((max_len,), name="input", )
embs = Embedding(
	len(vocab), embed_dim, input_length=max_len,
	weights=[embeddings], 
	trainable=IS_TRAINABLE_EMBEDDINGS
)(inputs)

x1 = Conv1D(num_filters, 1, activation='relu', padding='valid', strides=1)(embs)
x1 = MaxPooling1D()(x1)

x2 = Conv1D(num_filters, 2, activation='relu', padding='valid', strides=1)(embs)
x2 = MaxPooling1D()(x2)

x3 = Conv1D(num_filters, 3, activation='relu', padding='valid', strides=1)(embs)
x3 = MaxPooling1D()(x3)

x4 = Conv1D(num_filters, 4, activation='relu', padding='valid', strides=1)(embs)
x4 = MaxPooling1D()(x4)

x5 = Conv1D(num_filters, 5, activation='relu', padding='valid', strides=1)(embs)
x5 = MaxPooling1D()(x5)

x = Concatenate(axis=1)([x1,x2,x3,x4,x5])
x = GlobalMaxPooling1D()(x)
x = Dense(128, activation='relu')(x)
x = Dropout(0.25)(x)
output = Dense(NB_CLASSES, activation='softmax')(x)  #multi-label (k-hot encoding)
model = Model(inputs=[inputs], outputs=output)

optim = optimizers.Adam(learning_rate=LEARNING_RATE)
model.compile(loss='categorical_crossentropy', optimizer=optim, metrics=['categorical_accuracy'])
model.summary()
```
