# Model CNN 2.3

## Results

### Leaderboard

```powershell
G10_Maazouz-Turcotte	82.63%	39.76%	53.81%	9.83%	48.26%	24.89%	43.03%	29.75%	37.73%	10.71%	14.72%	55.81%
```

### Execution

```powershell
time python main.py --action train -tv bow-tfidf --type cnn --corpus train

My first sbatch script!
True

###############################################################################
 [TRAIN_MODEL]
###############################################################################
-------------------------------------------------------------------------------
 [ARGUMENTS]
-------------------------------------------------------------------------------
             Corpus : train (data/train.xml)
    Text Vectorizer : bow-tfidf
         Model Type : cnn
        Is fast run : False
 Corpus Subset Size : 1000
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 [TRAINING]
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
[VOCABULARY]
-------------------------------------------------------------------------------
> Existing vocabulary loaded from 'data/train-vocab.h5'...

Vocabulary Samples:
                   key   value
0                <pad>       0
1                <unk>       1
2          __tok_pos__       2
3          __tok_neg__       3
4          __tok_emp__       4
...                ...     ...
235186  désinteressant  235186
235187   inhospitalité  235187
235188          sortit  235188
235189        happante  235189
235190         anoblir  235190

[235191 rows x 2 columns]

Vocabulary Size: 235191
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
[EMBEDDINGS]
-------------------------------------------------------------------------------

Embedding Size: 300
Total Missing Words: 607

Missing Words (word, count):
[('e', 93344), ('a', 72530), ('i', 67018), ('r', 60729), ('n', 60236), ('s', 55450), ('t', 51353), ('o', 50819), ('l', 41168), ('u', 33942), ('c', 28947), ('m', 26249), ('d', 22178), ('h', 20221), ('p', 19256), ('é', 17032), ('g', 16461), ('b', 15978), ('f', 11924), ('v', 8835), ('k', 8487), ('y', 8016), ('è', 6302), ('q', 4854), ('w', 4576), ('z', 4563), ('j', 3619), ('x', 3228), ('0', 1475), ('2', 1379), ('_', 1304), ('1', 1240), ('3', 847), ('9', 667), ('â', 486), ('7', 480), ('ï', 463), ('ç', 455), ('ê', 426), ('8', 424), ('4', 371), ('ô', 337), ('5', 334), ('î', 299), ('à', 238), ('6', 234), ('ö', 128), ('û', 122), ('ü', 115), ('ė', 109), ('á', 90), ('ë', 84), ('장', 82), ('ó', 79), ('ä', 67), ('ù', 59), ('출', 56), ('í', 55), ('흥', 39), ('的', 37), ('ñ', 35), ('œ', 30), ('²', 27), ('ø', 26), ('ō', 23), ('ú', 19), ('ョ', 18), ('ã', 16), ('성', 16), ('是', 16), ('𝙀', 15), ('보', 14), ('一', 14), ('ì', 14), ('ĺ', 14), ('고', 13), ('마', 13), ('ò', 13), ('了', 13), ('å', 13), ('ı', 13), ('샵', 13), ('ŕ', 13), ('ا', 12), ('화', 11), ('순', 11), ('가', 10), ('æ', 10), ('部', 10), ('ʼ', 10), ('𝙎', 9), ('𝙊', 9), ('콜', 9), ('걸', 9), ('지', 9), ('남', 9), ('片', 9), ('ń', 9), ('ć', 9), ('𝙍', 8), ('사', 8), ('만', 8), ('ð', 8), ('ś', 8), ('不', 8), ('안', 7), ('õ', 7), ('ر', 7), ('ǝ', 7), ('ł', 7), ('看', 7), ('这', 7), ('모', 6), ('텔', 6), ('就', 6), ('电', 6), ('影', 6), ('격', 6), ('𝙇', 6), ('ū', 6), ('카', 6), ('톡', 6), ('人', 6), ('𝙐', 6), ('𝙄', 5), ('³', 5), ('有', 5), ('시', 5), ('ş', 5), ('š', 5), ('情', 5), ('和', 5), ('ˋ', 5), ('𝙏', 5), ('最', 5), ('时', 5), ('ɐ', 5), ('아', 4), ('씨', 4), ('ý', 4), ('O', 4), ('还', 4), ('给', 4), ('ď', 4), ('同', 4), ('小', 4), ('조', 4), ('건', 4), ('渐', 4), ('ğ', 4), ('结', 4), ('国', 4), ('无', 4), ('于', 4), ('过', 4), ('个', 4), ('𝙋', 4), ('い', 3), ('た', 3), ('T', 3), ('K', 3), ('里', 3), ('在', 3), ('面', 3), ('分', 3), ('ɹ', 3), ('업', 3), ('č', 3), ('些', 3), ('ل', 3), ('ÿ', 3), ('ă', 3), ('爱', 3), ('𝘼', 3), ('狗', 3), ('次', 3), ('自', 3), ('己', 3), ('意', 3), ('大', 3), ('候', 3), ('几', 3), ('美', 3), ('ē', 3), ('ɯ', 3), ('对', 3), ('子', 3), ('𝘿', 3), ('𝘽', 3), ('前', 3), ('星', 3), ('特', 3), ('都', 3), ('𝙁', 3), ('怪', 3), ('演', 3), ('ล', 2), ('あ', 2), ('り', 2), ('ま', 2), ('½', 2), ('E', 2), ('P', 2), ('死', 2), ('西', 2), ('风', 2), ('剧', 2), ('杰', 2), ('克', 2), ('把', 2), ('短', 2), ('为', 2), ('画', 2), ('场', 2), ('起', 2), ('ā', 2), ('打', 2), ('享', 2), ('靠', 2), ('ž', 2), ('م', 2), ('소', 2), ('þ', 2), ('ό', 2), ('暴', 2), ('力', 2), ('色', 2), ('外', 2), ('산', 2), ('ب', 2), ('ī', 2), ('ň', 2), ('拍', 2), ('制', 2), ('精', 2), ('良', 2), ('đ', 2), ('س', 2), ('پ', 2), ('ی', 2), ('好', 2), ('们', 2), ('ع', 2), ('如', 2), ('观', 2), ('众', 2), ('𝘾', 2), ('其', 2), ('复', 2), ('活', 2), ('声', 2), ('得', 2), ('맛', 2), ('ε', 2), ('ύ', 2), ('ς', 2), ('应', 2), ('该', 2), ('主', 2), ('很', 2), ('遗', 2), ('也', 2), ('会', 2), ('断', 2), ('续', 2), ('野', 2), ('句', 2), ('此', 2), ('麻', 2), ('昏', 2), ('单', 2), ('话', 2), ('血', 2), ('狄', 2), ('没', 2), ('多', 2), ('𝙑', 2), ('神', 2), ('𝙉', 2), ('の', 2), ('π', 2), ('ρ', 2), ('ه', 2), ('ż', 2), ('年', 2), ('半', 2), ('哥', 2), ('后', 2), ('出', 2), ('缺', 2), ('束', 2), ('之', 2), ('ǁ', 2), ('王', 2), ('っ', 2), ('能', 2), ('难', 2), ('高', 2), ('绘', 2), ('说', 2), ('正', 2), ('做', 2), ('什', 2), ('么', 2), ('所', 2), ('法', 2), ('身', 2), ('戏', 2), ('曼', 2), ('오', 1), ('피', 1), ('ř', 1), ('が', 1), ('と', 1), ('う', 1), ('ご', 1), ('ざ', 1), ('し', 1), ('서', 1), ('비', 1), ('스', 1), ('ร', 1), ('ะ', 1), ('M', 1), ('慢', 1), ('采', 1), ('坨', 1), ('翔', 1), ('像', 1), ('成', 1), ('长', 1), ('现', 1), ('扩', 1), ('充', 1), ('称', 1), ('动', 1), ('撑', 1), ('底', 1), ('ج', 1), ('ﻧ', 1), ('加', 1), ('恐', 1), ('怖', 1), ('한', 1), ('ك', 1), ('ס', 1), ('ר', 1), ('ט', 1), ('ķ', 1), ('除', 1), ('摄', 1), ('作', 1), ('ت', 1), ('幸', 1), ('我', 1), ('و', 1), ('ة', 1), ('𝙂', 1), ('实', 1), ('冷', 1), ('丁', 1), ('枪', 1), ('习', 1), ('惯', 1), ('觉', 1), ('已', 1), ('经', 1), ('ζ', 1), ('团', 1), ('圆', 1), ('局', 1), ('憾', 1), ('ز', 1), ('算', 1), ('亮', 1), ('点', 1), ('吗', 1), ('想', 1), ('往', 1), ('事', 1), ('מ', 1), ('ע', 1), ('ו', 1), ('ל', 1), ('ה', 1), ('近', 1), ('粗', 1), ('俗', 1), ('蛮', 1), ('露', 1), ('俳', 1), ('ƃ', 1), ('ɥ', 1), ('계', 1), ('제', 1), ('위', 1), ('谋', 1), ('杀', 1), ('木', 1), ('欲', 1), ('睡', 1), ('来', 1), ('余', 1), ('聊', 1), ('ɟ', 1), ('¾', 1), ('欧', 1), ('律', 1), ('刻', 1), ('¹', 1), ('弹', 1), ('穿', 1), ('脑', 1), ('袋', 1), ('渲', 1), ('染', 1), ('ⅰ', 1), ('门', 1), ('ʇ', 1), ('굿', 1), ('박', 1), ('춘', 1), ('배', 1), ('世', 1), ('界', 1), ('무', 1), ('명', 1), ('题', 1), ('ο', 1), ('μ', 1), ('η', 1), ('θ', 1), ('ʌ', 1), ('ง', 1), ('บ', 1), ('节', 1), ('盘', 1), ('屎', 1), ('ن', 1), ('ي', 1), ('带', 1), ('着', 1), ('腥', 1), ('懵', 1), ('懂', 1), ('浪', 1), ('费', 1), ('两', 1), ('烂', 1), ('今', 1), ('早', 1), ('巴', 1), ('黎', 1), ('展', 1), ('览', 1), ('二', 1), ('十', 1), ('他', 1), ('频', 1), ('繁', 1), ('交', 1), ('错', 1), ('格', 1), ('令', 1), ('讨', 1), ('厌', 1), ('老', 1), ('造', 1), ('烦', 1), ('朋', 1), ('友', 1), ('李', 1), ('宝', 1), ('田', 1), ('尾', 1), ('乎', 1), ('料', 1), ('ß', 1), ('物', 1), ('设', 1), ('计', 1), ('灰', 1), ('暗', 1), ('社', 1), ('区', 1), ('可', 1), ('少', 1), ('性', 1), ('ⅱ', 1), ('天', 1), ('気', 1), ('ف', 1), ('د', 1), ('α', 1), ('τ', 1), ('ὴ', 1), ('곡', 1), ('ȏ', 1), ('关', 1), ('仁', 1), ('龙', 1), ('𝙈', 1), ('𝙃', 1), ('N', 1), ('G', 1), ('く', 1), ('じ', 1), ('ビ', 1), ('ジ', 1), ('ン', 1), ('を', 1), ('持', 1), ('て', 1), ('る', 1), ('魔', 1), ('女', 1), ('신', 1), ('ế', 1), ('ص', 1), ('غ', 1), ('奇', 1), ('迹', 1), ('只', 1), ('ę', 1), ('院', 1), ('别', 1), ('那', 1), ('段', 1), ('念', 1), ('潮', 1), ('델', 1), ('루', 1), ('와', 1), ('确', 1), ('ⁿ', 1), ('谓', 1), ('以', 1), ('去', 1), ('彩', 1), ('犯', 1), ('罪', 1), ('比', 1), ('虽', 1), ('然', 1), ('荒', 1), ('诞', 1), ('雅', 1), ('谱', 1), ('达', 1), ('蒙', 1), ('再', 1), ('胧', 1), ('月', 1), ('本', 1), ('中', 1), ('勾', 1), ('回', 1), ('忆', 1), ('私', 1), ('は', 1), ('な', 1), ('よ', 1), ('も', 1), ('효', 1), ('진', 1), ('일', 1), ('광', 1), ('𝙕', 1), ('외', 1), ('인', 1), ('ě', 1), ('ầ', 1), ('毁', 1), ('创', 1), ('下', 1), ('丰', 1), ('功', 1), ('伟', 1), ('绩', 1), ('或', 1), ('许', 1), ('它', 1), ('魅', 1), ('尼', 1), ('基', 1), ('德', 1), ('技', 1), ('メ', 1), ('ア', 1), ('リ', 1), ('종', 1), ('구', 1), ('S', 1), ('家', 1), ('衛', 1), ('另', 1), ('真', 1), ('ช', 1), ('า', 1), ('ต', 1), ('雷', 1), ('奥', 1), ('兽', 1), ('般', 1), ('地', 1), ('ᵉ', 1), ('µ', 1)]
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
[DATA SAMPLING]
-------------------------------------------------------------------------------

Note Distribution:
count    665962.000000
mean          3.210459
std           1.261124
min           0.500000
25%           2.500000
50%           3.500000
75%           4.000000
max           5.000000
Name: note, dtype: float64
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
[INPUTS]
-------------------------------------------------------------------------------
Token Length Distribution:
2021-12-11 13:43:11.395137: I tensorflow/core/platform/cpu_feature_guard.cc:151] This TensorFlow binary is optimized with oneAPI Deep Neural Network Library (oneDNN) to use the following CPU instructions in performance-critical operations:  AVX2 AVX512F FMA
To enable them in other operations, rebuild TensorFlow with the appropriate compiler flags.
2021-12-11 13:43:12.010184: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1525] Created device /job:localhost/replica:0/task:0/device:GPU:0 with 15397 MB memory:  -> device: 0, name: Tesla P100-PCIE-16GB, pci bus id: 0000:3b:00.0, compute capability: 6.0
         token_count
count  665962.000000
mean       55.643493
std        72.250917
min         0.000000
25%        17.000000
50%        32.000000
75%        64.000000
max      2697.000000

Max Sequence Length: 125
Input Shape: (665962, 125)

First 2 Inputs:
[[158514 103992  37647  61854  82526 181264 122492  94523  22831 156264
   37892 183936  85563 110698  91587  27205  76811  73781 161749  26104
  195646 106763  26104 156133  30976  22041 224109  33712    137  31218
   35344 128784   8825 216835  45726 161901  60605  92140   5652 223259
  103992 122646 153571 233523 181641 145086  49843  80469  80948 234847
  106763 180548 155733 149845   7223 106548  66854 219233 151146 222678
  108747 170070  20230 212656   3855 183936 115169 119331  23131 138969
  153949  78648  48991  28705  22041 105866 151146 209588 140975 202640
  177684 207429  22041 174456 166658  49156 113164 147288 155432 160700
   22041 191763 145086 135507 234369 123737  22041 224109 196367      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0      0      0      0      0      0
       0      0      0      0      0]
 [129128  78983 156726 114352 224224  32903 203984 166001  13519 232023
   37652 220729 201459  24380 131300 162568   7585  13811   1705 153949
  215028  95984  61778 129638 114352  51205 173339 208129  48734  70978
  183223 169277 158463  47624 197077 147288  93738 177684 153949  13519
   86611 193649  27631  78648  31559 151470  10327  29202 193874  69128
  162568  78648 169997  69985  23233 178252  12473  91734  96192 217819
   78983 166001  13519 169182   2311 108971  76254 195706  86611 141887
   22277  83409 141621  20621 170699 100672  26530  15179  82526  79856
  129489 134323  91028  91147 121362  10700  22041 165825  80469  12473
    9360 157917  26530 121314  69158  26366 223683  23233 190570  80229
  218354  54947  71847  44954 185773  79543 188312 212834  44387  44677
  208927 209588 113239  14510 182231 202574 224109 230667  38247  90616
  182231 161470  31425 194756 162677]]

> Splitting data and labels into train/test sets.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
[TRAINING]
-------------------------------------------------------------------------------
Model: "model"
__________________________________________________________________________________________________
 Layer (type)                   Output Shape         Param #     Connected to                     
==================================================================================================
 input (InputLayer)             [(None, 125)]        0           []                               
                                                                                                  
 embedding (Embedding)          (None, 125, 300)     70557300    ['input[0][0]']                  
                                                                                                  
 conv1d (Conv1D)                (None, 125, 900)     270900      ['embedding[0][0]']              
                                                                                                  
 conv1d_1 (Conv1D)              (None, 124, 900)     540900      ['embedding[0][0]']              
                                                                                                  
 conv1d_2 (Conv1D)              (None, 123, 900)     810900      ['embedding[0][0]']              
                                                                                                  
 conv1d_3 (Conv1D)              (None, 122, 900)     1080900     ['embedding[0][0]']              
                                                                                                  
 conv1d_4 (Conv1D)              (None, 121, 900)     1350900     ['embedding[0][0]']              
                                                                                                  
 tf.identity (TFOpLambda)       (None, 125, 900)     0           ['conv1d[0][0]']                 
                                                                                                  
 tf.identity_1 (TFOpLambda)     (None, 124, 900)     0           ['conv1d_1[0][0]']               
                                                                                                  
 tf.identity_2 (TFOpLambda)     (None, 123, 900)     0           ['conv1d_2[0][0]']               
                                                                                                  
 tf.identity_3 (TFOpLambda)     (None, 122, 900)     0           ['conv1d_3[0][0]']               
                                                                                                  
 tf.identity_4 (TFOpLambda)     (None, 121, 900)     0           ['conv1d_4[0][0]']               
                                                                                                  
 max_pooling1d (MaxPooling1D)   (None, 62, 900)      0           ['tf.identity[0][0]']            
                                                                                                  
 max_pooling1d_1 (MaxPooling1D)  (None, 62, 900)     0           ['tf.identity_1[0][0]']          
                                                                                                  
 max_pooling1d_2 (MaxPooling1D)  (None, 61, 900)     0           ['tf.identity_2[0][0]']          
                                                                                                  
 max_pooling1d_3 (MaxPooling1D)  (None, 61, 900)     0           ['tf.identity_3[0][0]']          
                                                                                                  
 max_pooling1d_4 (MaxPooling1D)  (None, 60, 900)     0           ['tf.identity_4[0][0]']          
                                                                                                  
 global_max_pooling1d (GlobalMa  (None, 900)         0           ['max_pooling1d[0][0]']          
 xPooling1D)                                                                                      
                                                                                                  
 global_max_pooling1d_1 (Global  (None, 900)         0           ['max_pooling1d_1[0][0]']        
 MaxPooling1D)                                                                                    
                                                                                                  
 global_max_pooling1d_2 (Global  (None, 900)         0           ['max_pooling1d_2[0][0]']        
 MaxPooling1D)                                                                                    
                                                                                                  
 global_max_pooling1d_3 (Global  (None, 900)         0           ['max_pooling1d_3[0][0]']        
 MaxPooling1D)                                                                                    
                                                                                                  
 global_max_pooling1d_4 (Global  (None, 900)         0           ['max_pooling1d_4[0][0]']        
 MaxPooling1D)                                                                                    
                                                                                                  
 concatenate (Concatenate)      (None, 4500)         0           ['global_max_pooling1d[0][0]',   
                                                                  'global_max_pooling1d_1[0][0]', 
                                                                  'global_max_pooling1d_2[0][0]', 
                                                                  'global_max_pooling1d_3[0][0]', 
                                                                  'global_max_pooling1d_4[0][0]'] 
                                                                                                  
2021-12-11 13:43:15.703041: I tensorflow/stream_executor/cuda/cuda_dnn.cc:366] Loaded cuDNN version 8200
 dense (Dense)                  (None, 256)          1152256     ['concatenate[0][0]']            
                                                                                                  
 dense_1 (Dense)                (None, 10)           2570        ['dense[0][0]']                  
                                                                                                  
==================================================================================================
Total params: 75,766,626
Trainable params: 75,766,626
Non-trainable params: 0
__________________________________________________________________________________________________
Epoch 1/10

Epoch 00001: val_categorical_accuracy improved from -inf to 0.40153, saving model to models/model.b256-vacc0.4015.h5
2342/2342 - 417s - loss: 1.5803 - categorical_accuracy: 0.3743 - val_loss: 1.4848 - val_categorical_accuracy: 0.4015 - 417s/epoch - 178ms/step
Epoch 2/10

Epoch 00002: val_categorical_accuracy improved from 0.40153 to 0.40455, saving model to models/model.b256-vacc0.4046.h5
2342/2342 - 423s - loss: 1.3635 - categorical_accuracy: 0.4537 - val_loss: 1.4984 - val_categorical_accuracy: 0.4046 - 423s/epoch - 180ms/step
Warning : `load_model` does not return WordVectorModel or SupervisedModel any more, but a `FastText` object which is very similar.
Epoch 00002: early stopping
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
[EVALUATION]
-------------------------------------------------------------------------------
Test: [0 1 2 3 4 5 6 7 8 9]
Pred: [0 1 2 3 4 5 6 7 8 9]

> Plotting confusion matrix...

> Calculating classification report...
              precision    recall  f1-score   support

         0.5       0.45      0.76      0.56      3763
         1.0       0.40      0.09      0.15      3141
         1.5       0.32      0.07      0.11      2691
         2.0       0.35      0.43      0.39      5523
         2.5       0.38      0.24      0.30      5704
         3.0       0.38      0.51      0.43      9464
         3.5       0.41      0.18      0.26      9709
         4.0       0.39      0.62      0.48     12603
         4.5       0.37      0.06      0.10      6423
         5.0       0.49      0.66      0.56      7576

    accuracy                           0.40     66597
   macro avg       0.39      0.36      0.33     66597
weighted avg       0.40      0.40      0.36     66597

-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
[PREDICTIONS]
-------------------------------------------------------------------------------
[INPUTS]
-------------------------------------------------------------------------------
###############################################################################


real	22m54.301s
user	16m54.321s
sys	0m37.039s
```

## Changes

1. Using complete train dataset for training with a 90/10 percent train/validation split.
2. Reduced batch size from 512 to 256.
3. Reran final predictions using model with least validation loss.
   1. Model with best accuracy had more loss.

```python
PREDICTOR_CLASSES = ['0.5', '1.0', '1.5', '2.0', '2.5', '3.0', '3.5', '4.0', '4.5', '5.0']
NB_CLASSES = len(PREDICTOR_CLASSES)

MAX_NB_WORDS = 1000000
MAX_SEQUENCE_LEN = 200
IS_TRAINABLE_EMBEDDINGS = True

#training params
BATCH_SIZE = 256
LEARNING_RATE = 0.0005
num_epochs = 10

#model parameters
PATIENCE=1

num_filters = embed_dim*3

inputs = tf.keras.layers.Input((max_tokens,), name="input", )
embs = Embedding(
    len(vocab), embed_dim, input_length=max_tokens,
    weights=[embeddings], 
    trainable=IS_TRAINABLE_EMBEDDINGS
)(inputs)

x1 = Conv1D(num_filters, 1, activation='relu', padding='valid')(embs)
x1 = SpatialDropout1D(0.3)(x1)
x1 = MaxPooling1D()(x1)
# x1 = Conv1D(embed_dim*2, 1, activation='relu', padding='valid')(x1)
# x1 = SpatialDropout1D(0.3)(x1)
# x1 = MaxPooling1D()(x1)
# x1 = Conv1D(embed_dim, 1, activation='relu', padding='valid')(x1)
# x1 = SpatialDropout1D(0.3)(x1)
# x1 = MaxPooling1D()(x1)
x1 = GlobalMaxPooling1D()(x1)

x2 = Conv1D(num_filters, 2, activation='relu', padding='valid')(embs)
x2 = SpatialDropout1D(0.3)(x2)
x2 = MaxPooling1D()(x2)
# x2 = Conv1D(embed_dim*2, 2, activation='relu', padding='valid')(x2)
# x2 = SpatialDropout1D(0.3)(x2)
# x2 = MaxPooling1D()(x2)
# x2 = Conv1D(embed_dim, 2, activation='relu', padding='valid')(x2)
# x2 = SpatialDropout1D(0.3)(x2)
# x2 = MaxPooling1D()(x2)
x2 = GlobalMaxPooling1D()(x2)

x3 = Conv1D(num_filters, 3, activation='relu', padding='valid')(embs)
x3 = SpatialDropout1D(0.3)(x3)
x3 = MaxPooling1D()(x3)
# x3 = Conv1D(embed_dim*2, 3, activation='relu', padding='valid')(x3)
# x3 = SpatialDropout1D(0.3)(x3)
# x3 = MaxPooling1D()(x3)
# x3 = Conv1D(embed_dim, 3, activation='relu', padding='valid')(x3)
# x3 = SpatialDropout1D(0.3)(x3)
# x3 = MaxPooling1D()(x3)
x3 = GlobalMaxPooling1D()(x3)

x4 = Conv1D(num_filters, 4, activation='relu', padding='valid')(embs)
x4 = SpatialDropout1D(0.3)(x4)
x4 = MaxPooling1D()(x4)
# x4 = Conv1D(embed_dim*2, 4, activation='relu', padding='valid')(x4)
# x4 = SpatialDropout1D(0.3)(x4)
# x4 = MaxPooling1D()(x4)
# x4 = Conv1D(embed_dim, 4, activation='relu', padding='valid')(x4)
# x4 = SpatialDropout1D(0.3)(x4)
# x4 = MaxPooling1D()(x4)
x4 = GlobalMaxPooling1D()(x4)

x5 = Conv1D(num_filters, 5, activation='relu', padding='valid')(embs)
x5 = SpatialDropout1D(0.3)(x5)
x5 = MaxPooling1D()(x5)
# x5 = Conv1D(embed_dim*2, 5, activation='relu', padding='valid')(x5)
# x5 = SpatialDropout1D(0.3)(x5)
# x5 = MaxPooling1D()(x5)
# x5 = Conv1D(embed_dim, 5, activation='relu', padding='valid')(x5)
# x5 = SpatialDropout1D(0.3)(x5)
# x5 = MaxPooling1D()(x5)
x5 = GlobalMaxPooling1D()(x5)

x = Concatenate(axis=1)([x1,x2,x3,x4,x5])
# x = GlobalMaxPooling1D()(x)
# x = Flatten()(x)
# x = Dropout(0.25)(x)
x = Dense(512, activation='relu', kernel_regularizer=regularizers.l2(weight_decay))(x)
output = Dense(NB_CLASSES, activation='softmax')(x)  #multi-label (k-hot encoding)
model = Model(inputs=[inputs], outputs=output)

optim = optimizers.Adam(learning_rate=LEARNING_RATE)
# optim = optimizers.Adadelta(learning_rate=LEARNING_RATE, rho=0.95, epsilon=1e-06)
model.compile(loss='categorical_crossentropy', optimizer=optim, metrics=['categorical_accuracy'])
model.summary()
plot_model(model, show_shapes=True, to_file='model_architecture.png')
```
