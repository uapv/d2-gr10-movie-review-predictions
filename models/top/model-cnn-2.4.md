# Model CNN 2.4

## Results

### Leaderboard

```powershell
G10_Maazouz-Turcotte	82.87%	40.82%	53.71%	22.2%	48.78%	31.32%	43.16%	29.85%	39.01%	5.92%	24.98%	57.51%
```

### Execution

```powershell
time python main.py --action train -tv bow-tfidf --type cnn --corpus train

```

## Changes

1. Using complete dev and train dataset to build the vocabulary.
2. Using complete train dataset for training with a 90/10 percent train/validation split.
3. Increased the maximum token counts quantile from 0.9 to 0.99.
4. Increased the final Dense layer size from 512 to 4096.

```python
PREDICTOR_CLASSES = ['0.5', '1.0', '1.5', '2.0', '2.5', '3.0', '3.5', '4.0', '4.5', '5.0']
NB_CLASSES = len(PREDICTOR_CLASSES)

MAX_NB_WORDS = 1000000
MAX_SEQUENCE_LEN = 200
IS_TRAINABLE_EMBEDDINGS = True

#training params
BATCH_SIZE = 256
LEARNING_RATE = 0.0005
num_epochs = 10

#model parameters
PATIENCE=2

num_filters = embed_dim*3

# inputs = tf.keras.layers.Input((max_tokens,), name="input", )
# x1 = Embedding(
#     len(vocab), embed_dim, input_length=max_tokens,
#     weights=[embeddings], 
#     trainable=IS_TRAINABLE_EMBEDDINGS
# )(inputs)
# x1 = Conv1D(num_filters, 7, activation='relu', padding='same')(x1)
# x1 = MaxPooling1D()(x1)
# x1 = Conv1D(num_filters, 7, activation='relu', padding='same')(x1)
# x1 = GlobalMaxPooling1D()(x1)
# x1 = Dropout(0.5)(x1)
# # x1 = Dense(32, activation='relu', kernel_regularizer=regularizers.l2(weight_decay))(x1)
# x1 = Dense(32, activation='relu', kernel_regularizer=regularizers.l2(weight_decay))(x1)
# output = Dense(NB_CLASSES, activation='softmax')(x1)  #multi-label (k-hot encoding)
# model = Model(inputs=[inputs], outputs=output)


inputs = tf.keras.layers.Input((max_tokens,), name="input", )
embs = Embedding(
    len(vocab), embed_dim, input_length=max_tokens,
    weights=[embeddings], 
    trainable=IS_TRAINABLE_EMBEDDINGS
)(inputs)

x1 = Conv1D(num_filters, 1, activation='relu', padding='valid')(embs)
x1 = SpatialDropout1D(0.3)(x1)
x1 = MaxPooling1D()(x1)
# x1 = Conv1D(embed_dim*2, 1, activation='relu', padding='valid')(x1)
# x1 = SpatialDropout1D(0.3)(x1)
# x1 = MaxPooling1D()(x1)
# x1 = Conv1D(embed_dim, 1, activation='relu', padding='valid')(x1)
# x1 = SpatialDropout1D(0.3)(x1)
# x1 = MaxPooling1D()(x1)
x1 = GlobalMaxPooling1D()(x1)

x2 = Conv1D(num_filters, 2, activation='relu', padding='valid')(embs)
x2 = SpatialDropout1D(0.3)(x2)
x2 = MaxPooling1D()(x2)
# x2 = Conv1D(embed_dim*2, 2, activation='relu', padding='valid')(x2)
# x2 = SpatialDropout1D(0.3)(x2)
# x2 = MaxPooling1D()(x2)
# x2 = Conv1D(embed_dim, 2, activation='relu', padding='valid')(x2)
# x2 = SpatialDropout1D(0.3)(x2)
# x2 = MaxPooling1D()(x2)
x2 = GlobalMaxPooling1D()(x2)

x3 = Conv1D(num_filters, 3, activation='relu', padding='valid')(embs)
x3 = SpatialDropout1D(0.3)(x3)
x3 = MaxPooling1D()(x3)
# x3 = Conv1D(embed_dim*2, 3, activation='relu', padding='valid')(x3)
# x3 = SpatialDropout1D(0.3)(x3)
# x3 = MaxPooling1D()(x3)
# x3 = Conv1D(embed_dim, 3, activation='relu', padding='valid')(x3)
# x3 = SpatialDropout1D(0.3)(x3)
# x3 = MaxPooling1D()(x3)
x3 = GlobalMaxPooling1D()(x3)

x4 = Conv1D(num_filters, 4, activation='relu', padding='valid')(embs)
x4 = SpatialDropout1D(0.3)(x4)
x4 = MaxPooling1D()(x4)
# x4 = Conv1D(embed_dim*2, 4, activation='relu', padding='valid')(x4)
# x4 = SpatialDropout1D(0.3)(x4)
# x4 = MaxPooling1D()(x4)
# x4 = Conv1D(embed_dim, 4, activation='relu', padding='valid')(x4)
# x4 = SpatialDropout1D(0.3)(x4)
# x4 = MaxPooling1D()(x4)
x4 = GlobalMaxPooling1D()(x4)

x5 = Conv1D(num_filters, 5, activation='relu', padding='valid')(embs)
x5 = SpatialDropout1D(0.3)(x5)
x5 = MaxPooling1D()(x5)
# x5 = Conv1D(embed_dim*2, 5, activation='relu', padding='valid')(x5)
# x5 = SpatialDropout1D(0.3)(x5)
# x5 = MaxPooling1D()(x5)
# x5 = Conv1D(embed_dim, 5, activation='relu', padding='valid')(x5)
# x5 = SpatialDropout1D(0.3)(x5)
# x5 = MaxPooling1D()(x5)
x5 = GlobalMaxPooling1D()(x5)

x = Concatenate(axis=1)([x1,x2,x3,x4,x5])
# x = GlobalMaxPooling1D()(x)
# x = Flatten()(x)
# x = Dropout(0.25)(x)
x = Dense(4096, activation='relu', kernel_regularizer=regularizers.l2(weight_decay))(x)
output = Dense(NB_CLASSES, activation='softmax')(x)  #multi-label (k-hot encoding)
model = Model(inputs=[inputs], outputs=output)

optim = optimizers.Adam(learning_rate=LEARNING_RATE)
# optim = optimizers.Adadelta(learning_rate=LEARNING_RATE, rho=0.95, epsilon=1e-06)
model.compile(loss='categorical_crossentropy', optimizer=optim, metrics=['categorical_accuracy'])
model.summary()
plot_model(model, show_shapes=True, to_file='model_architecture.png')
```
