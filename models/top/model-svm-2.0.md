# Model SVM 2.0 (76.16%, 30.73%)

## Results

### Leaderboard

```powershell
G10_Maazouz-Turcotte	76.16%	30.73%	44.07%	10.09%	36.82%	20.49%	33.13%	21.13%	27.47%	6.51%	15.79%	45.6%
```

### Execution

```powershell
(venv) PS D:\Docs\M2\M2S3-APP-INNOV\D2-APP\d2-notes-films> python main.py --action train -tv bow-tf --type svm --corpus train --fast --corpus-subset 200000

###############################################################################
 [TRAIN_MODEL]
###############################################################################
-------------------------------------------------------------------------------
 [ARGUMENTS]
-------------------------------------------------------------------------------
             Corpus : train (data/train.xml)
    Text Vectorizer : bow-tf
         Model Type : svm
        Is fast run : True
 Corpus Subset Size : 200000
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 [TRAINING]
-------------------------------------------------------------------------------
Total Tokens: 3982

          token  frequency
0          plus     127174
1          bien     123711
2          très     118142
3          tout     106874
4           peu      67561
5          fait      67385
6           bon      66182
7      histoire      62943
8          sans      61088
9         comme      60599
10           si      59077
11        cette      53070
12         être      51253
13         voir      50125
14           ça      49274
15        aussi      44671
16      acteurs      43769
17         trop      43699
18     scénario      42674
19     vraiment      41994
20        faire      39079
21         peut      36440
22  personnages      33677
23        assez      33241
24          fin      32689
25        scène      31307
26         rien      31233
27     beaucoup      30057
28       scènes      29270
29         deux      28958
30        reste      28463
31         fois      27297
32        entre      27122
33          car      27011
34           où      26735
35          mal      26448
36       moment      26343
37        quand      26312
38         tous      25639
39           là      25393
40        films      25383
41     quelques      25359
42        moins      25180
43        grand      25091
44       encore      24963
45           vu      24488
46         donc      24392
47         cela      23958
48     toujours      23097
49   personnage      22947

            token  frequency
54    __TOK_EMP__      21422
1023  __TOK_POS__       1982
X Shape : (200000, 3982)
Y Shape : 200000

Model Summary :
LinearSVC(dual=False)

Mean Accuracy : 0.3586

              precision    recall  f1-score   support

         0.5       0.44      0.67      0.53      1839
         1.0       0.29      0.16      0.20      1440
         1.5       0.23      0.07      0.11      1256
         2.0       0.31      0.31      0.31      2554
         2.5       0.31      0.19      0.24      2701
         3.0       0.34      0.39      0.36      4304
         3.5       0.32      0.26      0.29      4361
         4.0       0.36      0.52      0.43      5420
         4.5       0.31      0.10      0.15      2840
         5.0       0.43      0.60      0.50      3285

    accuracy                           0.36     30000
   macro avg       0.33      0.33      0.31     30000
weighted avg       0.34      0.36      0.33     30000

-------------------------------------------------------------------------------
###############################################################################

(venv) PS D:\Docs\M2\M2S3-APP-INNOV\D2-APP\d2-notes-films> python main.py --action predict --type svm

###############################################################################
 [PREDICT_NOTES]
###############################################################################
-------------------------------------------------------------------------------
 [ARGUMENTS]
-------------------------------------------------------------------------------
         Model Type : svm
        Is fast run : False
 Corpus Subset Size : 1000
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 [PREDICTIONS]
-------------------------------------------------------------------------------
X Shape : (85847, 3982)
-------------------------------------------------------------------------------
###############################################################################
```

## Changes

1. Created and refactored execution pipeline.
2. Ran on 'train' instead of 'dev' with a corpus subset of 200000.
3. Tweaks to 'min_df' and 'max_df'.
4. Increased 'max_features'.

```python
stop_words = stopwords.words('french')

cv = CountVectorizer(
    # The 'lowercase' and 'strip_accents' parameters are ignored when
    # 'preprocessor' is passed.
    # lowercase=True,
    # strip_accents='unicode', # {‘ascii’, ‘unicode’}
    ngram_range=(1,4), # (1,1), # (1,3)
    min_df=0.0025, # Ignore too infrequent terms. 1
    max_df=0.40, # Ignore too frequent terms. 1.0
    max_features=6000,
    token_pattern=r'(?u)\b\w+\b', # (?u)\b\w\w+\b
    stop_words=stop_words, 
    preprocessor=preprocess_item
)
```
