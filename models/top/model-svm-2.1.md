# Model SVM 2.1 (76.72%, 31.05%)

## Results

### Leaderboard

```powershell
G10_Maazouz-Turcotte	76.72%	31.05%	44.03%	9.21%	36.73%	25.01%	33.12%	20.86%	26.73%	6.44%	17.07%	45.86%
```

### Execution

```powershell
(venv) PS D:\Docs\M2\M2S3-APP-INNOV\D2-APP\d2-notes-films> python main.py --action train -tv bow-tf --type svm --corpus train --fast --corpus-subset 200000

###############################################################################
 [TRAIN_MODEL]
###############################################################################
-------------------------------------------------------------------------------
 [ARGUMENTS]
-------------------------------------------------------------------------------
             Corpus : train (data/train.xml)
    Text Vectorizer : bow-tf
         Model Type : svm
        Is fast run : True
 Corpus Subset Size : 200000
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 [TRAINING]
-------------------------------------------------------------------------------
Total Tokens: 3634

          token  frequency
0   __TOK_EMP__     128127
1          plus     127175
2          bien     123712
3          très     118142
4           peu      67562
5           bon      66183
6      histoire      62944
7          sans      61088
8         cette      53070
9          voir      50126
10        aussi      44671
11      acteurs      43770
12         trop      43699
13     scénario      42674
14     vraiment      41994
15        faire      39079
16         peut      36440
17  personnages      33678
18        assez      33241
19          fin      32689
20        scène      31307
21         rien      31233
22     beaucoup      30057
23       scènes      29270
24         deux      28958
25        reste      28463
26         fois      27329
27        entre      27122
28          car      27011
29           où      26735
30          mal      26448
31       moment      26344
32        quand      26312
33         tous      25639
34           là      25393
35     quelques      25359
36        moins      25180
37        grand      25091
38       encore      24963
39           vu      24488
40         donc      24392
41         cela      23958
42     toujours      23097
43   personnage      22947
44        temps      22737
45       cinéma      22495
46        après      22024
47          vie      21921
48       plutôt      21338
49        alors      21287

           token  frequency
0    __TOK_EMP__     128127
961  __TOK_POS__       1982
X Shape : (200000, 3634)
Y Shape : 200000

Model Summary :
LinearSVC(dual=False)

Mean Accuracy : 0.355

              precision    recall  f1-score   support

         0.5       0.43      0.67      0.53      1796
         1.0       0.23      0.12      0.16      1423
         1.5       0.20      0.05      0.08      1279
         2.0       0.32      0.31      0.31      2539
         2.5       0.30      0.18      0.22      2723
         3.0       0.33      0.40      0.36      4149
         3.5       0.32      0.25      0.28      4413
         4.0       0.36      0.53      0.43      5588
         4.5       0.27      0.08      0.13      2777
         5.0       0.44      0.61      0.51      3313

    accuracy                           0.36     30000
   macro avg       0.32      0.32      0.30     30000
weighted avg       0.33      0.35      0.33     30000

-------------------------------------------------------------------------------
###############################################################################

(venv) PS D:\Docs\M2\M2S3-APP-INNOV\D2-APP\d2-notes-films> python main.py --action predict --type svm

###############################################################################
 [PREDICT_NOTES]
###############################################################################
-------------------------------------------------------------------------------
 [ARGUMENTS]
-------------------------------------------------------------------------------
         Model Type : svm
        Is fast run : False
 Corpus Subset Size : 1000
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 [PREDICTIONS]
-------------------------------------------------------------------------------
X Shape : (85847, 3634)
-------------------------------------------------------------------------------
###############################################################################
```

## Changes

1. Modified regex for TOK_EMP to also match single exclamation marks.
2. Regex to remove most digits, except for notes.
3. Created custom stop words dictionary for this problem. ('stopwords-min-fr.txt')

```python
REGEX_DIGITS = r"(?<!\.|,|\/)[0-9]+(?!\.|,|\/)"

with open('dictionaries/stopwords-min-fr.txt', 'r', encoding='utf-8') as f:
    stop_words = set([l.replace('\n', '') for l in f.readlines() if not l.startswith('#')])

stop_words.update(stopwords.words('french'))

cv = CountVectorizer(
    # The 'lowercase' and 'strip_accents' parameters are ignored when
    # 'preprocessor' is passed.
    # lowercase=True,
    # strip_accents='unicode', # {‘ascii’, ‘unicode’}
    ngram_range=(1,4), # (1,1), # (1,3)
    min_df=0.0025, # Ignore too infrequent terms. 1
    max_df=0.40, # Ignore too frequent terms. 1.0
    max_features=6000,
    token_pattern=r'(?u)\b\w+\b', # (?u)\b\w\w+\b
    stop_words=stop_words, 
    preprocessor=preprocess_item
)
```
