# Model SVM 2.2 (76.72%, 31.05%)

## Results

### Leaderboard

```powershell
G10_Maazouz-Turcotte	78.21%	33.88%	52.05%	13.6%	39.58%	26.92%	33.36%	26.9%	25.35%	14.28%	13.35%	52.28%
```

### Execution

```powershell
(venv) PS D:\Docs\M2\M2S3-APP-INNOV\D2-APP\d2-notes-films> python main.py --action train -tv bow-tfidf --type svm --corpus train --fast --corpus-subset 200000

###############################################################################
 [TRAIN_MODEL]
###############################################################################
-------------------------------------------------------------------------------
 [ARGUMENTS]
-------------------------------------------------------------------------------
             Corpus : train (data/train.xml)
    Text Vectorizer : bow-tfidf
         Model Type : svm
        Is fast run : True
 Corpus Subset Size : 200000
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 [TRAINING]
-------------------------------------------------------------------------------
	token	frequency
0	navet	2.729187417551908
1	propagande	2.669570932670055
2	nullité	2.5705278675517604
3	nul	2.4858865518142883
4	0 5	2.4201015631428193
5	nullissime	2.279943784552492
6	assez	-2.021523057985362
7	déconseille	1.9681020487145153
8	résume	1.892874288169204
9	honte	1.8802928993840256
10	lamentable	1.8003638616610522
11	consternant	1.7938713805352149
12	fuir	1.7727467245622648
13	demi étoile	1.6808617179255518
14	pire	1.6777051447708935
15	risible	1.672117479527805
16	supplice	1.6414036502128484
17	1 5	-1.5928972149487415
18	bon	-1.5866083178570491
19	1 2	1.582609642841079
20	bouse	1.5779393550642316
21	pourtant adore	1.569937914958641
22	excellent	-1.5559426588891203
23	première fois	1.546970720967152
24	__TOK_EMP__ fuir	1.5257077043810767
25	dormi	1.5248550255117501
26	insulte	1.4930830422562023
27	aucune scène	-1.4826014367098292
28	trop forte	1.4755447667476738
29	médiocrité	1.4739474107609936
30	pires	1.4566823731167124
31	naze	1.4563139640709195
32	hop	1.4552016300496609
33	énorme déception	1.441970154477367
34	meilleurs	-1.4317925357158572
35	merde __TOK_EMP__	1.425651078909263
36	carton	1.4203584764756119
37	vulgaire	1.4201447047339453
38	foutent	1.416656549035857
39	merdique	1.4087022036317978
40	sexualité	1.4086518283833571
41	plus mauvais	1.4047677677487072
42	daube	1.4047237920491802
43	affligeant	1.39777736435156
44	irritant	1.387005157856376
45	aucun	1.3767145987175498
46	parfait	-1.3759460287818597
47	plus rire	1.3372745685572967
48	duo	-1.3365221192177807
49	néant	1.3355251264759982

Model Summary :
SGDClassifier(max_iter=50)

Mean Accuracy : 0.34126666666666666

              precision    recall  f1-score   support

         0.5       0.44      0.73      0.55      1819
         1.0       0.19      0.12      0.14      1361
         1.5       0.18      0.12      0.14      1220
         2.0       0.28      0.23      0.25      2469
         2.5       0.27      0.22      0.24      2631
         3.0       0.32      0.32      0.32      4094
         3.5       0.30      0.25      0.27      4339
         4.0       0.38      0.43      0.40      5679
         4.5       0.20      0.10      0.13      2915
         5.0       0.43      0.66      0.52      3473

    accuracy                           0.34     30000
   macro avg       0.30      0.32      0.30     30000
weighted avg       0.32      0.34      0.32     30000

-------------------------------------------------------------------------------
###############################################################################


###############################################################################
 [PREDICT_NOTES]
###############################################################################
-------------------------------------------------------------------------------
 [ARGUMENTS]
-------------------------------------------------------------------------------
    Text Vectorizer : bow-tfidf
         Model Type : svm
        Is fast run : True
 Corpus Subset Size : 200000
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 [PREDICTIONS]
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
###############################################################################
```

## Changes

1. Used TF-IDF (TfidfVectorizer) instead of TF (CountVectorizer).
2. Subset is now fetching random samples via 'sample()' instead of 'head()'.

```python
REGEX_DIGITS = r"(?<!\.|,|\/)[0-9]+(?!\.|,|\/)"

with open('dictionaries/stopwords-min-fr.txt', 'r', encoding='utf-8') as f:
    stop_words = set([l.replace('\n', '') for l in f.readlines() if not l.startswith('#')])

stop_words.update(stopwords.words('french'))

vectorizer = TfidfVectorizer(
    # The 'lowercase' and 'strip_accents' parameters are ignored when
    # 'preprocessor' is passed.
    # lowercase=True,
    # strip_accents='unicode', # {‘ascii’, ‘unicode’}
    ngram_range=(1,6), # (1,1), # (1,3)
    min_df=0.00025, # Ignore too infrequent terms. 1
    max_df=0.95, # Ignore too frequent terms. 1.0
    max_features=200000,
    token_pattern=r'(?u)\b\w+\b', # (?u)\b\w\w+\b
    stop_words=stop_words, 
    preprocessor=preprocess_item
)

vectorizer = st.get_tfidf()    
classifier = SGDClassifier(alpha=0.0001, max_iter=50, penalty='l2')
```
