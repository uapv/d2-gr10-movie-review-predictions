# Model SVM 2.3 (76.72%, 31.05%)

## Results

### Leaderboard

```powershell
G10_Maazouz-Turcotte	78.25%	34.22%	51.57%	13.72%	39.88%	28.66%	33.02%	26.13%	26.5%	14.2%	14.23%	52.48%
```

### Execution

```powershell
(venv) PS D:\Docs\M2\M2S3-APP-INNOV\D2-APP\d2-notes-films> (Measure-Command { python main.py --action train -tv bow-tfidf --type svm --corpus train --fast --corpus-subset 200000 | Out-Default }).ToString()

###############################################################################
 [TRAIN_MODEL]
###############################################################################
-------------------------------------------------------------------------------
 [ARGUMENTS]
-------------------------------------------------------------------------------
             Corpus : train (data/train.xml)
    Text Vectorizer : bow-tfidf
         Model Type : svm
        Is fast run : True
 Corpus Subset Size : 200000
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 [TRAINING]
-------------------------------------------------------------------------------

Model Summary :
SGDClassifier(max_iter=50)

Mean Accuracy : 0.34476666666666667

              precision    recall  f1-score   support

         0.5       0.41      0.74      0.53      1669
         1.0       0.20      0.10      0.14      1393
         1.5       0.15      0.10      0.12      1212
         2.0       0.29      0.24      0.26      2454
         2.5       0.28      0.24      0.26      2575
         3.0       0.34      0.32      0.33      4326
         3.5       0.30      0.28      0.29      4418
         4.0       0.38      0.43      0.40      5610
         4.5       0.22      0.09      0.13      2968
         5.0       0.43      0.68      0.53      3375

    accuracy                           0.34     30000
   macro avg       0.30      0.32      0.30     30000
weighted avg       0.32      0.34      0.32     30000

-------------------------------------------------------------------------------
###############################################################################


###############################################################################
 [PREDICT_NOTES]
###############################################################################
-------------------------------------------------------------------------------
 [ARGUMENTS]
-------------------------------------------------------------------------------
    Text Vectorizer : bow-tfidf
         Model Type : svm
        Is fast run : False
 Corpus Subset Size : 1000
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 [PREDICTIONS]
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
###############################################################################

00:06:43.4295014
```

## Changes

1. Back to original stop words from NLTK.
2. Redued 'max_df' form 0.95 to 0.90.

```python
REGEX_DIGITS = r"(?<!\.|,|\/)[0-9]+(?!\.|,|\/)"

stop_words = stopwords.words('french')

vectorizer = TfidfVectorizer(
    # The 'lowercase' and 'strip_accents' parameters are ignored when
    # 'preprocessor' is passed.
    # lowercase=True,
    # strip_accents='unicode', # {‘ascii’, ‘unicode’}
    ngram_range=(1,6), # (1,1), # (1,3)
    min_df=0.00025, # Ignore too infrequent terms. 1
    max_df=0.90, # Ignore too frequent terms. 1.0
    max_features=200000,
    token_pattern=r'(?u)\b\w+\b', # (?u)\b\w\w+\b
    stop_words=stop_words, 
    preprocessor=preprocess_item
)

vectorizer = st.get_tfidf()    
classifier = SGDClassifier(alpha=0.0001, max_iter=50, penalty='l2')
```
