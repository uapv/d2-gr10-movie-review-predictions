# Model SVM 2.4 (78.44%, 34.77%)

## Results

### Leaderboard

```powershell
G10_Maazouz-Turcotte	78.44%	34.77%	52.26%	13%	40.87%	27.33%	35.01%	26.18%	27.91%	14.77%	13.5%	53.03%
```

### Execution

```powershell
(venv) PS D:\Docs\M2\M2S3-APP-INNOV\D2-APP\d2-notes-films> (Measure-Command { python main.py --action train -tv bow-tfidf --type svm --corpus train --fast --corpus-subset 200000 | Out-Default }).ToString()

###############################################################################
 [TRAIN_MODEL]
###############################################################################
-------------------------------------------------------------------------------
 [ARGUMENTS]
-------------------------------------------------------------------------------
             Corpus : train (data/train.xml)
    Text Vectorizer : bow-tfidf
         Model Type : svm
        Is fast run : True
 Corpus Subset Size : 200000
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 [TRAINING]
-------------------------------------------------------------------------------

Model Summary :
SGDClassifier(max_iter=50)

Mean Accuracy : 0.3532

              precision    recall  f1-score   support

         0.5       0.43      0.72      0.54      1734
         1.0       0.18      0.09      0.12      1310
         1.5       0.16      0.13      0.15      1159
         2.0       0.31      0.27      0.29      2487
         2.5       0.32      0.25      0.28      2696
         3.0       0.35      0.36      0.35      4250
         3.5       0.31      0.25      0.28      4460
         4.0       0.38      0.42      0.40      5646
         4.5       0.21      0.10      0.13      2790
         5.0       0.43      0.70      0.54      3468

    accuracy                           0.35     30000
   macro avg       0.31      0.33      0.31     30000
weighted avg       0.33      0.35      0.33     30000

-------------------------------------------------------------------------------
###############################################################################


###############################################################################
 [PREDICT_NOTES]
###############################################################################
-------------------------------------------------------------------------------
 [ARGUMENTS]
-------------------------------------------------------------------------------
    Text Vectorizer : bow-tfidf
         Model Type : svm
        Is fast run : False
 Corpus Subset Size : 1000
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 [PREDICTIONS]
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
###############################################################################

00:06:44.7224589
```

## Changes

1. Removed the digits filtering regex.

```python
stop_words = stopwords.words('french')

vectorizer = TfidfVectorizer(
    # The 'lowercase' and 'strip_accents' parameters are ignored when
    # 'preprocessor' is passed.
    # lowercase=True,
    # strip_accents='unicode', # {‘ascii’, ‘unicode’}
    ngram_range=(1,6), # (1,1), # (1,3)
    min_df=0.00025, # Ignore too infrequent terms. 1
    max_df=0.90, # Ignore too frequent terms. 1.0
    max_features=200000,
    token_pattern=r'(?u)\b\w+\b', # (?u)\b\w\w+\b
    stop_words=stop_words, 
    preprocessor=preprocess_item
)

vectorizer = st.get_tfidf()    
classifier = SGDClassifier(alpha=0.0001, max_iter=50, penalty='l2')
```
