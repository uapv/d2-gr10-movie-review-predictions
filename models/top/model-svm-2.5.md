# Model SVM 2.5 (79.24%, 35.00%)

## Results

### Leaderboard

```powershell
G10_Maazouz-Turcotte	79.24%	35%	51.91%	13.65%	40.5%	28.79%	34.49%	26.99%	29.2%	13.47%	13.98%	53.26%
```

### Execution

```powershell
(venv) PS D:\Docs\M2\M2S3-APP-INNOV\D2-APP\d2-notes-films> (Measure-Command { python main.py --action train -tv bow-tfidf --type svm --corpus train --fast --corpus-subset 200000 | Out-Default }).ToString()

###############################################################################
 [TRAIN_MODEL]
###############################################################################
-------------------------------------------------------------------------------
 [ARGUMENTS]
-------------------------------------------------------------------------------
             Corpus : train (data/train.xml)
    Text Vectorizer : bow-tfidf
         Model Type : svm
        Is fast run : True
 Corpus Subset Size : 200000
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 [TRAINING]
-------------------------------------------------------------------------------

Model Summary :
SGDClassifier(max_iter=50)

Mean Accuracy : 0.3539333333333333

              precision    recall  f1-score   support

         0.5       0.44      0.74      0.55      1718
         1.0       0.25      0.11      0.16      1377
         1.5       0.20      0.10      0.13      1267
         2.0       0.29      0.29      0.29      2432
         2.5       0.29      0.23      0.26      2669
         3.0       0.34      0.35      0.35      4233
         3.5       0.30      0.29      0.29      4276
         4.0       0.38      0.41      0.40      5597
         4.5       0.22      0.09      0.13      2905
         5.0       0.43      0.70      0.53      3526

    accuracy                           0.35     30000
   macro avg       0.32      0.33      0.31     30000
weighted avg       0.33      0.35      0.33     30000

-------------------------------------------------------------------------------
###############################################################################


###############################################################################
 [PREDICT_NOTES]
###############################################################################
-------------------------------------------------------------------------------
 [ARGUMENTS]
-------------------------------------------------------------------------------
    Text Vectorizer : bow-tfidf
         Model Type : svm
        Is fast run : False
 Corpus Subset Size : 1000
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 [PREDICTIONS]
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
###############################################################################

00:07:18.8651987
```

## Changes

1. Removed 'min_df' from the TF-IDF Vectorizer.

```python
stop_words = stopwords.words('french')

vectorizer = TfidfVectorizer(
    # The 'lowercase' and 'strip_accents' parameters are ignored when
    # 'preprocessor' is passed.
    # lowercase=True,
    # strip_accents='unicode', # {‘ascii’, ‘unicode’}
    ngram_range=(1,6), # (1,1), # (1,3)
    # min_df=0.00025, # Ignore too infrequent terms. 1
    max_df=0.90, # Ignore too frequent terms. 1.0
    max_features=200000,
    token_pattern=r'(?u)\b\w+\b', # (?u)\b\w\w+\b
    stop_words=stop_words, 
    preprocessor=preprocess_item
)

vectorizer = st.get_tfidf()    
classifier = SGDClassifier(alpha=0.0001, max_iter=50, penalty='l2')
```
