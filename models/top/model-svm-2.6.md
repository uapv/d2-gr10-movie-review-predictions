# Model SVM 2.6

## Results

### Leaderboard

```powershell
G10_Maazouz-Turcotte	79.34%	35.32%	51.98%	14.03%	40.9%	28.22%	36.17%	26.02%	29.5%	13.92%	14.82%	53.62%
```

### Execution

```powershell
(venv) PS D:\Docs\M2\M2S3-APP-INNOV\D2-APP\d2-notes-films> (Measure-Command { python main.py --action train -tv bow-tfidf --type svm --corpus train --fast --corpus-subset 200000 | Out-Default }).ToString()

###############################################################################
 [TRAIN_MODEL]
###############################################################################
-------------------------------------------------------------------------------
 [ARGUMENTS]
-------------------------------------------------------------------------------
             Corpus : train (data/train.xml)
    Text Vectorizer : bow-tfidf
         Model Type : svm
        Is fast run : True
 Corpus Subset Size : 200000
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 [TRAINING]
-------------------------------------------------------------------------------

Model Summary :
SGDClassifier(max_iter=50)

      Features : 300000
 Mean Accuracy : 0.358

              precision    recall  f1-score   support

         0.5       0.45      0.74      0.56      1718
         1.0       0.24      0.12      0.16      1352
         1.5       0.19      0.10      0.13      1205
         2.0       0.31      0.29      0.30      2545
         2.5       0.30      0.21      0.25      2673
         3.0       0.34      0.38      0.36      4263
         3.5       0.32      0.27      0.29      4404
         4.0       0.38      0.46      0.42      5645
         4.5       0.20      0.08      0.12      2853
         5.0       0.43      0.67      0.53      3342

    accuracy                           0.36     30000
   macro avg       0.32      0.33      0.31     30000
weighted avg       0.33      0.36      0.33     30000

-------------------------------------------------------------------------------
###############################################################################


###############################################################################
 [PREDICT_NOTES]
###############################################################################
-------------------------------------------------------------------------------
 [ARGUMENTS]
-------------------------------------------------------------------------------
    Text Vectorizer : bow-tfidf
         Model Type : svm
        Is fast run : False
 Corpus Subset Size : 1000
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
 [PREDICTIONS]
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
###############################################################################

00:06:05.2534121
```

## Changes

1. Halfed the value of 'max_df' for the TF-IDF Vectorizer (0.9 => 0.5).
2. Reduced ngram_range from (1,6) to (1,5).
3. Increased the max_features from 200000 to 300000.
   1. More features seem to add more accuracy. Is there a limit?

```python
stop_words = stopwords.words('french')

vectorizer = TfidfVectorizer(
    # The 'lowercase' and 'strip_accents' parameters are ignored when
    # 'preprocessor' is passed.
    # lowercase=True,
    # strip_accents='unicode', # {‘ascii’, ‘unicode’}
    ngram_range=(1,5), # (1,1), # (1,3)
    # min_df=0.00025, # Ignore too infrequent terms. 1
    max_df=0.50, # Ignore too frequent terms. 1.0
    max_features=300000,
    token_pattern=r'(?u)\b\w+\b', # (?u)\b\w\w+\b
    stop_words=stop_words, 
    preprocessor=preprocess_item
)

vectorizer = st.get_tfidf()    
classifier = SGDClassifier(alpha=0.0001, max_iter=50, penalty='l2')
```
