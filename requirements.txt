# cudatoolkit==11.3.1
# torch==1.10.0+cu102
# torchvision==0.11.1+cu102
# torchaudio===0.10.0+cu102
# transformers
# tokenizers==0.10.1
# datasets
# fasttext
regex
numpy
matplotlib
pandas
seaborn
pytables
fitter
scikit-learn
nltk
tqdm
scikit-learn-intelex
tensorflow-gpu
pydot