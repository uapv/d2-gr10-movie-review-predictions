#!/bin/bash
#SBATCH --job-name=tests
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=6
#SBATCH --gpus-per-node=1
#SBATCH --mem=32G
#SBATCH --time=2-00:00:00
#SBATCH --output=runs/slurm-%j.out

python -c "import torch; print('Is GPU training enabled? {}'.format(torch.cuda.is_available()))"

# time python -u _01_dataset.py

# time python -u _02_linear_svm.py \
#     --feature_set basic \
#     --corpus_subset 700000 \
#     --test_ratio 0.2 \
#     --alpha 1e-4 \
#     --max_iterations 1000 \
#     --penalty l2 \
#     --loss hinge \
#     --ngram_min 1 \
#     --ngram_max 5 \
#     --df_max 0.95 \
#     --max_features 30000000
# time python -u _02_linear_svm.py \
#     --feature_set metadata \
#     --corpus_subset 700000 \
#     --test_ratio 0.2 \
#     --alpha 1e-4 \
#     --max_iterations 1000 \
#     --penalty l2 \
#     --loss hinge \
#     --ngram_min 1 \
#     --ngram_max 5 \
#     --df_max 0.95 \
#     --max_features 30000000
time python -u _02_linear_svm.py \
    --feature_set sentiment \
    --corpus_subset 70000 \
    --test_ratio 0.2 \
    --alpha 1e-4 \
    --max_iterations 1000 \
    --penalty l2 \
    --loss hinge \
    --ngram_min 1 \
    --ngram_max 5 \
    --df_max 0.95 \
    --max_features 30000000

# time python -u _03_cnn.py \
#     --feature_set basic \
#     --corpus_subset 700000 \
#     --max_epochs 4 \
#     --batch_size 256 \
#     --learning_rate 5e-4 \
#     --filters 600 \
#     --dropout 0.4 \
#     --trainable_embeddings

# time python -u _04_bert.py \
#     --feature_set basic \
#     --corpus_subset 160000 \
#     --test_ratio 0.2 \
#     --max_epochs 4 \
#     --batch_size 32 \
#     --learning_rate 1e-5 \
#     --epsilon 1e-4 \
#     --max_sequence_len 512 \
#     --use_fp16 \
#     --use_amp_level O3 \
#     --bert_model 'camembert-base'
# time python -u _04_bert.py \
#     --feature_set basic \
#     --corpus_subset 160000 \
#     --test_ratio 0.2 \
#     --max_epochs 4 \
#     --batch_size 8 \
#     --learning_rate 1e-5 \
#     --epsilon 1e-4 \
#     --max_sequence_len 512 \
#     --use_fp16 \
#     --amp_level O3 \
#     --bert_model 'camembert/camembert-large'