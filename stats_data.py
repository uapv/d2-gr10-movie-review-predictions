import torch
import fitter
import matplotlib.pyplot as plt
from pandas.core.series import Series
from pandas.core.frame import DataFrame
import seaborn as sns

sns.set_theme(style="darkgrid")

# SOURCE: https://www.python-graph-gallery.com/24-histogram-with-a-boxplot-on-top-seaborn
def display_histbox_combo_graph(df:DataFrame, column:str, bins:list):
    # creating a figure composed of two matplotlib.Axes objects (ax_box and ax_hist)
    f, (ax_box, ax_hist) = plt.subplots(2, sharex=True, gridspec_kw={"height_ratios": (.15, .85)})
    
    # assigning a graph to each ax
    sns.boxplot(data=df, x=column, ax=ax_box)
    sns.histplot(data=df, x=column, ax=ax_hist, bins=bins)
    
    # Remove x axis name for the boxplot
    ax_box.set(xlabel='')
    plt.show()
    plt.close()
    return

def display_correlation_matrix(df:DataFrame, include_computed=False):
    # Correlation between original and computed variables.
    df_corr = add_computed_variables(df, include_computed=include_computed).corr()

    # Display correlation matrix.
    cmap = sns.diverging_palette(230, 20, as_cmap=True)
    sns.heatmap(df_corr, annot=True, cmap=cmap)
    plt.show()
    plt.close()
    return

def display_distribution_review_id(df:DataFrame):
    freq = df.groupby(['review_id'])['note'].count()
    ax = sns.lineplot(data=df, x=range(0, len(df)), y=freq, alpha=0.7)
    ax.set(xlabel='', ylabel='')
    plt.show()
    plt.close()
    return

def display_distribution_movie_id(df:DataFrame):
    # TODO: Log plot instead of long skewed histogram.
    display_histbox_combo_graph(
        df.groupby(['movie_id'])['review_id'].count().reset_index(name='review_id'), 
        'review_id', 
        100
    )
    return

def display_distribution_user_id(df:DataFrame):
    # TODO: Log plot instead of long skewed histogram.
    display_histbox_combo_graph(
        df.groupby(['user_id'])['review_id'].count().reset_index(name='review_id'), 
        'review_id', 
        100
    )
    return

def display_distribution_name(df:DataFrame):
    # TODO: Log plot instead of long skewed histogram.
    display_histbox_combo_graph(
        df.groupby(['name'])['review_id'].count().reset_index(name='review_id'), 
        'review_id', 
        100
    )
    return

def display_distribution_note(df:DataFrame):
    display_histbox_combo_graph(df, 'note', 10)
    return

def display_distribution_comment_length(df:DataFrame):
    # TODO: Log plot instead of long skewed histogram.
    display_histbox_combo_graph(df, 'comment_length', 100)
    return

def add_computed_variables(df:DataFrame, include_computed:bool=False, verbose:bool=False):
    dfc = df.copy(deep=True)

    dfc['reviews_per_movie_id'] = dfc.groupby(['movie_id'])['review_id'].transform('count')
    dfc['reviews_per_user_id'] = dfc.groupby(['user_id'])['review_id'].transform('count')
    dfc['reviews_per_name'] = dfc.groupby(['name'])['review_id'].transform('count')

    if (include_computed):
        dfc['comment_len_avg_per_movie_id'] = dfc.groupby(['movie_id'])['comment_length'].transform('mean')
        dfc['comment_len_med_per_movie_id'] = dfc.groupby(['movie_id'])['comment_length'].transform('median')
        dfc['comment_len_avg_per_user_id'] = dfc.groupby(['user_id'])['comment_length'].transform('mean')
        dfc['comment_len_med_per_user_id'] = dfc.groupby(['user_id'])['comment_length'].transform('median')
        dfc['note_avg_per_movie_id'] = dfc.groupby(['movie_id'])['note'].transform('mean')
        dfc['note_med_per_movie_id'] = dfc.groupby(['movie_id'])['note'].transform('median')
        dfc['note_avg_per_user_id'] = dfc.groupby(['user_id'])['note'].transform('mean')
        dfc['note_med_per_user_id'] = dfc.groupby(['user_id'])['note'].transform('median')

    if (verbose):
        print(dfc.info())
        print(dfc.groupby(['user_id'])['comment'].apply(lambda x: len(x)).head(25))

    return dfc

def get_distribution(series:Series, show_plot:bool) -> dict:
    # Fit the distribution.
    f = fitter.Fitter(
        series,
        timeout=60, 
        distributions=fitter.get_common_distributions())
    f.fit()
    
    # Display the 5 best distribution fits.
    if (show_plot):
        f.summary(Nbest=5, lw=2, plot=True, method='sumsquare_error', clf=True)
        plt.show()
        plt.close()

    # Return only the best distribution fit.
    return f.get_best(method='sumsquare_error')

##############################################################################
# [ENTRYPOINTS]
##############################################################################

# Display and save plots.
# EXAMPLES: https://www.python-graph-gallery.com/
# EXAMPLES: https://seaborn.pydata.org/examples/index.html
def display_graphs(df:DataFrame):
    # Distribution Graphs
    # TODO: Fix the titles, axes labels and legends!
    display_distribution_note(df)
    display_distribution_comment_length(df)
    display_distribution_review_id(df)
    display_distribution_movie_id(df)
    display_distribution_user_id(df)
    display_distribution_name(df)

    # Correlation between original variables..corr()
    display_correlation_matrix(df)

    # Correlation between original and computed variables.
    display_correlation_matrix(df, include_computed=True)

    # for i in range(0, len(df.columns), 5):
    #     sns.pairplot(data=df,
    #         x_vars=df.columns[i:i+5],
    #         y_vars=['note'])
    #     plt.show()
    #     plt.close()

    # sns.pairplot(df)
    # plt.show()
    # plt.close()

    # g = sns.PairGrid(df)
    # g.map_upper(sns.histplot)
    # g.map_lower(sns.kdeplot, fill=True)
    # g.map_diag(sns.histplot, kde=True)
    # plt.show()
    # plt.close()

    # sns.pairplot(
    #     df,
    #     x_vars=['note'],
    #     y_vars=['comment_length'],
    # )
    # plt.show()
    # plt.close()
    return